﻿using System;
using Kofax.Capture.SDK.Workflow;
using Kofax.Capture.SDK.Data;
using System.Runtime.InteropServices;
using log4net;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using Microsoft.Win32;
using System.IO;

namespace SmartCAP.WFA.ClientLogging
{
    /// <summary>
    /// Implements the IACWorkflowAgent interface
    /// </summary>
    [ProgId("SmartCAP.WFA.ClientLogging")]
    public class WorkflowAgent : IACWorkflowAgent
    {
        public WorkflowAgent()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        #region module and state declaration
        private const string moduleRecognition = "fp.exe";
        private const string modulePDF = "kfxpdf.exe";
        private const string moduleOCRFullText = "ocr.exe";
        private const string moduleRelease = "release.exe";
        private const string moduleScan = "scan.exe";
        private const string moduleValidation = "index.exe";
        private const string moduleQC = "qc.exe";
        private const string moduleBatchManager = "ops.exe";
        private const string moduleKCNS = "acirsa.exe";
        //private const string moduleSmartCAP = "SmartCAP";

        private const string stateReady = "Ready";
        private const string stateError = "Error";
        private const string stateSuspended = "Suspended";
        private const string stateReserved = "Reserved";
        #endregion 

        /// <summary>
        /// Will be called by every module after a batch is closed 
        /// </summary>
        /// <param name="oWorkflowData"></param>
        public void ProcessWorkflow(ref IACWorkflowData oWorkflowData)
        {
            InitializeLogging();

            SmartWFABatch smartBatch = new SmartWFABatch(oWorkflowData);
            
            try
            {
                //React on leaving Validation module with state Ready
                if (smartBatch.KCWorkflowData.CurrentModule.ID == moduleValidation && smartBatch.KCWorkflowData.NextState.Name == stateReady)
                {
                    LeaveValidation(smartBatch);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in SmartCAP.WFA.ClientLogging: " + ex.Message);

                foreach (Document doc in smartBatch.Documents)
                {
                    doc.Rejected = true;    // Reject all documents in case of error
                    doc.Note = "Fehler in SmartCAP.WFA.ClientLogging - " + ex.Message;
                }

                // document will be routed with the error message to Quality Control
                throw new Exception("Error in SmartCAP.WFA.ClientLogging: " + ex.Message);
            }
        }

        private bool LeaveValidation(SmartWFABatch smartBatch)
        {
            List<CustomStorageString> settings = smartBatch.Setup.BatchClass.SetupCustomStorageStrings.Where(css => css.Name.StartsWith("SmartCAP.WFA.ClientLogging.")).ToList();
            if (settings.Count == 0) { throw new Exception("Es sind keine Einstellungen für diese Stapelklasse vorrhanden - bitte treffen Sie die Einstellungen für den WorkflowAgent"); }

            //string serverPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0", "ServerPath", null);
            //if (serverPath == null) { throw new Exception("Ser ServerPath ist in der Registry nicht gesetzt"); }
            string storagePath = settings.FirstOrDefault(s => s.Name.EndsWith("txtPath"))?.Value;
            if (storagePath == null) { throw new Exception("Der Speicherpfad ist nicht gesetzt - bitte richten Sie den WFA im Verwaltungsmodul für diese Stapelklasse ein"); }

            string targetDir = Path.Combine(storagePath, smartBatch.Name.Replace(":",".").Replace("/", ".").Replace("\\", ".").Replace("*", ".").Replace("?", ".").Replace("\"", ".").Replace("<", ".").Replace(">", ".").Replace("|", "."));
            if (Directory.Exists(targetDir)) { Directory.Delete(targetDir); }
            Directory.CreateDirectory(targetDir);

            string logFile = Path.Combine(targetDir, DateTime.Now.ToString("yyyyMMdd") + "_Validierung.log");
            if (File.Exists(logFile)) { File.Delete(logFile); }

            foreach (Document doc in smartBatch.Documents)
            {
                try
                {
                    File.AppendAllText(logFile, doc.UniqueDocumentID + ";" + doc.FormTypeName + ";" + doc.IndexFields.First(i => i.Name.ToLower() == "fallzahl").Value + ";" + doc.IndexFields.First(i => i.Name.ToLower() == "dokumentierendeoe").Value + ";" +
                                                doc.IndexFields.First(i => i.Name.ToLower() == "aufnahmedatum").Value + ";" + doc.IndexFields.First(i => i.Name.ToLower() == "fachlicheoe").Value + ";" + doc.IndexFields.First(i => i.Name.ToLower() == "dokumenttyp").Value + "\r\n");
                }
                catch (Exception ex)
                {
                    doc.Rejected = true;
                    doc.Note = "Fehler in SmartCAP.WFA.ClientLogging - " + ex.Message;
                }       
            }
              
            return true;
        }
    }

}
