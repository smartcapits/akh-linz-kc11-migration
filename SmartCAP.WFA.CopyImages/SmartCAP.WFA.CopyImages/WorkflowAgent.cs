﻿using System;
using Kofax.Capture.SDK.Workflow;
using Kofax.Capture.SDK.Data;
using System.Runtime.InteropServices;
using log4net;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using System.IO;

namespace SmartCAP.WFA.CopyImages
{
    /// <summary>
    /// Implements the IACWorkflowAgent interface
    /// </summary>
    [ProgId("SmartCAP.WFA.CopyImages")]
    public class WorkflowAgent : IACWorkflowAgent
    {
        public WorkflowAgent()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        #region module and state declaration
        private const string moduleRecognition = "fp.exe";
        private const string modulePDF = "kfxpdf.exe";
        private const string moduleOCRFullText = "ocr.exe";
        private const string moduleRelease = "release.exe";
        private const string moduleScan = "scan.exe";
        private const string moduleValidation = "index.exe";
        private const string moduleQC = "qc.exe";
        private const string moduleBatchManager = "ops.exe";
        private const string moduleKCNS = "acirsa.exe";
        //private const string moduleSmartCAP = "SmartCAP";

        private const string stateReady = "Ready";
        private const string stateError = "Error";
        private const string stateSuspended = "Suspended";
        private const string stateReserved = "Reserved";
        #endregion 

        /// <summary>
        /// Will be called by every module after a batch is closed 
        /// </summary>
        /// <param name="oWorkflowData"></param>
        public void ProcessWorkflow(ref IACWorkflowData oWorkflowData)
        {
            InitializeLogging();

            SmartWFABatch smartBatch = new SmartWFABatch(oWorkflowData);

            try
            {
                //React on entering Validation module
                if (smartBatch.KCWorkflowData.CurrentModule.ID == moduleRecognition && smartBatch.KCWorkflowData.NextState.Name == stateReady)
                {
                    LeaveRecognition(smartBatch);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in SmartCAP.WFA.CopyImages: " + ex.Message);

                foreach (Document doc in smartBatch.Documents)
                {
                    doc.Rejected = true;    // Reject all documents in case of error
                    doc.Note = "Fehler in SmartCAP.WFA.CopyImages - " + ex.Message;
                }

                // document will be routed with the error message to Quality Control
                throw new Exception("Error in SmartCAP.WFA.CopyImages: " + ex.Message);
            }
        }

        private bool LeaveRecognition(SmartWFABatch smartBatch)
        {
            string targetPath;

            // Load settings
            if (smartBatch.Setup.BatchClass.SetupCustomStorageStrings.Any(css => css.Name == "SmartCAP.WFA.CopyImages.txtTargetPath"))
            { 
                targetPath = smartBatch.Setup.BatchClass.SetupCustomStorageStrings.First(css => css.Name == "SmartCAP.WFA.CopyImages.txtTargetPath").Value;
                if (targetPath == null || targetPath == "") { throw new Exception("Bitte setzen Sie das Zielverzeichnis in der Setup-Maske der Stapelklasse"); }
            }
            else 
            { throw new Exception("Bitte setzen Sie das Zielverzeichnis in der Setup-Maske der Stapelklasse"); }

            // Prepare targetPath
            targetPath = Path.Combine(targetPath, smartBatch.UniqueBatchID);  
            if (Directory.Exists(targetPath)) { Directory.Delete(targetPath, true); }

            // Copy all subFolders and files of Images-Directory to targetPath (targetPath will automatically be created)
            SmartCopyTools.CopyWholeDirectory(smartBatch.ImagesFolder, targetPath);

            return true;
        }
    }

}
