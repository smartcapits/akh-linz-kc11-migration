﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CM.AssignBatchClass
{
    public class CustomModuleSettings
    {
        public string SettingsCSSName { get; set; } = "SmartCAP.CM.AssignBatchClass.Settings";

        // Properties
        public string AssignmentIndexField { get; set; } = "";
        public string DefaultBatchClass { get; set; } = "";
        public int DeleteFirstPageAtDefaultBatchClass { get; set; } = 0;
        public int GroupSameDocumentsInOneBatch { get; set; } = 0;
        public List<BatchClassAssignment> BatchClassAssignments { get; set; } = new List<BatchClassAssignment>();
    }

    public class BatchClassAssignment
    {
        public string BatchClassName { get; set; } = "";
        public string IndexRegEx { get; set; } = "";
        public int DeleteFirstPage { get; set; } = 0;
    }
}
