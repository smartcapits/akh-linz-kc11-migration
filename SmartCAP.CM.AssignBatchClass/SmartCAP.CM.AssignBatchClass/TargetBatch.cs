﻿using Kofax.Capture.SDK.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartCAP.SmartKCBatch;

namespace SmartCAP.CM.AssignBatchClass
{
    public enum AddMode { CreateNewBatch, AddToBatch }

    public class TargetBatches
    {
        public List<TargetBatch> Batches { get; set; } = new List<TargetBatch>();

        public void AddDocument(Document document, string batchClass, bool deleteFirstPage, AddMode addMode)
        {
            switch (addMode)
            {
                case AddMode.CreateNewBatch:
                    // Neuen Stapel erstellen egal ob es einen Stapel gleicher Klasse schon gibt
                    Batches.Add(new TargetBatch(batchClass, new List<Document> { document }, deleteFirstPage));
                    break;
                case AddMode.AddToBatch:
                    // Dokument einem existierenden Stapel zufügen wenn es einen der gewünschten Klasse bereits gibt. Sonst neuen Stapel erstellen
                    TargetBatch existingBatch = GetBatchOfClass(batchClass);
                    if (existingBatch != null) { existingBatch.Documents.Add(document); }
                    else { Batches.Add(new TargetBatch(batchClass, new List<Document> { document }, deleteFirstPage)); }
                    break;
                default:
                    break;
            }
        }


        private TargetBatch GetBatchOfClass(string batchClass)
        {
            foreach (TargetBatch batch in Batches) { if (batch.BatchClass == batchClass) { return batch; } }
            return null;
        }

    }

    public class TargetBatch
    {
        public string BatchClass { get; set; } = "";
        public List<Document> Documents { get; set; } = new List<Document>();
        public bool DeleteFirstPage { get; set; } = false;

        public TargetBatch() { }
        public TargetBatch(string batchClass, List<Document> documents, bool deleteFirstPage)
        {
            BatchClass = batchClass;
            Documents = documents;
            DeleteFirstPage = deleteFirstPage;
        }
    }
}
