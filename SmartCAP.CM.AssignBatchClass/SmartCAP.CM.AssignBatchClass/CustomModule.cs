﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using log4net.Core;
using Microsoft.Win32;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SmartCAP.CM.AssignBatchClass
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }
    public class DocumentEventArgs : EventArgs
    {
        public Document Document { get; private set; }

        public DocumentEventArgs(Document document)
        {
            Document = document;
        }
    }
    public class ProgressBarEventArgs : EventArgs
    {
        public int MinValue { get; private set; }
        public int MaxValue { get; private set; }
        public int CurrentValue { get; private set; }

        public ProgressBarEventArgs(int minValue = -1, int maxValue = -1, int currentValue = -1)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            CurrentValue = currentValue;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    public delegate void DocumentEventHandler(object sender, DocumentEventArgs e);
    public delegate void ProgressBarHandler(object sender, ProgressBarEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        private enum BatchStatus { OK, SkipValidation, Error }
        private enum DocumentStatus { OK, SkipValidation, Reject, Error }

        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CM.AssignBatchClass";
        public static string DisplayName = "SmartCAP.CM.AssignBatchClass Service";
        public static string Description = "Kofax Capture Service for Custom Module SmartCAP.CM.AssignBatchClass";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;
        private static bool _debugMode = false;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event DocumentEventHandler DocumentOpened;
        public event DocumentEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;
        public event TextEventHandler LogEvent;
        public event ProgressBarHandler ProgressBarUpdate;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value) || value == "0") { return true; }       // DisableBatchNotification key does not exist or is set to "0" - batch notification is enabled
            else { return false; }                                                  // DisableBatchNotificationKey exists - batch notification is disabled
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CustomModule.AssignBatchClass";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CM.AssignBatchClass", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        public void Logout()
        {
            if (session != null)
            {
                login.Logout();
                session = null;
            }
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null) { throw new Exception("you need to login to KC first"); }

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here is where all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            BatchStatus batchStatus = BatchStatus.SkipValidation;
            TargetBatches _targetBatches = new TargetBatches();
            string errorCode = "";

            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                Log.Debug("Initialize SmartBatch");
                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);

                Log.Debug("Loading settings");
                CustomModuleSettings settings = new CustomModuleSettings();
                CustomStorageString settingsCSS = smartBatch.Setup.BatchClass.SetupCustomStorageStrings.FirstOrDefault(css => css.Name == settings.SettingsCSSName);
                if (settingsCSS != null && settingsCSS.Value != "") { settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsCSS.Value, typeof(CustomModuleSettings)); }
                else { throw new Exception("Cannot load Settings-CSS '" + settings.SettingsCSSName + "' - please configure CustomModule 'SmartCAP.CM.AssignBatchClass' for this BatchClass"); }
  

                Log.Debug("Processing documents");
                smartBatch.Documents.ForEach(d =>
                {
                    Log.Debug($"Document {d.OrderNumber} opened");
                    LogDocumentOpened(d); // Log the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode                    

                    // Only process non-rejected documents
                    if (d.Rejected)
                    {
                        errorCode = "There are rejected documents present in batch - processing skipped";
                        batchStatus = BatchStatus.Error;
                    }
                    else
                    {
                        // Process document
                        string batchClassName = "";
                        bool deleteFirstPage = false;

                        // Get assignment index value
                        IndexField assignmentIF = d.IndexFields.FirstOrDefault(iF => iF.Name == settings.AssignmentIndexField);
                        if (assignmentIF != null)
                        {
                            // Match assignment value aganinst RegEx for batch class assignment
                            BatchClassAssignment bca = settings.BatchClassAssignments.FirstOrDefault(ba => Regex.IsMatch(assignmentIF.Value, ba.IndexRegEx));
                            if (bca != null)
                            {
                                deleteFirstPage = bca.DeleteFirstPage == 1 ? true : false;
                                batchClassName = bca.BatchClassName;
                                cw("Assigning document to batch class '" + batchClassName + "' (based on assignment value '" + assignmentIF.Value + "')");
                            }
                        }

                        // Assign default batch class if no assignment has been found
                        if (batchClassName == "" && settings.DefaultBatchClass != "")
                        {
                            batchClassName = settings.DefaultBatchClass;
                            deleteFirstPage = settings.DeleteFirstPageAtDefaultBatchClass == 1 ? true : false;
                            cw("Assigning document to default batch class '" + batchClassName + "'");
                        }

                        // Error if we still dont have a batch class assignment
                        if (batchClassName == "")
                        {
                            d.Rejected = true;
                            d.Note = "No batch class assignment found for document and no default batch class specified (assignment value: '" + assignmentIF.Value + "')";
                            batchStatus = BatchStatus.Error;
                            cw($"No batch class assignment found for document {d.OrderNumber} and no default batch class specified (assignment value: '" + assignmentIF.Value + "')");
                        }
                        else
                        {
                            // Add document to list of target batches
                            cw("Adding document to list ob targetBatches");
                            _targetBatches.AddDocument(d, batchClassName, deleteFirstPage, settings.GroupSameDocumentsInOneBatch == 1 ? AddMode.AddToBatch : AddMode.CreateNewBatch);
                        }

                        LogDocumentClosed(d); // Log the document closing to the event list, if the custom module is executed in interactive mode
                    }
                });


                // Create target batches, move documents 
                foreach (TargetBatch targetBatch in _targetBatches.Batches)
                {
                    // Create new target batch
                    SmartCMBatch newBatch = smartBatch.CreateBatch(targetBatch.BatchClass, smartBatch.Name + " - Assigned " + DateTime.Now.ToString("HH:mm:ss.fff"));
                    cw("New batch of class '" + targetBatch.BatchClass + "' created: '" + newBatch.Name + "'");

                    // Move document pages to loose pages in new batch
                    foreach (Document doc in targetBatch.Documents)
                    {
                        try
                        {
                            // Only move non-rejected documents (there should not be any rejected docs at this stage but just to make sure)
                            if (doc.Rejected)
                            { 
                                cw($"Skipping moving of rejected document {doc.OrderNumber} to target batch" ); 
                            }
                            else
                            {
                                // Move pages of document to new batch. If skip first page is marked, start move at 2nd page
                                for (int pageNr = targetBatch.DeleteFirstPage ? 0 : 1; pageNr < doc.Pages.Count; pageNr++)
                                {
                                    doc.Pages[pageNr].MoveToBatch(newBatch);
                                }

                                // Delete source document after move
                                cw("Deleting source document " + doc.OrderNumber + " from source batch");
                                doc.Delete();
                            }
                        }
                        catch (Exception ex)
                        {
                            doc.Rejected = true;
                            doc.Note = "Error moving document to target batch '" + newBatch.Name + "' - Msg: " + ex.Message;
                            batchStatus = BatchStatus.Error;
                        }
                    }

                    // Inherit batch field values
                    foreach (BatchField bField in smartBatch.BatchFields)
                    {
                        BatchField targetBF = newBatch.BatchFields.FirstOrDefault(bF => bF.Name == bField.Name);
                        if (targetBF != null) { targetBF.Value = bField.Value; }
                    }

                    // Close new batch or delete it if it has no pages
                    if (newBatch.LoosePages.Count == 0)
                    {
                        cw($"Deleting empty target batch '{newBatch.Name}'");
                        newBatch.Delete();
                    }
                    else
                    {
                        cw($"Closing target batch '{newBatch.Name}'");
                        newBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext);
                    }
                }


                // Close or delete source batch (if its empty)
                if (smartBatch.Documents.Count == 0 && smartBatch.LoosePages.Count == 0)
                {
                    Log.Debug("Deleting empty batch");
                    smartBatch.Delete();
                }
                else if (batchStatus != BatchStatus.Error)
                {
                    Log.Debug("Closing batch");
                    smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext);
                }
                else
                {
                    Log.Debug("Moving batch to QC");
                    smartBatch.Close(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, errorCode);
                }

                smartBatch = null;
                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                }
            }
        }

        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here is where the action happens 
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                // suppress duplicate key error
                if (!ex.Message.Contains("Duplicate key value supplied."))
                {
                    Log.Error(message, ex);
                    ErrorOccured?.Invoke(this, new TextEventArgs(message));
                }
                else
                {
                    Log.Error("Suppressed error: " + message, ex);
                }
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }
        }

        #endregion processing methods

        #region GUIhelpers

        /// <summary>
        /// Logs a message to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            LogEvent?.Invoke(this, new TextEventArgs(message));
        }

        private static void cw(string message, bool isDebugInfo = false)
        {
            // Gibt die message in der Console aus. Ist Debug-Parameter "-debug" beim Programmaufruf gesetzt dann wierden auch als DebugInfo gekennzeichnete Infos ausgegeben
            if (!isDebugInfo || _debugMode) { Console.WriteLine(DateTime.Now + ": " + message); }
        }

        /// <summary>
        /// Logs the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentOpened(Document document)
        {
            DocumentOpened?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Logs the document closing to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentClosed(Document document)
        {
            DocumentClosed?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Updates the progress bar - handy in case the number of documents has changed
        /// </summary>
        /// <param name="currentValue"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        private void UpdateProgressBar(int maxValue = -1, int currentValue = -1, int minValue = -1)
        {
            ProgressBarUpdate?.Invoke(this, new ProgressBarEventArgs(minValue, maxValue, currentValue));
        }

        #endregion
    }
}
