﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SmartCAP.CM.AssignBatchClass
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IAdminApplication _adminApp;
        public IBatchClass BatchClass;

        public SetupForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(IAdminApplication adminApplication)
        {
            _adminApp = adminApplication;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }
        #endregion

        public void InitializePanel()
        {
            // Check for ActiveBatchClass to only have 1 document class
            if (_adminApp.ActiveBatchClass.DocumentClasses.Count != 1) { throw new Exception("Only 1 document class is supported in this batch class"); }

            // Initialize IndexFields 
            cboIndexField.Items.Clear();
            foreach (DocumentClass docClass in _adminApp.ActiveBatchClass.DocumentClasses) { foreach (IndexField iF in docClass.IndexFields) { cboIndexField.Items.Add(iF.Name); }}

            // Initialize DefaultBatchClass 
            cboDefaultBatchClass.Items.Clear();
            cboDefaultBatchClass.Items.Add("");    // empty entry so no default batch class can be selected
            foreach (BatchClass batchClass in _adminApp.BatchClasses) { cboDefaultBatchClass.Items.Add(batchClass.Name); }

            try
            {
                dgvBatchClassAssignments.ColumnCount = 2;
                dgvBatchClassAssignments.Columns[0].Name = "Batch Class";
                dgvBatchClassAssignments.Columns[1].Name = "Trigger RegEx";
                DataGridViewCheckBoxColumn dgvCBColumn = new DataGridViewCheckBoxColumn();
                dgvCBColumn.Name = "Delete first page";
                dgvBatchClassAssignments.Columns.Add(dgvCBColumn);
                dgvBatchClassAssignments_Resize(null, null);

                // Load all published batch classes to BatchClassAssignemnts
                foreach (BatchClass batchClass in _adminApp.BatchClasses)
                {
                    {
                        string[] row = new string[] { batchClass.Name, "" };
                        int rowNr = dgvBatchClassAssignments.Rows.Add(row);
                        dgvBatchClassAssignments.Rows[rowNr].Cells[0].ReadOnly = true;      // Set first column tio ReadOnly
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show("Error initializing SetupForm for SmartCAP.CM.AssignBatchClass - Msg: " + ex.Message, "Setup-Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        #region Settings
        public void LoadSettings()
        {
            CustomModuleSettings settings = new CustomModuleSettings();

            string settingsXML = GetCustomStorageString(settings.SettingsCSSName);
            if (settingsXML != "")
            {
                settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsXML, typeof(CustomModuleSettings));

                // Set static values
                if (settings.AssignmentIndexField != "" && cboIndexField.Items.Contains(settings.AssignmentIndexField)) { cboIndexField.Text = settings.AssignmentIndexField; }
                if (settings.DefaultBatchClass != "" && cboDefaultBatchClass.Items.Contains(settings.DefaultBatchClass)) { cboDefaultBatchClass.Text = settings.DefaultBatchClass; }
                chkDefaultBCDeleteFirstPage.Checked = settings.DeleteFirstPageAtDefaultBatchClass == 1 ? true : false;
                rbGroupDocuments.Checked = settings.GroupSameDocumentsInOneBatch == 1 ? true : false;
                rbSingleDocumentBatches.Checked = settings.GroupSameDocumentsInOneBatch != 1 ? true : false;

                // Set dynamic batch class assignment values
                foreach (DataGridViewRow row in dgvBatchClassAssignments.Rows)
                {
                    BatchClassAssignment batchAssignment = settings.BatchClassAssignments.FirstOrDefault(x => x.BatchClassName == row.Cells[0].Value.ToString());
                    if (batchAssignment != null)
                    {
                        row.Cells[1].Value = batchAssignment.IndexRegEx;
                        row.Cells[2].Value = batchAssignment.DeleteFirstPage == 1 ? true : false;
                    }
                }
            }
        }

        public bool VerifySettings()
        {
            string errorMsg = "";

            try
            {
                if (cboIndexField.Text == "") { errorMsg += "Please select an index field for batch class assignment"; }

                // Check for invalid RegEx in Assignment matirx
                foreach (DataGridViewRow row in dgvBatchClassAssignments.Rows)
                {
                    if (row.Cells[1].Style.ForeColor == Color.Red)
                    {
                        errorMsg += "Invalid RegEx / Settings detected\r\n";
                        row.Cells[1].Selected = true;
                        break;
                    }
                }

                if (errorMsg != "") { throw new ConditionValidationException("Please correct the following errors:\r\n" + errorMsg); }
            }
            catch (ConditionValidationException cve)
            {
                MessageBox.Show(cve.Message, "Error saving settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (Exception)
            { throw; }

            return true;
        }

        public void SaveSettings()
        {
            CustomModuleSettings settings = new CustomModuleSettings();

            // Save static values
            settings.AssignmentIndexField = cboIndexField.Text;
            settings.DefaultBatchClass = cboDefaultBatchClass.Text;
            settings.DeleteFirstPageAtDefaultBatchClass = chkDefaultBCDeleteFirstPage.Checked ? 1 : 0;
            settings.GroupSameDocumentsInOneBatch = rbGroupDocuments.Checked ? 1 : 0;

            // Save dynamic batch class assignment values
            settings.BatchClassAssignments.Clear();
            foreach (DataGridViewRow row in dgvBatchClassAssignments.Rows)
            {
                // Wenn Barcode-Feld gefüllt ist dann Zuweisung in Settings speichern
                if (row.Cells[1].Value != null && row.Cells[1].Value.ToString() != "") 
                { settings.BatchClassAssignments.Add(new BatchClassAssignment() { BatchClassName = row.Cells[0].Value.ToString(), IndexRegEx = row.Cells[1].Value.ToString(), 
                                                                                  DeleteFirstPage = row.Cells[2].Value != null && row.Cells[2].Value.ToString() == true.ToString() ? 1 : 0 }); }
            }

            SetCustomStorageString(settings.SettingsCSSName, SerializationTool.Serialize(settings));    // Write Settings as serialized XML to Batch CSS
        }
        #endregion


        #region UserInteraction
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btnTest_Click(object sender, EventArgs e)
        {
            int matchCount = 0;

            if (txtTestValue.Text != "")
            {
                foreach (DataGridViewRow row in dgvBatchClassAssignments.Rows)
                {
                    row.Cells[0].Style.BackColor = Color.White;

                    if (row.Cells[1].Value != null && row.Cells[1].Value.ToString() != "" && row.Cells[1].Style.ForeColor != Color.Red)
                    {
                        if (Regex.IsMatch(txtTestValue.Text, row.Cells[1].Value.ToString())) { row.Cells[0].Style.BackColor = Color.LightGreen; matchCount++; }
                    }
                }

                lblMatches.Text = matchCount.ToString();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvBatchClassAssignments.Rows) { row.Cells[0].Style.BackColor = Color.White; }
            lblMatches.Text = "0";
        }

        private void dgvBatchClassAssignments_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Check for valid RegEx on field change
            if (e.ColumnIndex == 1)
            {
                if (dgvBatchClassAssignments.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null) 
                { dgvBatchClassAssignments.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black; }
                else
                {
                    try
                    {
                        if (Regex.IsMatch("TEST", dgvBatchClassAssignments.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString())) { }
                        dgvBatchClassAssignments.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    }
                    catch (Exception)
                    { dgvBatchClassAssignments.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Red; }
                }
            }
        }

        private void dgvBatchClassAssignments_Resize(object sender, EventArgs e)
        {
            dgvBatchClassAssignments.Columns[2].Width = 70;

            if (dgvBatchClassAssignments.ScrollBars == ScrollBars.Vertical)
            {
                dgvBatchClassAssignments.Columns[0].Width = (dgvBatchClassAssignments.Width - 77 - SystemInformation.VerticalScrollBarWidth) / 2;
                dgvBatchClassAssignments.Columns[1].Width = dgvBatchClassAssignments.Width - 77 - SystemInformation.VerticalScrollBarWidth - dgvBatchClassAssignments.Columns[0].Width;
            }
            else
            {
                dgvBatchClassAssignments.Columns[0].Width = (dgvBatchClassAssignments.Width - 77) / 2;
                dgvBatchClassAssignments.Columns[1].Width = dgvBatchClassAssignments.Width - 77 - dgvBatchClassAssignments.Columns[0].Width;
            }
        }
        #endregion


        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            { BatchClass.set_CustomStorageString(name, value); }
            catch
            { }
        }

        private string GetCustomStorageString(string name, string defaultValue = "")
        {
            try
            { return BatchClass.get_CustomStorageString(name); }
            catch
            { return defaultValue; }
        }


        #endregion

        
    }
}
