﻿using Kofax.Capture.AdminModule.InteropServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SmartCAP.CM.AssignBatchClass
{
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ISetupForm
    {
        [DispId(1)]
        AdminApplication Application { set; }
        [DispId(2)]
        void ActionEvent(int EventNumber, object Argument, out int Cancel);
    }

    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("SmartCAP.CM.AssignBatchClass.Setup")]
    public class CustomModuleSetup : UserControl, ISetupForm
    {
        private AdminApplication adminApplication;

        public AdminApplication Application
        {
            set
            {
                value.AddMenu("SmartCAP.CM.AssignBatchClass.Setup", "SmartCAP.CM.AssignBatchClass - Setup", "BatchClass");
                adminApplication = value;
            }
        }

        public void ActionEvent(int EventNumber, object Argument, out int Cancel)
        {
            Cancel = 0;

            if ((KfxOcxEvent)EventNumber == KfxOcxEvent.KfxOcxEventMenuClicked && (string)Argument == "SmartCAP.CM.AssignBatchClass.Setup")
            {
                SetupForm form = new SetupForm();
                form.ShowDialog(adminApplication);
            }
        }

    }
}
