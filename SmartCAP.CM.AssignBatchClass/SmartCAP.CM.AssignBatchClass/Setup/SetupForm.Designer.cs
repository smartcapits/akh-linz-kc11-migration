﻿namespace SmartCAP.CM.AssignBatchClass
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbGroupDocuments = new System.Windows.Forms.RadioButton();
            this.rbSingleDocumentBatches = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblMatches = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtTestValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvBatchClassAssignments = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkDefaultBCDeleteFirstPage = new System.Windows.Forms.CheckBox();
            this.cboDefaultBatchClass = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboIndexField = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchClassAssignments)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(632, 658);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(137, 54);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(787, 658);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 54);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.rbGroupDocuments);
            this.groupBox3.Controls.Add(this.rbSingleDocumentBatches);
            this.groupBox3.Location = new System.Drawing.Point(515, 293);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(371, 105);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Batch creation settings";
            // 
            // rbGroupDocuments
            // 
            this.rbGroupDocuments.AutoSize = true;
            this.rbGroupDocuments.Location = new System.Drawing.Point(9, 65);
            this.rbGroupDocuments.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbGroupDocuments.Name = "rbGroupDocuments";
            this.rbGroupDocuments.Size = new System.Drawing.Size(296, 24);
            this.rbGroupDocuments.TabIndex = 6;
            this.rbGroupDocuments.TabStop = true;
            this.rbGroupDocuments.Text = "Group same documents in one batch";
            this.rbGroupDocuments.UseVisualStyleBackColor = true;
            // 
            // rbSingleDocumentBatches
            // 
            this.rbSingleDocumentBatches.AutoSize = true;
            this.rbSingleDocumentBatches.Checked = true;
            this.rbSingleDocumentBatches.Location = new System.Drawing.Point(9, 29);
            this.rbSingleDocumentBatches.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbSingleDocumentBatches.Name = "rbSingleDocumentBatches";
            this.rbSingleDocumentBatches.Size = new System.Drawing.Size(296, 24);
            this.rbSingleDocumentBatches.TabIndex = 5;
            this.rbSingleDocumentBatches.TabStop = true;
            this.rbSingleDocumentBatches.Text = "Create new batch for each document";
            this.rbSingleDocumentBatches.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblMatches);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Controls.Add(this.btnTest);
            this.groupBox2.Controls.Add(this.txtTestValue);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(515, 430);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(371, 207);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test assignment";
            // 
            // lblMatches
            // 
            this.lblMatches.AutoSize = true;
            this.lblMatches.Location = new System.Drawing.Point(89, 172);
            this.lblMatches.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMatches.Name = "lblMatches";
            this.lblMatches.Size = new System.Drawing.Size(18, 20);
            this.lblMatches.TabIndex = 3;
            this.lblMatches.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 172);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 20);
            this.label6.TabIndex = 3;
            this.label6.Text = "Matches:";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(14, 106);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(93, 43);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(154, 106);
            this.btnTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(212, 43);
            this.btnTest.TabIndex = 9;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // txtTestValue
            // 
            this.txtTestValue.Location = new System.Drawing.Point(14, 66);
            this.txtTestValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTestValue.Name = "txtTestValue";
            this.txtTestValue.Size = new System.Drawing.Size(350, 26);
            this.txtTestValue.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 42);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Assignment Value:";
            // 
            // dgvBatchClassAssignments
            // 
            this.dgvBatchClassAssignments.AllowUserToAddRows = false;
            this.dgvBatchClassAssignments.AllowUserToDeleteRows = false;
            this.dgvBatchClassAssignments.AllowUserToOrderColumns = true;
            this.dgvBatchClassAssignments.AllowUserToResizeColumns = false;
            this.dgvBatchClassAssignments.AllowUserToResizeRows = false;
            this.dgvBatchClassAssignments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBatchClassAssignments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBatchClassAssignments.Location = new System.Drawing.Point(13, 14);
            this.dgvBatchClassAssignments.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvBatchClassAssignments.MultiSelect = false;
            this.dgvBatchClassAssignments.Name = "dgvBatchClassAssignments";
            this.dgvBatchClassAssignments.RowHeadersWidth = 25;
            this.dgvBatchClassAssignments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvBatchClassAssignments.Size = new System.Drawing.Size(489, 702);
            this.dgvBatchClassAssignments.TabIndex = 13;
            this.dgvBatchClassAssignments.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatchClassAssignments_CellValueChanged);
            this.dgvBatchClassAssignments.Resize += new System.EventHandler(this.dgvBatchClassAssignments_Resize);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.chkDefaultBCDeleteFirstPage);
            this.groupBox4.Controls.Add(this.cboDefaultBatchClass);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.cboIndexField);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(515, 14);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(371, 265);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Assignament settings";
            // 
            // chkDefaultBCDeleteFirstPage
            // 
            this.chkDefaultBCDeleteFirstPage.AutoSize = true;
            this.chkDefaultBCDeleteFirstPage.Location = new System.Drawing.Point(14, 231);
            this.chkDefaultBCDeleteFirstPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkDefaultBCDeleteFirstPage.Name = "chkDefaultBCDeleteFirstPage";
            this.chkDefaultBCDeleteFirstPage.Size = new System.Drawing.Size(152, 24);
            this.chkDefaultBCDeleteFirstPage.TabIndex = 4;
            this.chkDefaultBCDeleteFirstPage.Text = "Delete first page";
            this.chkDefaultBCDeleteFirstPage.UseVisualStyleBackColor = true;
            // 
            // cboDefaultBatchClass
            // 
            this.cboDefaultBatchClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDefaultBatchClass.FormattingEnabled = true;
            this.cboDefaultBatchClass.Location = new System.Drawing.Point(14, 142);
            this.cboDefaultBatchClass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboDefaultBatchClass.Name = "cboDefaultBatchClass";
            this.cboDefaultBatchClass.Size = new System.Drawing.Size(350, 28);
            this.cboDefaultBatchClass.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 198);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(277, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "-> non-assignable document go to QC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 178);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(306, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "In case no default batch class  is specified";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 117);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Default batch class";
            // 
            // cboIndexField
            // 
            this.cboIndexField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIndexField.FormattingEnabled = true;
            this.cboIndexField.Location = new System.Drawing.Point(14, 65);
            this.cboIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboIndexField.Name = "cboIndexField";
            this.cboIndexField.Size = new System.Drawing.Size(350, 28);
            this.cboIndexField.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 40);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(275, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "IndexField for batch class assignment";
            // 
            // SetupForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(894, 726);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvBatchClassAssignments);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.Text = "SmartCAP.CM.AssignBatchClass.Setup";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchClassAssignments)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbGroupDocuments;
        private System.Windows.Forms.RadioButton rbSingleDocumentBatches;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblMatches;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TextBox txtTestValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvBatchClassAssignments;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chkDefaultBCDeleteFirstPage;
        private System.Windows.Forms.ComboBox cboDefaultBatchClass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboIndexField;
        private System.Windows.Forms.Label label9;
    }
}