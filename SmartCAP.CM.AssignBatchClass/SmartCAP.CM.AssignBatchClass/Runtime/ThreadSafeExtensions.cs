﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCAP.CM.AssignBatchClass
{
    public static class ThreadSafeOperations
    {
        /// <summary>
        /// Executes the defined action on the current control thread-safe
        /// </summary>
        /// <param name="form"></param>
        /// <param name="ctrl"></param>
        /// <param name="fn"></param>
        public static void ExecuteThreadSafe(this Form form, Control ctrl, Action<Control> fn)
        {
            ExecuteSafe(form, ctrl, fn);
        }

        private delegate void ControlActionDelegate(Form f, Control ctrl, Action<Control> fn);

        private static void ExecuteSafe(Form form, Control ctrl, Action<Control> fn)
        {
            if (ctrl.InvokeRequired)
            {
                ControlActionDelegate d = new ControlActionDelegate(ExecuteSafe);
                form.Invoke(d, new object[] { form, ctrl, fn });
            }
            else
            {
                fn(ctrl);
            }
        }
    }
}
