﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCAP.CustomModule.Patientenakt
{
    public partial class RuntimeForm : Form
    {
        CustomModule cm;
        BackgroundWorker worker;
        int batchId;

        public RuntimeForm(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeComponent();

            batchId = GetBatchIdFromArgs(args);

            // login to KC
            cm = new CustomModule();
            cm.Login("", "", true);

            // add progress event handlers
            cm.BatchOpened += Cm_BatchOpened;
            cm.BatchClosed += Cm_BatchClosed;
            cm.DocumentOpened += Cm_DocumentOpened;
            cm.DocumentClosed += Cm_DocumentClosed;
            cm.ErrorOccured += Cm_ErrorOccured;
            cm.LogEvent += Cm_LogEvent;

            // process batch/batches immediately / on demand (in background thread so, that the form does not freeze)
            worker = new BackgroundWorker();
            worker.DoWork += (s, a) => Process();
            worker.RunWorkerAsync();

            // poll/listen for batches
            if (CustomModule.BatchNotificationEnabled == true)
                cm.ListenForNewBatches();
            else
                cm.PollForNewBatches();
        }

        private void Cm_LogEvent(object sender, TextEventArgs e)
        {
            string log = String.Format("{0} {1}", DateTime.Now.ToString("HH:mm:ss"), e.Text);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
        }

        private void btnPollNow_Click(object sender, EventArgs e)
        {
            // process in background thread, so that the form does not freeze
            worker.RunWorkerAsync();
        }

        private void Process()
        {
            if (batchId > -1)
            {
                // started from batch manager
                cm.ProcessSpecificBatch(batchId);
                Application.Exit();
            }
            else
            {
                // started as module
                cm.ProcessBatches();
            }
        }

        private int GetBatchIdFromArgs(string[] args)
        {
            int batchId = -1;
            foreach (string arg in args)
            {
                if (arg.StartsWith("-B"))
                {
                    int.TryParse(arg.Substring(2), out batchId);
                }
            }
            return batchId;
        }

        #region handle progress events
        private void Cm_BatchClosed(object sender, BatchEventArgs e)
        {
            string log = String.Format("{0} Closed batch: \"{1}\"",
                DateTime.Now.ToString("HH:mm:ss"),
                e.Batch.Name);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
            this.ExecuteThreadSafe(btnPollNow, x => x.Enabled = true);
        }

        private void Cm_BatchOpened(object sender, BatchEventArgs e)
        {
            this.ExecuteThreadSafe(btnPollNow, x => x.Enabled = false);
            this.ExecuteThreadSafe(progressBar1, x =>
            {
                ProgressBar p = (ProgressBar)x;
                p.Value = 0;
                p.Maximum = e.Batch.DocumentCount;
            });

            this.ExecuteThreadSafe(lblCurrentDocument, x => ((Label)x).Text = "0");
            this.ExecuteThreadSafe(lblDocumentCount, x => ((Label)x).Text = e.Batch.DocumentCount.ToString());
            this.ExecuteThreadSafe(lblBatchName, x => ((Label)x).Text = e.Batch.Name);
            this.ExecuteThreadSafe(lblBatchClass, x => ((Label)x).Text = e.Batch.BatchClassName);

            string log = String.Format("{0} Opened batch: \"{1}\"",
                DateTime.Now.ToString("HH:mm:ss"),
                e.Batch.Name);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
        }

        private void Cm_ErrorOccured(object sender, TextEventArgs e)
        {
            string log = String.Format("{0} Error occured: \"{1}\"",
                DateTime.Now.ToString("HH:mm:ss"),
                e.Text);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
        }

        private void Cm_DocumentClosed(object sender, DocumentEventArgs e)
        {
            string log = String.Format("{0} Closed document: #{1}",
                DateTime.Now.ToString("HH:mm:ss"),
                progressBar1.Value);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
        }

        private void Cm_DocumentOpened(object sender, DocumentEventArgs e)
        {
            this.ExecuteThreadSafe(progressBar1, x => ((ProgressBar)x).PerformStep());
            this.ExecuteThreadSafe(lblCurrentDocument, x => ((Label)x).Text = progressBar1.Value.ToString());
            this.ExecuteThreadSafe(lblDocumentClass, x => ((Label)x).Text = e.Document.DocumentClass.Name);
            this.ExecuteThreadSafe(lblFormType, x => ((Label)x).Text = e.Document.FormTypeName);

            string log = String.Format("{0} Opened document: #{1}",
                DateTime.Now.ToString("HH:mm:ss"),
                progressBar1.Value);
            this.ExecuteThreadSafe(lb_Events, x => ((ListBox)x).Items.Add(log));
        }

        #endregion handle progress events                               
    }
}
