﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartCAP.CustomModule.Patientenakt
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }

    public class DocumentEventArgs : EventArgs
    {
        public Document Document { get; private set; }

        public DocumentEventArgs(Document document)
        {
            Document = document;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    //public delegate void ACDataElementEventHandler(object sender, ACDataElementEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    public delegate void DocumentEventHandler(object sender, DocumentEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        private enum BatchStatus { NeedsValidation, SkipValidation, Error }
        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CustomModule.Patientenakt";
        public static string DisplayName = "SmartCAP.CustomModule.Patientenakt Service";
        public static string Description = "Kofax Capture Service for Custom Module SmartCAP.CustomModule.Patientenakt";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event DocumentEventHandler DocumentOpened;
        public event DocumentEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;
        public event TextEventHandler LogEvent;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value) || value == "0") { return true; }       // DisableBatchNotification key does not exist or is set to "0" - batch notification is enabled
            else { return false; }                                                  // DisableBatchNotificationKey exists - batch notification is disabled
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CustomModule.Patientenakt";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CustomModule.Patientenakt.exe", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null)
                throw new Exception("you need to login to KC first");

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here is where all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            BatchStatus batchStatus = BatchStatus.SkipValidation;
            const string ftArchivDeckblatt = "Deckblatt_Neu";
            //const string ftPatientenakt = "Patientenakt_Neu";
            string fallzahl = "";
            string dokumentierendeOE = "";
            string aufnahmedatum = "";
            string fachlicheOE = "";
            string trennblattBarcode = "";
            string selectedOMRField = "";
            string translatedID = "";
            bool docNeedsValidation = false;

            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                Log.Debug("Initialize SmartBatch");
                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);

                Log.Debug("Loading settings");
                CustomModuleSettings settings = new CustomModuleSettings();
                CustomStorageString settingsCSS = smartBatch.Setup.BatchClass.SetupCustomStorageStrings.FirstOrDefault(css => css.Name == settings.SettingsCSSName);
                if (settingsCSS != null && settingsCSS.Value != "") { settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsCSS.Value, typeof(CustomModuleSettings)); }
                else { throw new Exception("Cannot load Settings-CSS '" + settings.SettingsCSSName + "' - please configure CustomModule 'SmartCAP.CustomModule.Patientenakt' for this BatchClass"); }

                // Load Translation-Settings-File and deserialize it
                Log.Debug("Loading Translation-Settings-File \"" + settings.TranslationXMLFile + "\"");
                string translationSettingsXML = File.ReadAllText(settings.TranslationXMLFile);
                TranslationSettings translationSettings = (TranslationSettings)SerializationTool.DeSerialize(translationSettingsXML, typeof(TranslationSettings));

                // Schauen ob es ausgesetzte Dokumente im Stapel gibt. Wenn ja dann Verarbeitung nicht durchführen
                Log.Debug("Checking batch for rejected documents");
                smartBatch.Documents.ForEach(d =>
                {
                    // Nur nicht rejectete Dokumente verarbeiten - Exception bei rejectetem Dokument
                    if (d.Rejected) { throw new Exception("Es gibt mindestes ein ausgesetztes Dokument im Stapel. Die Verarbeitung wurde abgebrochen."); }
                });

                Log.Debug("Processing documents");
                int docIndex = 1;
                smartBatch.Documents.ForEach(d =>
                {
                    Log.Debug("Processing document " + docIndex.ToString() + " of " + smartBatch.Documents.Count.ToString());                    
                    LogDocumentOpened(d); // Log the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode                    
                 
                    docNeedsValidation = false;

                    // Checken auf 1. Dokument = Archiv-Deckblatt
                    if (docIndex == 1 && d.FormTypeName != ftArchivDeckblatt)
                    {
                        d.Rejected = true; 
                        d.Note = "Das erste Dokument muss vom Typ '" + ftArchivDeckblatt + "' sein";
                        throw new Exception("Das erste Dokument muss vom Typ '" + ftArchivDeckblatt + "' sein");
                    }


                    // Bei Deckblatt Barcode-Werte zur Vererbung auf Folgedokumente speichern
                    if (d.FormTypeName == ftArchivDeckblatt)
                    {
                        // Checken ob Indexfelder alle gefüllt sind (Barcodes wurden erkannt) - Merken der Indexfelder für Vererbung auf alle Patientenakt-Dokumente
                        fallzahl = d.IndexFields.First(iF => iF.Name == "Fallzahl").Value; if (fallzahl == "") { docNeedsValidation = true; }
                        dokumentierendeOE = d.IndexFields.First(iF => iF.Name == "DokumentierendeOE").Value; if (dokumentierendeOE == "") { docNeedsValidation = true; }
                        aufnahmedatum = d.IndexFields.First(iF => iF.Name == "Aufnahmedatum").Value; if (aufnahmedatum == "") { docNeedsValidation = true; }
                        fachlicheOE = d.IndexFields.First(iF => iF.Name == "FachlicheOE").Value; if (fachlicheOE == "") { docNeedsValidation = true; }
                    }
                    else
                    {
                        // Indexfelder von Archiv-Deckblatt auf Patientenakt-Dokument vererben
                        d.IndexFields.First(iF => iF.Name == "Fallzahl").Value = fallzahl;
                        d.IndexFields.First(iF => iF.Name == "DokumentierendeOE").Value = dokumentierendeOE;
                        d.IndexFields.First(iF => iF.Name == "Aufnahmedatum").Value = aufnahmedatum;
                        d.IndexFields.First(iF => iF.Name == "FachlicheOE").Value = fachlicheOE;

                        // Barcode-Feld auslesen
                        trennblattBarcode = d.IndexFields.First(iF => iF.Name == settings.BarcodeFieldName).Value;
                        if (trennblattBarcode.Length != 3)
                        {
                            docNeedsValidation = true;
                        }
                        else
                        {
                            // Trennblatt-Barcode-Wert in Dokumenttyp-Feld vererben
                            d.IndexFields.First(iF => iF.Name == "Dokumenttyp").Value = trennblattBarcode;

                            // Übersetzungstabelle für Barcode laden - Falls diese nicht exisitert Fehler werden
                            TranslationSettings.TranslationTable translationTable = SmartTools.GetTranslationTable(translationSettings, trennblattBarcode);
                            if (translationTable == null)
                            {
                                d.Rejected = true;
                                d.Note = "Für den Trennblatt-Barcode '" + trennblattBarcode + "' konnte keine Übersetzungstabelle in der Translation-XML-Datei gefunden werden!";
                                batchStatus = BatchStatus.Error;
                            }
                            else
                            {
                                // Checken ob keine oder nur 1 Angabe angekreuzt -> wenn nein Validierung
                                selectedOMRField = d.IndexFields.First(iF => iF.Name == settings.OMRFieldName).Value;
                                if (selectedOMRField.Length > 4)
                                {
                                    docNeedsValidation = true;      // Bei mehr als 4 Stellen wurden mind. 2 Felder angekreuzt
                                }
                                else if (selectedOMRField.Length == 0)
                                {
                                    // Default-Wert für Barcode aus Translation-Tabelle laden
                                    //if (translationTable.DefaultID == "") { KofaxFunctions.RejectDocument(document, "Für den Trennblatt-Barcode '" + trennblattBarcode + "' ist keine DefaultID in der Translation-XML-Datei eingetragen!"); processStatus = BatchStatus.Error; }
                                    //else { KofaxFunctions.SetIndexField(document, _settings.IDTargetField, translationTable.DefaultID); }
                                    d.IndexFields.First(iF => iF.Name == settings.IDTargetField).Value = translationTable.DefaultID;
                                }
                                else
                                {
                                    // Ein Wert angekreuzt - Auswahlfeld setzen
                                    d.IndexFields.First(iF => iF.Name == settings.OMRDisplayFieldName).Value = selectedOMRField;
                                    // Wert übersetzen
                                    translatedID = SmartTools.TranslateOMRtoID(translationTable, selectedOMRField);
                                    if (translatedID == null)
                                    {
                                        d.Rejected = true;
                                        d.Note = "Für den Trennblatt-Barcode '" + trennblattBarcode + "' und das Ankreuzfeld '" + selectedOMRField + "' konnte keine ID in der Translation-XML-Datei gefunden werden!";
                                        batchStatus = BatchStatus.Error;
                                    }
                                    else
                                    {
                                        d.IndexFields.First(iF => iF.Name == settings.IDTargetField).Value = translatedID;
                                    }
                                }
                            }
                        }
                    }


                    if (docNeedsValidation)
                    {
                        d.Validated = false;
                        d.Note = "Es müssen Indexwerte validiert werden";
                        if (batchStatus != BatchStatus.Error) { batchStatus = BatchStatus.NeedsValidation; }     // Default = SkipValidation. OK = normaler Weg in Validierung
                    }

                    LogDocumentClosed(d); // Log the document closing to the event list, if the custom module is executed in interactive mode
                    docIndex++;
                });

                if (batchStatus == BatchStatus.NeedsValidation)
                {
                    // Stapel muss validiert werden
                    Log.Debug("Closing batch -> Status: Needs Validation");
                    smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext);
                }
                else if (batchStatus == BatchStatus.SkipValidation)
                {
                    // Validierung überspringen - aber nur wenn Validierung auch zugewiesen ist. Sonst einfach ins nächste Modul gehen.
                    Log.Debug("Closing batch -> Status: Skip Validation");
                    if (smartBatch.Modules.Any(m => m.ID == "index.exe")) { smartBatch.Close(KfxDbState.KfxDbBatchReady, smartBatch.Modules.SkipWhile(m => m.ID != "index.exe").Skip(1).First()); }
                    else { smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext); }
                }
                else //(batchStatus == BatchStatus.Error)
                {
                    // Verarbeitungs-ERROR
                    Log.Debug("Closing batch -> Status: ERROR -> QC");
                    throw new Exception("Fehler in der Verarbeitung von SmartCAP.CM.Patientenakt");
                }

                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                }
            }
        }

        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here happens the action
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }
        }

        #endregion login and processing methods

        #region GUIhelpers

        /// <summary>
        /// Logs a message to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            LogEvent?.Invoke(this, new TextEventArgs(message));
        }

        /// <summary>
        /// Logs the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentOpened(Document document)
        {
            DocumentOpened?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Logs the document closing to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentClosed(Document document)
        {
            DocumentClosed?.Invoke(this, new DocumentEventArgs(document));
        }

        #endregion
    }
}
