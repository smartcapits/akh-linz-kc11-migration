﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CustomModule.Patientenakt
{
    static class SmartTools
    {
        public static TranslationSettings.TranslationTable GetTranslationTable(TranslationSettings translationSettings, string barcode)
        {
            // Sucht die TranslationTable des aktuellen Barcode-Wertes aus den TranslationSettings
            try
            {
                foreach (TranslationSettings.TranslationTable tTable in translationSettings.TranslationTables)
                { if (tTable.Barcode == barcode) { return tTable; } }
            }
            catch (Exception ex)
            { throw new Exception("Error in GetTranslationTable! Msg: " + ex.Message); }

            return null;
        }


        public static string TranslateOMRtoID(TranslationSettings.TranslationTable translationTable, string omrValue)
        {
            try
            {
                switch (omrValue)
                {
                    case "1.1": return translationTable.Code_R1C1;
                    case "1.2": return translationTable.Code_R1C2;
                    case "1.3": return translationTable.Code_R1C3;
                    case "1.4": return translationTable.Code_R1C4;
                    case "2.1": return translationTable.Code_R2C1;
                    case "2.2": return translationTable.Code_R2C1;
                    case "2.3": return translationTable.Code_R2C3;
                    case "2.4": return translationTable.Code_R2C4;
                    case "3.1": return translationTable.Code_R3C1;
                    case "3.2": return translationTable.Code_R3C2;
                    case "3.3": return translationTable.Code_R3C3;
                    case "3.4": return translationTable.Code_R3C4;
                    case "4.1": return translationTable.Code_R4C1;
                    case "4.2": return translationTable.Code_R4C2;
                    case "4.3": return translationTable.Code_R4C3;
                    case "4.4": return translationTable.Code_R4C4;
                    case "5.1": return translationTable.Code_R5C1;
                    case "5.2": return translationTable.Code_R5C1;
                    case "5.3": return translationTable.Code_R5C3;
                    case "5.4": return translationTable.Code_R5C4;
                    case "6.1": return translationTable.Code_R6C1;
                    case "6.2": return translationTable.Code_R6C1;
                    case "6.3": return translationTable.Code_R6C3;
                    case "6.4": return translationTable.Code_R6C4;
                    case "7.1": return translationTable.Code_R7C1;
                    case "7.2": return translationTable.Code_R7C2;
                    case "7.3": return translationTable.Code_R7C3;
                    case "7.4": return translationTable.Code_R7C4;
                    case "8.1": return translationTable.Code_R8C1;
                    case "8.2": return translationTable.Code_R8C2;
                    case "8.3": return translationTable.Code_R8C3;
                    case "8.4": return translationTable.Code_R8C4;
                    case "9.1": return translationTable.Code_R9C1;
                    case "9.2": return translationTable.Code_R9C2;
                    case "9.3": return translationTable.Code_R9C3;
                    case "9.4": return translationTable.Code_R9C4;
                    case "10.1": return translationTable.Code_R10C1;
                    case "10.2": return translationTable.Code_R10C2;
                    case "10.3": return translationTable.Code_R10C3;
                    case "10.4": return translationTable.Code_R10C4;
                    case "11.1": return translationTable.Code_R11C1;
                    case "11.2": return translationTable.Code_R11C2;
                    case "11.3": return translationTable.Code_R11C3;
                    case "11.4": return translationTable.Code_R11C4;
                    case "12.1": return translationTable.Code_R12C1;
                    case "12.2": return translationTable.Code_R12C2;
                    case "12.3": return translationTable.Code_R12C3;
                    case "12.4": return translationTable.Code_R12C4;
                    default: return null;
                }
            }
            catch (Exception ex)
            { throw new Exception("Error in TranslateOMRtoID! Msg: " + ex.Message); }
        }
    }
}
