﻿using System.Collections.Generic;

namespace SmartCAP.CustomModule.Patientenakt
{
    // Klasse zum Serialisieren der Translation-Settings

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class TranslationSettings
    {
        // Properties
        public List<TranslationTable> TranslationTables { get; set; }


        // Klasse inerhalb der Settings
        public class TranslationTable
        {
            public string Barcode { get; set; } = "";
            public string DefaultID { get; set; } = "";
            public string Code_R1C1 { get; set; } = "";
            public string Code_R1C2 { get; set; } = "";
            public string Code_R1C3 { get; set; } = "";
            public string Code_R1C4 { get; set; } = "";
            public string Code_R2C1 { get; set; } = "";
            public string Code_R2C2 { get; set; } = "";
            public string Code_R2C3 { get; set; } = "";
            public string Code_R2C4 { get; set; } = "";
            public string Code_R3C1 { get; set; } = "";
            public string Code_R3C2 { get; set; } = "";
            public string Code_R3C3 { get; set; } = "";
            public string Code_R3C4 { get; set; } = "";
            public string Code_R4C1 { get; set; } = "";
            public string Code_R4C2 { get; set; } = "";
            public string Code_R4C3 { get; set; } = "";
            public string Code_R4C4 { get; set; } = "";
            public string Code_R5C1 { get; set; } = "";
            public string Code_R5C2 { get; set; } = "";
            public string Code_R5C3 { get; set; } = "";
            public string Code_R5C4 { get; set; } = "";
            public string Code_R6C1 { get; set; } = "";
            public string Code_R6C2 { get; set; } = "";
            public string Code_R6C3 { get; set; } = "";
            public string Code_R6C4 { get; set; } = "";
            public string Code_R7C1 { get; set; } = "";
            public string Code_R7C2 { get; set; } = "";
            public string Code_R7C3 { get; set; } = "";
            public string Code_R7C4 { get; set; } = "";
            public string Code_R8C1 { get; set; } = "";
            public string Code_R8C2 { get; set; } = "";
            public string Code_R8C3 { get; set; } = "";
            public string Code_R8C4 { get; set; } = "";
            public string Code_R9C1 { get; set; } = "";
            public string Code_R9C2 { get; set; } = "";
            public string Code_R9C3 { get; set; } = "";
            public string Code_R9C4 { get; set; } = "";
            public string Code_R10C1 { get; set; } = "";
            public string Code_R10C2 { get; set; } = "";
            public string Code_R10C3 { get; set; } = "";
            public string Code_R10C4 { get; set; } = "";
            public string Code_R11C1 { get; set; } = "";
            public string Code_R11C2 { get; set; } = "";
            public string Code_R11C3 { get; set; } = "";
            public string Code_R11C4 { get; set; } = "";
            public string Code_R12C1 { get; set; } = "";
            public string Code_R12C2 { get; set; } = "";
            public string Code_R12C3 { get; set; } = "";
            public string Code_R12C4 { get; set; } = "";
        }                         
    }
}
