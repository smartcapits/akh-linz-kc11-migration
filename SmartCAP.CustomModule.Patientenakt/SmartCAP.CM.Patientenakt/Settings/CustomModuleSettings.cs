﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CustomModule.Patientenakt
{
    public class CustomModuleSettings
    {
        public string SettingsCSSName { get; set; } = "SmartCAP.CustomModule.Patientenakt.Settings";

        public string OMRFieldName { get; set; } = "";
        public string BarcodeFieldName { get; set; } = "";
        public string OMRDisplayFieldName { get; set; } = "";
        public string IDTargetField { get; set; } = "";
        public string TranslationXMLFile { get; set; } = "";
    }
 
}
