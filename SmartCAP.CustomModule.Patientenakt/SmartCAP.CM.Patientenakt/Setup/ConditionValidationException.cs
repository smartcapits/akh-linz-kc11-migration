﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CustomModule.Patientenakt
{
    [Serializable]
    public class ConditionValidationException : Exception
    {
        public ConditionValidationException() { }
        public ConditionValidationException(string message) : base(message) { }
        public ConditionValidationException(string message, Exception inner) : base(message, inner) { }
        protected ConditionValidationException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
