﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.IO;

namespace SmartCAP.CustomModule.Patientenakt
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IBatchClass BatchClass;

        public SetupForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(IBatchClass batchClass)
        {
            BatchClass = batchClass;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }
        #endregion

        public void InitializePanel()
        {
        }

        #region Settings
        public void LoadSettings()
        {
            CustomModuleSettings settings = new CustomModuleSettings();

            try
            {
                string settingsXML = GetCustomStorageString(settings.SettingsCSSName);
                if (settingsXML != "")
                {
                    settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsXML, typeof(CustomModuleSettings));

                    txtBarcodeField.Text = settings.BarcodeFieldName;
                    txtIDTargetField.Text = settings.IDTargetField;
                    txtOMRDisplayField.Text = settings.OMRDisplayFieldName;
                    txtOMRField.Text = settings.OMRFieldName;
                    txtTranslationXMLFile.Text = settings.TranslationXMLFile;
                }
            }
            catch (Exception)
            {
                // Do nothing, just dont load settings
            }
            
        }

        public bool VerifySettings()
        {
            string errorMsg = "";

            try
            {
                // Perform checks here
                if (txtOMRField.Text == "" || txtBarcodeField.Text == "" || txtOMRDisplayField.Text == "" || txtIDTargetField.Text == "" || txtTranslationXMLFile.Text == "") { errorMsg += "Es müssen Werte für alle Felder angegeben sein!" + "\r\n"; }
                if (!File.Exists(txtTranslationXMLFile.Text)) { errorMsg += "Die angegebene XML-Datei existiert nicht oder der Pfad ist nicht zugreifbar" + "\r\n"; }

                if (errorMsg != "") { throw new ConditionValidationException("Please correct the following errors:\r\n" + errorMsg); }
            }
            catch (ConditionValidationException cve)
            {
                MessageBox.Show(cve.Message, "Error saving settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (Exception)
            { throw; }

            return true;
        }

        public void SaveSettings()
        {
  
            // Werte speichern
            CustomModuleSettings settings = new CustomModuleSettings();

            settings.OMRFieldName = txtOMRField.Text;
            settings.BarcodeFieldName = txtBarcodeField.Text;
            settings.OMRDisplayFieldName = txtOMRDisplayField.Text;
            settings.IDTargetField = txtIDTargetField.Text;
            settings.TranslationXMLFile = txtTranslationXMLFile.Text;

            SetCustomStorageString(settings.SettingsCSSName, SerializationTool.Serialize(settings));    // Write Settings as serialized XML to Batch CSS          
        }
        #endregion


        #region UserInteraction
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            { BatchClass.set_CustomStorageString(name, value); }
            catch
            { }
        }

        private string GetCustomStorageString(string name, string defaultValue = "")
        {
            try
            { return BatchClass.get_CustomStorageString(name); }
            catch
            { return defaultValue; }
        }

        #endregion

    }
}
