﻿using System;
using Kofax.Capture.SDK.Workflow;
using Kofax.Capture.SDK.Data;
using System.Runtime.InteropServices;
using log4net;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using Microsoft.Win32;
using System.Windows.Forms.VisualStyles;

namespace SmartCAP.WFA.LSBarcodeCheck
{
    /// <summary>
    /// Implements the IACWorkflowAgent interface
    /// </summary>
    [ProgId("SmartCAP.WFA.LSBarcodeCheck")]
    public class WorkflowAgent : IACWorkflowAgent
    {
        public WorkflowAgent()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        #region module and state declaration
        private const string moduleRecognition = "fp.exe";
        private const string modulePDF = "kfxpdf.exe";
        private const string moduleOCRFullText = "ocr.exe";
        private const string moduleRelease = "release.exe";
        private const string moduleScan = "scan.exe";
        private const string moduleValidation = "index.exe";
        private const string moduleQC = "qc.exe";
        private const string moduleBatchManager = "ops.exe";
        private const string moduleKCNS = "acirsa.exe";
        //private const string moduleSmartCAP = "SmartCAP";

        private const string stateReady = "Ready";
        private const string stateError = "Error";
        private const string stateSuspended = "Suspended";
        private const string stateReserved = "Reserved";
        #endregion 

        /// <summary>
        /// Will be called by every module after a batch is closed 
        /// </summary>
        /// <param name="oWorkflowData"></param>
        public void ProcessWorkflow(ref IACWorkflowData oWorkflowData)
        {
            InitializeLogging();

            try
            {
                SmartWFABatch smartBatch = new SmartWFABatch(oWorkflowData);

                //React on entering Validation module
                if ((smartBatch.KCWorkflowData.CurrentModule.ID == moduleRecognition || smartBatch.KCWorkflowData.CurrentModule.ID == moduleValidation) && smartBatch.KCWorkflowData.NextState.Name == stateReady)
                {
                    CheckBarcodes(smartBatch);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in SmartCAP.WFA.LSBarcodeCheck: " + ex.Message);
                // document will be routed with the error message to Quality Control
                throw new Exception("Error in SmartCAP.WFA.LSBarcodeCheck: " + ex.Message);
            }
        }

        private bool CheckBarcodes(SmartWFABatch smartBatch)
        {
            int? requiredBarcodeLength;
            bool? calculateChecksum;
            string numberLowerLimit1;
            string numberLowerLimit2;
            string numberUpperLimit1;
            string numberUpperLimit2;

            try
            {
                requiredBarcodeLength = int.Parse((string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Barcode Länge", null));
                calculateChecksum = SmartTools.BoolParser((string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Prüfzifferberechnung", null));
                numberLowerLimit1 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Wertebereich untere Grenze 1", null);
                numberLowerLimit2 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Wertebereich untere Grenze 2", null);
                numberUpperLimit1 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Wertebereich obere Grenze 1", null);
                numberUpperLimit2 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine", "Wertebereich obere Grenze 2", null);

                if (requiredBarcodeLength == null) { throw new Exception("'Barcode Länge' ist nicht gesetzt"); }
                if (calculateChecksum == null) { throw new Exception("'Prüfzifferberechnung' ist nicht gesetzt"); }
                if (numberLowerLimit1 == null) { throw new Exception("'Wertebereich untere Grenze 1' ist nicht gesetzt"); }
                if (numberLowerLimit2 == null) { throw new Exception("'Wertebereich untere Grenze 2' ist nicht gesetzt"); }
                if (numberUpperLimit1 == null) { throw new Exception("'Wertebereich obere Grenze 1' ist nicht gesetzt"); }
                if (numberUpperLimit2 == null) { throw new Exception("'Wertebereich obere Grenze 2' ist nicht gesetzt"); }
            }
            catch (Exception ex)
            { throw new Exception(@"Fehler beim Auslesen der Einstellungen aus der Registy (Pfad: 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0\Stapelklasse Lieferscheine') - " + ex.Message); }
            

            foreach (Document doc in smartBatch.Documents)
            {
                // Check Barcodes for length, min/max value and checksum
                doc.IndexFields.Where(iF => iF.Name.ToLower() == "barcode").ToList().ForEach(bcIF =>
                {
                    bcIF.Value = bcIF.Value.Trim();

                    // Check for length
                    if (!doc.Rejected && bcIF.Value.Length != requiredBarcodeLength) { RejectDocument(doc, "Der Barcode (" + bcIF.Value + ") muss die Länge " + requiredBarcodeLength.ToString() + " haben"); }

                    // Check for value limits
                    if (!doc.Rejected && 
                       !(string.Compare(bcIF.Value, numberLowerLimit1) >= 0 && string.Compare(bcIF.Value, numberUpperLimit1) <= 0) && !(string.Compare(bcIF.Value, numberLowerLimit2) >= 0 && string.Compare(bcIF.Value, numberUpperLimit2) <= 0))
                        { RejectDocument(doc, "Der Barcode (" + bcIF.Value + ") muss zwischen den Werten " + numberLowerLimit1 + " und " + numberUpperLimit1 + " oder den Werten " + numberLowerLimit2 + " und " + numberUpperLimit2 + " liegen"); }

                    // Calculate chekcsum
                    if (!doc.Rejected && calculateChecksum == true)
                    {
                        if (bcIF.Value.Substring(bcIF.Value.Length - 1, 1) != SmartTools.CalculateCheckSum(bcIF.Value))
                        { RejectDocument(doc, "Die Prüfziffer des Barcodes (" + bcIF.Value.Substring(bcIF.Value.Length - 1, 1) + ") stimmt nicht mit der errechneten Prüfziffer (" + SmartTools.CalculateCheckSum(bcIF.Value) + ") überein"); }
                    }

                    if (!doc.Rejected) { doc.Note = ""; }      // In case doc was rejected before
                });
            }

            return true;
        }


        private void RejectDocument(Document document, string note)
        {
            document.Rejected = true;
            document.Note = note;
        }

    }

}
