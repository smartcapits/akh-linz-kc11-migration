﻿using Kofax.Capture.SDK.Data;
using Kofax.Capture.SDK.Workflow;

namespace SmartCAP.WFA.LSBarcodeCheck.Model
{
    public class WorkflowData
    {
        private IACDataElement runtime;
        public IACDataElement Batch { get { return runtime.FindChildElementByName("Batch"); } }
        public IACDataElementCollection Documents { get { return Batch.FindChildElementByName("Documents").FindChildElementsByName("Document"); } }
        public IACDataElementCollection BatchFields { get { return Batch.FindChildElementByName("BatchFields").FindChildElementsByName("BatchField"); } }

        public WorkflowData(ref IACWorkflowData oWorkflowData)
        {
            runtime = oWorkflowData.ExtractRuntimeACDataElement(0);
        }

        public IACDataElementCollection GetPages(IACDataElement document)
        {
            return document.FindChildElementByName("Pages").FindChildElementsByName("Page");
        }
        public IACDataElementCollection GetIndexFields(IACDataElement document)
        {
            return document.FindChildElementByName("IndexFields").FindChildElementsByName("IndexField");
        }
    }
}
