﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.WFA.LSBarcodeCheck
{
    static class SmartTools
    {
        public static bool BoolParser(string value)
        {
            // Avoid exceptions
            if (value == null) { return false; }

            // Remove whitespace from string and lower case
            value = value.Trim().ToLower();
            if (value == "true") { return true; }
            if (value == "t") { return true; }
            if (value == "1") { return true; }
            if (value == "yes") { return true; }
            if (value == "y") { return true; }
            if (value == "ja") { return true; }

            // Must be false
            return false;
        }


        public static string CalculateCheckSum(string inputNumeric)
        {
            string checkSum;
            string tempChar;
            int i, sum = 0, j = 3;
            string inputWithoutChecksum = inputNumeric.Substring(0, inputNumeric.Length - 1);

            for (i = inputWithoutChecksum.Length - 1; i >= 0; i--)
            {
                tempChar = inputWithoutChecksum.Substring(i, 1);
                sum += int.Parse(tempChar) * j;

                j = j == 3 ? 1 : 3;   // Toggle j between 3 and 1
            }

            checkSum = (sum % 10).ToString();
            if (checkSum == "10") { checkSum = "0"; }

            return checkSum;
        }
    }
}
