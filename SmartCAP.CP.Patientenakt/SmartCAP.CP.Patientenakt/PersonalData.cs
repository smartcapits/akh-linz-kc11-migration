﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CP.Patientenakt
{
    public class PersonalData
    {
        public string LastName { get; set; } = "";
        public string FirstName { get; set; } = "";
        public string DateOfBirth { get; set; } = "";
    }
}
