﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using KC = Kofax.Capture.CaptureModule.InteropServices;
using Microsoft.Win32;
using System.CodeDom;

namespace SmartCAP.CP.Patientenakt.User
{
    [ProgId("SmartCAP.CP.Patientenakt")]
    public partial class UserPanel : UserControl
    {
        /// <summary>
        /// Enable the controls only for these batch classes
        /// </summary>
        public string[] ENABLE_FOR_BATCHCLASSES = new string[] { "PATIENTENAKT", "PATIENTENAKT_TEST", "PATIENTENAKT_NEU", "PATIENTENAKT_NEU_TEST" };
        public string[] DISABLE_FOR_BATCHCLASSES = new string[] { "EINVERSTÄNDNISERKLÄRUNG", "FI-", "FRAGEBÖGEN", "TRANSFUSION", "STAPELDECKBLATT" };
        public string SAP_Target_System = "";
        public string Einrichtung = "";

        public const string IF_Fallzahl = "Fallzahl";
        public const string IF_DokumentierendeOE = "DokumentierendeOE";
        public const string IF_Aufnahmedatum = "Aufnahmedatum";
        public const string IF_FachlicheOE = "FachlicheOE";
        public const string IF_Dokumentenbeschreibung = "Dokumentenbeschreibung";
        public const string BF_Zielsystem = "Zielsystem";

        bool enabled = false;

        public UserPanel()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeComponent();
        }

        private KC.Application kcApp;

        /// <summary>
        /// The calling Kofax application (e.g. Scan or Quality Control) will be exposed to this object.
        /// For further use its assigned to an internal variable
        /// </summary>
        public KC.Application Application
        {
            set
            {
                kcApp = value;
                ToggleControls(false); // disable controls by default
                kcApp.ShowWindow(false); // hide the custom panel by default
                enabled = false;
            }
        }

        /// <summary>
        /// Called when any action is performed
        /// </summary>
        /// <param name="EventNumber">The number of the action performed (KfxOcxEvent)</param>
        /// <param name="Argument"></param>
        /// <param name="Cancel">Set Cancel to 1, if you want to cancel the currenct action</param>
        public void ActionEvent(int EventNumber, object Argument, out int Cancel)
        {
            Cancel = 0;

            try
            {
                switch ((KC.KfxOcxEvent)EventNumber)
                {
                    case KC.KfxOcxEvent.KfxOcxEventBatchCreating:
                        break;

                    case KC.KfxOcxEvent.KfxOcxEventBatchOpened:
                        if (!DISABLE_FOR_BATCHCLASSES.Any(dBC => kcApp.ActiveBatch.BatchClass.Name.ToUpper().Contains(dBC.ToUpper()))) 
                        { OpenBatch(); }
                        break;

                    case KC.KfxOcxEvent.KfxOcxEventBatchClosing:
                    case KC.KfxOcxEvent.KfxOcxEventBatchRejecting:
                    case KC.KfxOcxEvent.KfxOcxEventBatchSuspending:
                    case KC.KfxOcxEvent.KfxOcxEventBatchDeleting:
                        if (enabled) { CloseBatch(); }
                        break;

                    case KC.KfxOcxEvent.KfxOcxEventDocumentOpened:
                        // Do not process rejected document
                        if (enabled && !kcApp.ActiveBatch.ActiveDocument.Rejected) { DocumentOpened(); }
                        break;

                    case KC.KfxOcxEvent.KfxOcxEventDocumentClosing:
                        // Do not process rejected document, only process Pateintenakt documents
                        if (enabled && !kcApp.ActiveBatch.ActiveDocument.Rejected) 
                        {
                            if (kcApp.ActiveBatch.ClassName.ToUpper().Contains("PATIENTENAKT")) { DocumentClosing(); }
                        }
                        break;

                    case KC.KfxOcxEvent.KfxOcxEventFieldExiting:
                        // Do not process rejected document
                        if (enabled && !kcApp.ActiveBatch.ActiveDocument.Rejected) { FieldExiting(); }
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Cancel = 1;
                MessageBox.Show("Patientenakt IndexFields - Es ist ein Fehler im Event '" + ((KC.KfxOcxEvent)EventNumber).ToString() + "' aufgetreten: " + ex.Message, "Fehler in Patientenakt IndexFields", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private bool DocumentOpened()
        {
            // Save index data, display personal data in Panel

            // Initiate INI-File
            string serverPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0", "ServerPath", null);
            if (serverPath == null) { throw new Exception("Ser ServerPath ist in der Registry nicht gesetzt"); }
            string iniFilePath = System.IO.Path.Combine(serverPath, @"Patientenakt\Patientenakt.ini");
            IniFileHelper iniFile = new IniFileHelper(iniFilePath);
            
            // Connect to SAP
            if (SAP_Target_System == "")
            {
                SAP_Target_System = kcApp.ActiveBatch.BatchFields[BF_Zielsystem].Value;

                // Connect to SAP 
                SAP_NET_Integration.Connect(SAP_Target_System);
            }

            // Get Einrichtung from INI-File
            if (Einrichtung == "") { Einrichtung = iniFile.Read("Einrichtung", SAP_Target_System + "_SAP"); }

            IndexData.Fallzahl.Old = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value;
            IndexData.Aufnahmedatum.Old = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Aufnahmedatum].Value;
            IndexData.DokumentierendeOE.Old = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_DokumentierendeOE].Value;
            IndexData.FachlicheOE.Old = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_FachlicheOE].Value;

            if (kcApp.ActiveBatch.ActiveDocument.FormType.Name == "Dokumentenbeschreibung")
            { 
                if (IndexData.Fallzahl.Old == "" || IndexData.Aufnahmedatum.Old == "" || IndexData.DokumentierendeOE.Old == "" || IndexData.FachlicheOE.Old == "")
                {
                    kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value = IndexData.Fallzahl.New;
                    kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Aufnahmedatum].Value = IndexData.Aufnahmedatum.New;
                    kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_DokumentierendeOE].Value = IndexData.DokumentierendeOE.New;
                    kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_FachlicheOE].Value = IndexData.FachlicheOE.New;
                }
            }

            // Update personal data if Fallzahl changes
            if (kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value != IndexData.Fallzahl.New)
            {
                try
                {
                    // Get Data from SAP 
                    PersonalData persData = SAP_NET_Integration.GetPersData(kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value, 
                                                                            kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_FachlicheOE].Value, 
                                                                            kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_DokumentierendeOE].Value, Einrichtung);

                    if (persData == null)
                    {
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        txtBirthDate.Text = "";
                        throw new Exception("Keine Patientendaten gefunden");
                    }
                    else
                    {
                        txtFirstName.Text = persData.LastName;
                        txtLastName.Text = persData.FirstName;
                        txtBirthDate.Text = persData.DateOfBirth;
                    }
                }
                catch (Exception ex)
                { MessageBox.Show("Bei der Abfrage der Daten aus SAP ist ein Fehler aufgetreten: " + ex.Message, "Fehler in SAP-Abfrage", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            
            return true;
        }


        private bool FieldExiting()
        {
            // Display personal data in Panel when leaving index field "Fallzahl" or "FachlicheOE" 

            if (kcApp.ActiveBatch.ActiveDocument.ActiveIndexField.Name == IF_Fallzahl || kcApp.ActiveBatch.ActiveDocument.ActiveIndexField.Name == IF_FachlicheOE)
            {
                try
                {
                    // Get Data from SAP 
                    PersonalData persData = SAP_NET_Integration.GetPersData(kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value, 
                                                                            kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_FachlicheOE].Value,
                                                                            kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_DokumentierendeOE].Value, Einrichtung);

                    if (persData == null)
                    {
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        txtBirthDate.Text = "";
                        throw new Exception("Keine Patientendaten gefunden");
                    }
                    else
                    {
                        txtFirstName.Text = persData.LastName;
                        txtLastName.Text = persData.FirstName;
                        txtBirthDate.Text = persData.DateOfBirth;
                    }
                }
                catch (Exception ex)
                { MessageBox.Show("Bei der Abfrage der Daten aus SAP ist ein Fehler aufgetreten: " + ex.Message, "Fehler in SAP-Abfrage", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }

            return true;
        }

        private bool DocumentClosing()
        {
            // Inherit index data to following documents in batch

            IndexData.Fallzahl.New = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Fallzahl].Value;
            IndexData.Aufnahmedatum.New = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Aufnahmedatum].Value;
            IndexData.DokumentierendeOE.New = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_DokumentierendeOE].Value;
            IndexData.FachlicheOE.New = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_FachlicheOE].Value;

            if (kcApp.ActiveBatch.ActiveDocument.FormType.Name.Contains("Dokumentenbeschreibung"))
            {
                IndexData.DokBeschreibung.New = kcApp.ActiveBatch.ActiveDocument.IndexFields[IF_Dokumentenbeschreibung].Value;
                if (IndexData.DokBeschreibung.New == "") { IndexData.DokBeschreibung.New = " "; }

                // Copy Dokumentenbeschreibung
                for (int currentDoc = kcApp.ActiveBatch.ActiveDocument.OrderNumber + 1; currentDoc < kcApp.ActiveBatch.DocumentCount; currentDoc++)
                {
                    if (kcApp.ActiveBatch.Documents[currentDoc].FormType.Name == "Dokumenttyptrennblatt")
                    { kcApp.ActiveBatch.Documents[currentDoc].IndexFields[IF_Dokumentenbeschreibung].Value = IndexData.DokBeschreibung.New; }
                    else
                    { break; }
                }
            }

            if (kcApp.ActiveBatch.ActiveDocument.FormType.Name.Contains("Patientendeckblatt"))
            {
                // Copy IndexFields
                for (int currentDoc = kcApp.ActiveBatch.ActiveDocument.OrderNumber + 1; currentDoc < kcApp.ActiveBatch.DocumentCount; currentDoc++)
                {
                    if (kcApp.ActiveBatch.Documents[currentDoc].FormType.Name != "Patientendeckblatt")
                    {
                        if (IndexData.Fallzahl.Old != IndexData.Fallzahl.New) { kcApp.ActiveBatch.Documents[currentDoc].IndexFields[IF_Fallzahl].Value = IndexData.Fallzahl.New; }
                        if (IndexData.Aufnahmedatum.Old != IndexData.Aufnahmedatum.New) { kcApp.ActiveBatch.Documents[currentDoc].IndexFields[IF_Aufnahmedatum].Value = IndexData.Aufnahmedatum.New; }
                        if (IndexData.DokumentierendeOE.Old != IndexData.DokumentierendeOE.New) { kcApp.ActiveBatch.Documents[currentDoc].IndexFields[IF_DokumentierendeOE].Value = IndexData.DokumentierendeOE.New; }
                        if (IndexData.FachlicheOE.Old != IndexData.FachlicheOE.New) { kcApp.ActiveBatch.Documents[currentDoc].IndexFields[IF_FachlicheOE].Value = IndexData.FachlicheOE.New; }
                    }
                    else
                    { break; }
                }
            }

            return true;
        }

        private void CloseBatch()
        {
            if (SAP_Target_System != "") { SAP_NET_Integration.Disconnect(SAP_Target_System); SAP_Target_System = ""; }
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtBirthDate.Text = "";
            ToggleControls(false);
            kcApp.ShowWindow(false);
            enabled = false;
        }

        private void OpenBatch()
        {
            ToggleControls(true);
            kcApp.ShowWindow(true);
            enabled = true;
        }

        #region helpers

        private void SelectMenuQC(KC.KfxApiQCMenuItems item)
        {
            kcApp.SelectMenuItem((int)item);
        }

        private void SelectMenuScan(KC.KfxApiScanMenuItems item)
        {
            kcApp.SelectMenuItem((int)item);
        }

        private void ToggleControls(bool enabled)
        {
            // enable/disable only buttons or textboxes
            Controls.OfType<Button>().ToList().ForEach(x => x.Enabled = enabled);
            Controls.OfType<TextBox>().ToList().ForEach(x => x.Enabled = enabled);
        }

        #endregion helpers
    }
}
