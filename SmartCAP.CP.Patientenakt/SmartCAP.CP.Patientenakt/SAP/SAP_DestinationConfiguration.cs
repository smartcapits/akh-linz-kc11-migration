﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using SAP.Middleware.Connector;

namespace SmartCAP.CP.Patientenakt
{
    public class SAP_DestinationConfiguration : IDestinationConfiguration
    {
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public bool ChangeEventsSupported()
        {
            return false;
        }

        public RfcConfigParameters GetParameters(string destinationName)
        {
            string serverPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0", "ServerPath", null);
            if (serverPath == null) { throw new Exception("Ser ServerPath ist in der Registry nicht gesetzt"); }

            string iniFilePath = System.IO.Path.Combine(serverPath, @"Patientenakt\Patientenakt.ini");
            IniFileHelper iniFile = new IniFileHelper(iniFilePath);

            string SAP_MessageServer = iniFile.Read("SAP_MessageServer", destinationName + "_SAP");
            string SAP_Router = iniFile.Read("SAP_SAPRouter", destinationName + "_SAP");
            string SAP_Groupname = iniFile.Read("SAP_Groupname", destinationName + "_SAP");
            string SAP_System = iniFile.Read("SAP_System", destinationName + "_SAP");
            string SAP_Language = iniFile.Read("SAP_Language", destinationName + "_SAP");
            string SAP_Instance = iniFile.Read("SAP_Instance", destinationName + "_SAP");
            string SAP_AppServer = iniFile.Read("SAP_AppServer", destinationName + "_SAP");
            string SAP_Client = iniFile.Read("SAP_Client", destinationName + "_SAP");
            string SAP_Username = iniFile.Read("SAP_UserName", destinationName + "_SAP");
            string SAP_PassWord = iniFile.Read("SAP_PassWord", destinationName + "_SAP");


            // Set properties
            RfcConfigParameters sapParams = new RfcConfigParameters();
            sapParams.Add(RfcConfigParameters.Name, destinationName);
            if (SAP_Client != "") sapParams.Add(RfcConfigParameters.Client, SAP_Client);
            if (SAP_Username != "") sapParams.Add(RfcConfigParameters.User, SAP_Username);
            if (SAP_PassWord != "") sapParams.Add(RfcConfigParameters.Password, SAP_PassWord);
            if (SAP_Language != "") sapParams.Add(RfcConfigParameters.Language, SAP_Language);
            if (SAP_MessageServer != "") sapParams.Add(RfcConfigParameters.MessageServerHost, SAP_MessageServer);
            if (SAP_Groupname != "") sapParams.Add(RfcConfigParameters.LogonGroup, SAP_Groupname);
            if (SAP_System != "") sapParams.Add(RfcConfigParameters.SystemID, SAP_System);
            if (SAP_Instance != "") sapParams.Add(RfcConfigParameters.SystemNumber, SAP_Instance);    // 0
            if (SAP_Router != "") sapParams.Add(RfcConfigParameters.SAPRouter, SAP_Router);
            if (SAP_AppServer != "") sapParams.Add(RfcConfigParameters.AppServerHost, SAP_AppServer);
            sapParams.Add(RfcConfigParameters.PoolSize, "5");
            sapParams.Add(RfcConfigParameters.PeakConnectionsLimit, "10");
            sapParams.Add(RfcConfigParameters.ConnectionIdleTimeout, "600");

            return sapParams;
        }
    }
}
