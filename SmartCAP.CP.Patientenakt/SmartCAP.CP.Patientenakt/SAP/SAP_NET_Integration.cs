﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAP.Middleware.Connector;


namespace SmartCAP.CP.Patientenakt
{
    static class SAP_NET_Integration
    {
        public static bool Connected { get; private set; } = false;
        private static RfcDestination sapDest = null;

        public static bool Connect(string targetSystem)
        {
            SAP_DestinationConfiguration sapDestConfig = new SAP_DestinationConfiguration();
            sapDestConfig.GetParameters(targetSystem);

            if (!RfcDestinationManager.IsDestinationConfigurationRegistered()) { RfcDestinationManager.RegisterDestinationConfiguration(sapDestConfig); }
            sapDest = RfcDestinationManager.GetDestination(targetSystem);

            if (sapDest == null)
            {
                Connected = false;
                return false;
            }

            Connected = true;
            return true;
        }

        public static void Disconnect(string targetSystem)
        {
            SAP_DestinationConfiguration sapDestConfig = new SAP_DestinationConfiguration();
            sapDestConfig.GetParameters(targetSystem);
            if (!RfcDestinationManager.IsDestinationConfigurationRegistered()) { RfcDestinationManager.UnregisterDestinationConfiguration(sapDestConfig); }

            sapDest = null;
            Connected = false;
        }


        public static PersonalData GetPersData(string fallZahl, string fachlicheOE, string dokumentierendeOE, string einrichtung)
        {
            // Gets first name, last name and date of birth from SAP. Return null in case of no match was found

            PersonalData persData = null;

            if (sapDest == null)
            { throw new Exception("Bitte melden Sie sich an SAP an vor der Abfrage"); }
            else
            {
                RfcRepository sapRepo = sapDest.Repository;
                IRfcFunction sapFunction = sapRepo.CreateFunction("Z_CHECK_DOC_FROM_ARCHIVE");
                IRfcStructure sapNDOC = sapFunction.GetStructure("SS_NDOC");

                // Set query parameters
                sapNDOC.SetValue("FALNR", fallZahl);
                sapNDOC.SetValue("ORGFA", fachlicheOE);
                sapNDOC.SetValue("ORGPF", dokumentierendeOE);
                sapNDOC.SetValue("ORGLE", dokumentierendeOE);
                sapNDOC.SetValue("ORGDO", dokumentierendeOE);
                sapNDOC.SetValue("EINRI", einrichtung);

                // Query SAP
                sapFunction.Invoke(sapDest);
                int sapErrorCount = (int)sapFunction.GetValue("SS_ERROR_COUNT");

                if (sapErrorCount == 0)
                {
                    persData = new PersonalData();

                    IRfcStructure sapPatient = sapFunction.GetStructure("SS_NPAT");
                    persData.FirstName = sapPatient.GetValue("VNAME").ToString();
                    persData.LastName = sapPatient.GetValue("NNAME").ToString();
                    persData.DateOfBirth = sapPatient.GetValue("GBDAT").ToString();
                }
                else
                {
                    //throw new Exception("Die Abfrage hat keine Treffer ergeben");
                }
            }

            return persData;
        }

    }
}
