﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace SmartCAP.CP.Patientenakt
{
    static class SAP_Integration
    {
        public static bool Connected { get; private set; } = false;

        private static object SAP_COM_Obj = null;
        private static object SAP_Connection = null;
        private static object SAP_CheckFunction = null;
        private static object SAP_CheckTable = null;


        public static bool Connect(string targetSystem)
        {
            // Connect to SAP using COM-Object late binding 
            // Coding examples for late binding:
            // https://www.bestprog.net/en/2018/12/08/late-binding-method-call-example-class-system-activator-method-invoke/
            // https://stackoverflow.com/questions/10214183/get-dll-directory-from-progid
            // https://www.codeproject.com/articles/10838/how-to-get-properties-and-methods-in-late-binding


            string serverPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0", "ServerPath", null);
            if (serverPath == null) { throw new Exception("Ser ServerPath ist in der Registry nicht gesetzt"); }

            string iniFilePath = Path.Combine(serverPath, @"Patientenakt\Patientenakt.ini");
            IniFileHelper iniFile = new IniFileHelper(iniFilePath);

            string SAP_MessageServer = iniFile.Read("SAP_MessageServer", targetSystem + "_SAP");
            string SAP_Router = iniFile.Read("SAP_SAPRouter", targetSystem + "_SAP");
            string SAP_Groupname = iniFile.Read("SAP_Groupname", targetSystem + "_SAP");
            string SAP_System = iniFile.Read("SAP_System", targetSystem + "_SAP");
            string SAP_Language = iniFile.Read("SAP_Language", targetSystem + "_SAP");
            string SAP_Instance = iniFile.Read("SAP_Instance", targetSystem + "_SAP");
            string SAP_AppServer = iniFile.Read("SAP_AppServer", targetSystem + "_SAP");
            string SAP_Client = iniFile.Read("SAP_Client", targetSystem + "_SAP");
            string SAP_Username = iniFile.Read("SAP_UserName", targetSystem + "_SAP");
            string SAP_PassWord = iniFile.Read("SAP_PassWord", targetSystem + "_SAP");
            
            Type SAP_COM_Type = Type.GetTypeFromProgID("SAP.Functions");
            SAP_COM_Obj = Activator.CreateInstance(SAP_COM_Type);
            SAP_Connection = SAP_COM_Obj.GetType().InvokeMember("Connection", BindingFlags.GetProperty, null, SAP_COM_Obj, null);


            // Set properties
            if (SAP_AppServer.Trim() == "")
            {
                SAP_Connection.GetType().InvokeMember("MessageServer", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_MessageServer });
                SAP_Connection.GetType().InvokeMember("GroupName", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Groupname });
                SAP_Connection.GetType().InvokeMember("System", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_System });
                SAP_Connection.GetType().InvokeMember("SAPRouter", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Router });
            }
            else
            {
                SAP_Connection.GetType().InvokeMember("ApplicationServer", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_AppServer });
                SAP_Connection.GetType().InvokeMember("SystemNumber", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Instance });
            }
            SAP_Connection.GetType().InvokeMember("Client", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Client });
            SAP_Connection.GetType().InvokeMember("User", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Username });
            SAP_Connection.GetType().InvokeMember("Password", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_PassWord });
            SAP_Connection.GetType().InvokeMember("Language", BindingFlags.SetProperty, null, SAP_Connection, new object[] { SAP_Language });

            // Log on
            object SAP_LogOnSuccess = SAP_Connection.GetType().InvokeMember("Logon", BindingFlags.InvokeMethod, null, SAP_Connection, new object[] { 0, true });
            if (!(bool)SAP_LogOnSuccess) { throw new Exception("SAP_Integration.Connect: Login an SAP R/3 war nicht erfolgreich"); }

            // Create function reference
            SAP_CheckFunction = SAP_COM_Obj.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, SAP_COM_Obj, new object[] { "Z_CHECK_DOC_FROM_ARCHIVE" });
            if (SAP_CheckFunction == null) { throw new Exception("SAP_Integration.Add: Funktionsbaustein Z_CHECK_DOC_FROM_ARCHIV im SAP R/3 nicht ansprechbar"); }

            // Create table reference
            SAP_CheckTable = SAP_CheckFunction.GetType().InvokeMember("Tables", BindingFlags.InvokeMethod, null, SAP_CheckFunction, new object[] { "SS_MESG" });
            if (SAP_CheckFunction == null) { throw new Exception("SAP_Integration.Tables:  Fehler beim Hinzufügen der SAP-Fehlermeldungstabelle des Prüf-Funktionsbausteins"); }

            Connected = true;
            return true;
        }


        public static PersonalData GetPersData(string fallZahl, string fachlicheOE, string dokumentierendeOE, string einrichtung)
        {
            // Gets first name, last name and date of birth from SAP

            PersonalData persData = null;
            object SAP_Document = null;


            // Create SAP document object
            SAP_Document = SAP_CheckFunction.GetType().InvokeMember("Exports", BindingFlags.InvokeMethod, null, SAP_CheckFunction, new object[] { "SS_NDOC" });
            if (SAP_Document == null) { throw new Exception("SAP_Integration.Exports: Kann das Dokument-Objekt in SAP R/3 nicht erzeugen"); }

            // Set parameters
            SAP_Document.GetType().InvokeMember("FALNR", BindingFlags.SetProperty, null, SAP_Document, new object[] { fallZahl });
            SAP_Document.GetType().InvokeMember("ORGFA", BindingFlags.SetProperty, null, SAP_Document, new object[] { fachlicheOE });
            SAP_Document.GetType().InvokeMember("ORGPF", BindingFlags.SetProperty, null, SAP_Document, new object[] { dokumentierendeOE });
            SAP_Document.GetType().InvokeMember("ORGLE", BindingFlags.SetProperty, null, SAP_Document, new object[] { dokumentierendeOE });
            SAP_Document.GetType().InvokeMember("ORGDO", BindingFlags.SetProperty, null, SAP_Document, new object[] { dokumentierendeOE });
            SAP_Document.GetType().InvokeMember("EINRI", BindingFlags.SetProperty, null, SAP_Document, new object[] { einrichtung });

            // Get document data
            object SAP_Document_Success = SAP_CheckFunction.GetType().InvokeMember("Call", BindingFlags.InvokeMethod, null, SAP_CheckFunction, null);
            if (!(bool)SAP_Document_Success) { throw new Exception("SAP_Integration.Call: Bei der Abfrage der Personendaten in SAP R/3 ist ein Fehler aufgetreten"); }

            // See if we have errors
            object SAP_Document_ErrorCount = SAP_CheckFunction.GetType().InvokeMember("Document.imports", BindingFlags.InvokeMethod, null, SAP_CheckFunction, new object[] { "SS_ERROR_COUNT" });
            if ((long)SAP_Document_ErrorCount > 0)
            {
                // XXXXXXX

                //meldungsText = ""
                //firstLine = True
                //ErrorCount = SAPCheckTable.RowCount
                //For Zaehler = 1 To ErrorCount
                //    Set SAPMessage = SAPCheckTable.Rows(Zaehler)
                //    If firstLine = True Then
                //        meldungsText = SAPMessage.Value("TEXT")
                //        firstLine = False
                //    Else
                //        meldungsText = meldungsText + Chr(13) + Chr(13) + SAPMessage.Value("TEXT")
                //    End If
                //Next
                //errMsg = "Fehler beim Prüfen der Daten in KIS-MED!" + Chr(13) + Chr(13) + "ReturnCode: " + Str(.imports("SS_ERROR_COUNT")) + Chr(13) + Chr(13) + "Fehlerbeschreibung:" + Chr(13) + meldungsText


                throw new Exception("SAP_Integration.Imports:Fehler beim Prüfen der Daten in KIS-MED");
            }
            else
            {
                // Retrieve personal data from document
                object SAP_Document_PersonalData = SAP_CheckFunction.GetType().InvokeMember("Persdata.imports", BindingFlags.InvokeMethod, null, SAP_CheckFunction, new object[] { "SS_NPAT" });

                persData.FirstName = (string)SAP_Document_PersonalData.GetType().InvokeMember("Value", BindingFlags.InvokeMethod, null, SAP_Document_PersonalData, new object[] { "VNAME" });
                persData.LastName = (string)SAP_Document_PersonalData.GetType().InvokeMember("Value", BindingFlags.InvokeMethod, null, SAP_Document_PersonalData, new object[] { "NNAME" });
                persData.DateOfBirth = (string)SAP_Document_PersonalData.GetType().InvokeMember("Value", BindingFlags.InvokeMethod, null, SAP_Document_PersonalData, new object[] { "GBDAT" });
            }

            return persData;
        }

    }

}
