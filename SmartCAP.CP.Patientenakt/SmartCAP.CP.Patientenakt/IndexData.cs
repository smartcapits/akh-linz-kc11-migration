﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CP.Patientenakt
{
    public static class IndexData
    {
        public static class Fallzahl
        { 
            public static string Old { get; set; }
            public static string New { get; set; }
        }

        public static class Aufnahmedatum
        {
            public static string Old { get; set; }
            public static string New { get; set; }
        }

        public static class DokumentierendeOE
        {
            public static string Old { get; set; }
            public static string New { get; set; }
        }

        public static class FachlicheOE
        {
            public static string Old { get; set; }
            public static string New { get; set; }
        }

        public static class DokBeschreibung
        {
            public static string New { get; set; }
        }

    }
}
