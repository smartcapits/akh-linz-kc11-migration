﻿using System;
using Kofax.Capture.SDK.Workflow;
using Kofax.Capture.SDK.Data;
using System.Runtime.InteropServices;
using SmartCAP.WFA.CheckFragebogen.Model;
using log4net;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;

namespace SmartCAP.WFA.CheckFragebogen
{
    /// <summary>
    /// Implements the IACWorkflowAgent interface
    /// </summary>
    [ProgId("SmartCAP.WFA.CheckFragebogen")]
    public class WorkflowAgent : IACWorkflowAgent
    {
        public WorkflowAgent()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        #region module and state declaration
        private const string moduleRecognition = "fp.exe";
        private const string modulePDF = "kfxpdf.exe";
        private const string moduleOCRFullText = "ocr.exe";
        private const string moduleRelease = "release.exe";
        private const string moduleScan = "scan.exe";
        private const string moduleValidation = "index.exe";
        private const string moduleQC = "qc.exe";
        private const string moduleBatchManager = "ops.exe";
        private const string moduleKCNS = "acirsa.exe";
        //private const string moduleSmartCAP = "SmartCAP";

        private const string stateReady = "Ready";
        private const string stateError = "Error";
        private const string stateSuspended = "Suspended";
        private const string stateReserved = "Reserved";
        #endregion 

        private const string documentCheckErrorMessage = "Bei diesem Dokument wurde mindestens ein ungültiger Indexwert gefunden!";

        /// <summary>
        /// Will be called by every module after a batch is closed 
        /// </summary>
        /// <param name="oWorkflowData"></param>
        public void ProcessWorkflow(ref IACWorkflowData oWorkflowData)
        {
            InitializeLogging();

            try
            {
                SmartWFABatch smartBatch = new SmartWFABatch(oWorkflowData);

                //React on entering Validation module
                if ((smartBatch.KCWorkflowData.NextModule.ID == moduleValidation || smartBatch.KCWorkflowData.NextModule.ID == moduleRelease) && smartBatch.KCWorkflowData.NextState.Name == stateReady)
                {
                    EnterValidationAndRelease(smartBatch);

                    // Workflow altern auf nächsten Step nach Validation wenn wir in Validation gehen
                    if (smartBatch.KCWorkflowData.NextModule.ID == moduleValidation)
                    {
                        // Stapel auf Modul nach validation routen
                        smartBatch.RouteTo(smartBatch.Modules.SkipWhile(m => m.ID != moduleValidation).Skip(1).First(), State.Ready);
                    }
                }

                else if (smartBatch.KCWorkflowData.NextState.Name == stateError && smartBatch.KCWorkflowData.CurrentModule.ID != moduleBatchManager)
                {
                    if (EnterQC(smartBatch)) 
                    {
                        // Stapel auf Validation routen wenn nur CheckFragebogen-ausgesetzte Dokuemnte im Stapel sind. Wenn vom BatchManager auf Error gesetzt -> in QC gehen
                        smartBatch.RouteTo(smartBatch.Modules.First(m => m.ID == moduleValidation), State.Ready);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in SmartCAP.WFA.CheckFragebogen: " + ex.Message);
                // document will be routed with the error message to Quality Control
                throw new Exception("Error in SmartCAP.WFA.Template: " + ex.Message);
            }
        }

        private bool EnterValidationAndRelease(SmartWFABatch smartBatch)
        {
            foreach (Document doc in smartBatch.Documents)
            {
                // document checken
                if (ContentChecks.CheckFragebogenIndexFields(doc))
                {
                    // Document auf Validated setzen
                    doc.Validated = true;
                    doc.Note = "";
                }

                else
                {
                    // Document auf Rejected setzen, in Note festhalten dass es sich um Index-Error handelt. Auch auf zu validierend setzen
                    doc.Validated = false;
                    doc.Rejected = true;
                    doc.Note = documentCheckErrorMessage;
                }
            }

            return true;
        }

        private bool EnterQC(SmartWFABatch smartBatch)
        {
            bool onlyCheckFragebogenErrors = true;

            foreach (Document doc in smartBatch.Documents)
            {
                 // Checken ob ein document eine andere Fehlermeldung hat als die voreingestellte für Indexfeld-Error
                if (doc.Note == documentCheckErrorMessage || doc.Note == "")
                {
                    // Document unrejecten damit validiert werden kann
                    doc.Rejected = false;
                    //doc.Note = "";    // Keep note so validator knows that there is a problem with an index field
                }
                else
                { 
                    onlyCheckFragebogenErrors = false; 
                }
            }

            return onlyCheckFragebogenErrors;
        }


        #region helpers
        /// <summary>
        /// Route the current batch to a specific module with a specific state
        /// </summary>
        /// <param name="oWorkflowData"></param>
        /// <param name="moduleName"></param>
        /// <param name="stateName"></param>
        /// <returns></returns>
        public bool routeBatch(IACWorkflowData oWorkflowData, string moduleName, string stateName)
        {
            var m = GetWorkflowModuleByName(oWorkflowData, moduleName);
            var s = GetStateByName(oWorkflowData, stateName);
            if (m != null && s != null)
            {
                oWorkflowData.NextModule = m;
                oWorkflowData.NextState = s;
                return true;
            }
            return false;
        }
        public IACWorkflowModule GetWorkflowModuleByName(IACWorkflowData oWorkflowData, string moduleName)
        {
            foreach (IACWorkflowModule m in oWorkflowData.PossibleModules)
            {
                if (m.ID == moduleName)
                {
                    return m;
                }
            }
            return null;
        }

        public IACWorkflowState GetStateByName(IACWorkflowData oWorkflowData, string stateName)
        {
            foreach (IACWorkflowState s in oWorkflowData.PossibleStates)
            {
                if (s.Name == stateName)
                {
                    return s;
                }
            }
            return null;
        }

        /// <summary>
        /// Get a list of all custom storage strings that were set during setup in administration
        /// </summary>
        /// <param name="oWorkflowData"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetSetupCustomStorageStrings(IACWorkflowData oWorkflowData)
        {
            Dictionary<string, string> customStorageStrings = new Dictionary<string, string>();
            IACDataElement runtime = oWorkflowData.ExtractRuntimeACDataElement(0);
            IACDataElement batch = runtime.FindChildElementByName("Batch");
            IACDataElement setup = oWorkflowData.ExtractSetupACDataElement(0);
            IACDataElementCollection batchClasses = setup.FindChildElementByName("BatchClasses").FindChildElementsByName("BatchClass");
            foreach (IACDataElement batchClass in batchClasses)
            {
                if (batchClass["Name"] == batch["BatchClassName"])
                {
                    foreach (IACDataElement css in batchClass.FindChildElementByName("BatchClassCustomStorageStrings").FindChildElementsByName("BatchClassCustomStorageString"))
                    {
                        customStorageStrings.Add(css["Name"], css["Value"]);
                    }
                }
            }
            return customStorageStrings;
        }

        /// <summary>
        /// Get a batch custom storage string that was previously set in script
        /// </summary>
        /// <param name="oWorkflowData"></param>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private string GetBatchCustomStorageString(IACWorkflowData oWorkflowData, string name, string defaultValue = "")
        {
            try
            {
                return oWorkflowData.get_BatchCustomStorageString(name);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Set a batch custom storage string to retain batch specific information in script
        /// </summary>
        /// <param name="oWorkflowData"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        private void SetBatchCustomStorageString(IACWorkflowData oWorkflowData, string name, string value)
        {
            oWorkflowData.set_BatchCustomStorageString(name, value);
        }
        #endregion
    }
}
