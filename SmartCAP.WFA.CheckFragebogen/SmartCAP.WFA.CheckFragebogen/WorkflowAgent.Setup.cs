﻿using Kofax.Capture.AdminModule.InteropServices;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System;

namespace SmartCAP.WFA.CheckFragebogen
{
    [ProgId("SmartCAP.WFA.CheckFragebogen.Setup")]
    public class WorkflowAgentSetup : UserControl
    {
        private AdminApplication adminApplication;

        public AdminApplication Application
        {
            set
            {
                value.AddMenu("SmartCAP.WFA.CheckFragebogen.Setup", "SmartCAP.WFA.CheckFragebogen - Setup", "BatchClass");
                adminApplication = value;
            }
        }

        public WorkflowAgentSetup()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }

        public void ActionEvent(int EventNumber, object Argument, out int Cancel)
        {
            Cancel = 0;

            if ((KfxOcxEvent)EventNumber == KfxOcxEvent.KfxOcxEventMenuClicked && (string)Argument == "SmartCAP.WFA.CheckFragebogen.Setup")
            {
                SetupForm form = new SetupForm();
                form.ShowDialog(adminApplication.ActiveBatchClass);
            }
        }

    }
}
