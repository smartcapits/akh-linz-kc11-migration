﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartCAP.SmartKCBatch;

namespace SmartCAP.WFA.CheckFragebogen
{
    static class ContentChecks
    {
        public static bool CheckFragebogenIndexFields(Document document)
        {
            // True wenn Dokument komplett gültig ist (kann exportiert werden), False wenn Dokument mind. 1 Fehler hat (muss dann validiert werden)

            bool returnValue = true;
            bool checkResult = true;
            string singleChoiceFieldStartChar = "ABCMP";
            string mandatoryFieldStartChar = "ABP";
            string textFieldStartChar = "O";

            try
            {
                foreach (IndexField indexField in document.IndexFields)
                {
                    // 2. Indexfeldart erueuren -> SingleCoiche- oder MultipleCoiche-Check aufrufen, auf Pflichtfelder prüfen
                    // SingleChoice: Matrix1, Matrix2, C1, C2, C3, C4, C5, C6
                    // MultipleChoice mit einmaliger Prüfung bei mehreren Angaben: A, P
                    // Pflichtfelder (mind. 1 Angabe): A, P

                    // Checken ob Feld leer ist -> bei leerem Feld Error da jedes Feld befüllt sein muss
                    if (indexField.Value == "")
                    { 
                        checkResult = false; 
                    }
                    else
                    {
                        // SingleChoice-Felder checken
                        if (singleChoiceFieldStartChar.Contains(indexField.Name.Substring(0, 1)))
                        {
                            checkResult = CheckSingleChoiceField(indexField, "0", "-");
                        }

                        // Pflichtfelder checken
                        if (mandatoryFieldStartChar.Contains(indexField.Name.Substring(0, 1)))
                        {
                            checkResult = CheckMandatoryField(indexField, "0", "-");
                        }

                        // TextFeld checken
                        if (textFieldStartChar.Contains(indexField.Name.Substring(0, 1)))
                        {
                            checkResult = CheckTextField(indexField, "1");
                        }
                    }   

                    // 3. Return-Wert je nach ergebnis setzen. Ein Fehler macht Return-Wert schon "false"
                    if (!checkResult)
                    {
                        // Merken dass mind. 1 Feld einen Fehler hat -> Stapel muss in Validation gehen
                        returnValue = false;
                        // Aus IndexField-Loop ausssteigen da ein Fehler schon genügt um das Document in die Validierung zu holen. feldwerte werden hier keine verändert...
                        break;
                    }
                }
            }
            catch (Exception e)
            { throw new Exception("Error in CheckFragebogenIndexFields! Msg: " + e.Message); }

            return returnValue;
        }



        private static bool CheckSingleChoiceField(IndexField indexField, string uncheckedSymbol, string separatorChar)
        {
            // Checkt ob kein oder nur 1 Wert angegeben ist 

            bool returnValue = true;
            int checkedFieldsCount = 0;

            try
            {
                // Indexfeldwerte in Liste aufsplitten - macht Durchzählen der Werte einfacher
                List<string> fieldValues = new List<string>();
                fieldValues.InsertRange(0, indexField.Value.Split(separatorChar.ToCharArray()));

                // Alle Werte durchgehen, Zählen nicht-leerer Werte
                foreach (string fieldValue in fieldValues)
                {
                    if (fieldValue != uncheckedSymbol) { checkedFieldsCount++; }
                }

                // Schauen ob nicht mehr als 1 Feld gecheckt ist
                if (checkedFieldsCount > 1) { returnValue = false; }
            }
            catch (Exception e)
            { throw new Exception("Error in CheckSingleChoiceField! Msg: " + e.Message); }

            return returnValue;
        }


        private static bool CheckMandatoryField(IndexField indexField, string uncheckedSymbol, string separatorChar, int minNumberOfCheckedValues = 1)
        {
            // Checkt ob mind. die angegbene Anzahl von Werten angegeben sind (default = 1)

            bool returnValue = true;
            int checkedFieldsCount = 0;

            try
            {
                // Indexfeldwerte in Liste aufsplitten - macht Durchzählen der Werte einfacher
                List<string> fieldValues = new List<string>();
                fieldValues.InsertRange(0, indexField.Value.Split(separatorChar.ToCharArray()));

                // Alle Werte durchgehen, Zählen nicht-leerer Werte
                foreach (string fieldValue in fieldValues)
                {
                    if (!fieldValue.Equals(uncheckedSymbol)) { checkedFieldsCount++; }
                }

                // Schauen ob wir die Mindestanzahl gecheckter Werte erreicht haben
                if (checkedFieldsCount < minNumberOfCheckedValues) { returnValue = false; }
            }
            catch (Exception e)
            { throw new Exception("Error in CheckMandatoryField! Msg: " + e.Message); }

            return returnValue;
        }


        private static bool CheckTextField(IndexField indexField, string needsStopSymbol)
        {
            // Checkt ob im Textfeld was drinnen steht. True wenn Feld leer oder OK befüllt ist, false = Feld muss validiert werden

             if (indexField.Value == needsStopSymbol) { return false; }

            return true;
        }

    }
}
