﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartCAP.CM.SmartSort
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }

    public class DocumentEventArgs : EventArgs
    {
        public Document Document { get; private set; }

        public DocumentEventArgs(Document document)
        {
            Document = document;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    //public delegate void ACDataElementEventHandler(object sender, ACDataElementEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    public delegate void DocumentEventHandler(object sender, DocumentEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CM.SmartSort";
        public static string DisplayName = "SmartCAP.CM.SmartSort Service";
        public static string Description = "Kofax Capture Service for Custom Module SmartCAP.CM.SmartSort";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event DocumentEventHandler DocumentOpened;
        public event DocumentEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;
        public event TextEventHandler LogEvent;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value) || value == "0") { return true; }       // DisableBatchNotification key does not exist or is set to "0" - batch notification is enabled
            else { return false; }                                                  // DisableBatchNotificationKey exists - batch notification is disabled
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CM.SmartSort";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CM.SmartSort.exe", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        public void Logout()
        {
            login.Logout();
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null)
                throw new Exception("you need to login to KC first");

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here is where all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            bool batchHasErrors = false;

            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                Log.Debug("Initialize SmartBatch");
                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);

                Log.Debug("Loading settings");
                CustomModuleSettings settings = new CustomModuleSettings();
                CustomStorageString settingsCSS = smartBatch.Setup.BatchClass.SetupCustomStorageStrings.FirstOrDefault(css => css.Name == settings.SettingsCSSName);
                if (settingsCSS != null && settingsCSS.Value != "") { settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsCSS.Value, typeof(CustomModuleSettings)); }
                else { throw new Exception("Cannot load Settings-CSS '" + settings.SettingsCSSName + "' - please configure CustomModule 'SmartCAP.CM.SmartSort' for this BatchClass"); }

                Log.Debug("Processing Master-documents");
                List<Document> masterDocs = smartBatch.Documents.Where(d => d.FormTypeName == settings.MasterFormType).ToList();
                for (int masterDocIndex = 0; masterDocIndex < masterDocs.Count; masterDocIndex++)
                {
                    Document masterDoc = masterDocs[masterDocIndex];
                    Document sortDoc = null;
                    string masterDocErrors = "";

                    LogDocumentOpened(masterDoc); // Log the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode                    
                    //LogMessage("This message is logged to the CM GUI, if custom module is executed in interactive mode"); // Log a message to the event list, if the custom module is executed in interactive mode                    

                    // Search for each SortDocument in SearchRealm
                    Log.Debug("Processing Master-documents");
                    settings.SortItemSettings.ForEach(sI =>
                    {
                        switch (sI.SearchRealm)
                        {
                            case "To next Master FormType":
                                List<Document> searchRealm = new List<Document>();
                                // Check if we´re at the last MasterDoc in batch. If so search till the end of the batch, if not search till next MasterDoc
                                if (masterDocIndex == masterDocs.Count - 1)
                                { searchRealm = smartBatch.Documents; }
                                else
                                { searchRealm = smartBatch.Documents.TakeWhile(d => d.GUID != masterDocs[masterDocIndex + 1].GUID).ToList(); }

                                sortDoc = searchRealm.FirstOrDefault(d => d.FormTypeName == sI.FormType);
                                break;

                            case "Within Folder":
                                sortDoc = masterDoc.Folder.Documents.FirstOrDefault(d => d.FormTypeName == sI.FormType);
                                break;

                            case "Within Batch":
                                sortDoc = smartBatch.Documents.FirstOrDefault(d => d.FormTypeName == sI.FormType);
                                break;
                        }

                        // Combine documents if found or raise error if not found and sortDoc is mandatory
                        if (sortDoc == null && sI.PresenceMandatory)
                        {
                            LogMessage("Document {0}: mandatory SortItem {1} could not be found {2}".Replace("{0}", masterDoc.UniqueDocumentID).Replace("{1}", sI.FormType).Replace("{2}", sI.SearchRealm));
                            masterDocErrors += "Cannot find mandatory SortItem \"{0}\" {1}; ".Replace("{0}", sI.FormType).Replace("{1}", sI.SearchRealm);
                        }
                        else if (sortDoc != null)
                        {
                            LogDocumentOpened(sortDoc);
                            LogMessage("Sorting document {0} to document {1}".Replace("{0}", sortDoc.UniqueDocumentID).Replace("{1}", masterDoc.UniqueDocumentID));
                            KofaxTools.CombineDocuments(masterDoc, sortDoc, sI.InheritIndexValues, sI.OverwriteIndexFields);
                            
                            LogDocumentClosed(sortDoc);
                            sortDoc.Delete();
                        }

                    });

                    // Reject MasterDoc if there is 1 or more errors
                    if (masterDocErrors != "")
                    {
                        LogMessage("Rejecting document {0} due to missing SortItems".Replace("{0}", masterDoc.UniqueDocumentID));
                        masterDoc.Rejected = true;
                        masterDoc.Note = "Error in SmartSort: " + masterDocErrors;
                        batchHasErrors = true;
                    }

                    LogDocumentClosed(masterDoc); // Log the document closing to the event list, if the custom module is executed in interactive mode
                }

                #region code check settings, sample serial document processing, moving pages to documents, skip validation
                // <<< Check settings >>>
                //if (settings.Count == 0) { throw new Exception("Für das CustomModule '" + ServiceName + "' wurden keine Einstellungen gefunden. Bitte konfigurieren Sie das CustomModule für diese Stapelklasse im KC Verwaltungsmodul"); }
                // setting name is the name of the control on setup form.
                // Example: string docClass = settings["SmartCAP.CM.SmartSort.cboDocumentClass"]).Value

                //// <<< Serial document processing >>>
                //// Checks every document (single page documents) in batch for presence of start or stop separator sheets
                //// If start barcode was found (in IndexField "Barcode"), group all oncoming documents (pages) into 1 document unless stop barcode or another start barcode is found
                //string startBarcode = "99999999";   // Get from CM settings
                //string stopBarcode = "00000000";
                //Document targetDoc = null;

                //Log.Debug("Processing documents");
                //foreach (Document doc in smartBatch.Documents)
                //{
                //    Log.Debug("Starting processing document");
                //    DocumentOpened?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));

                //    // Check for start and stop barcode on separator sheet
                //    if (doc.IndexFields.First(iF => iF.Name == "Barcode").Value == startBarcode)
                //    {
                //        doc.Pages[0].Delete();
                //        doc.IndexFields.First(iF => iF.Name == "Barcode").Value = "";
                //        targetDoc = null;
                //    }
                //    else if (doc.IndexFields.First(iF => iF.Name == "Barcode").Value == stopBarcode)
                //    {
                //        targetDoc = null;
                //        doc.Delete();
                //    }
                //    else
                //    {
                //        if (targetDoc != null)       // If we´re within a multipage document, move doc pages there
                //        {
                //            doc.Pages.ForEach(p => p.MoveToDocument(targetDoc));
                //            doc.Delete();
                //        }
                //    }

                //    Log.Debug("Finished processing document");
                //    DocumentClosed?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));
                //}
                //Log.Debug("Finished processing documents");

                // example to skip validation (send to next module after index)
                //if (smartBatch.Modules.Any(m => m.ID == "index.exe")) { smartBatch.Close(KfxDbState.KfxDbBatchReady, smartBatch.Modules.SkipWhile(m => m.ID != "index.exe").Skip(1).First()); }
                //else { smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext, ""); }
                #endregion code sample

                if (batchHasErrors)
                {
                    Log.Debug("Closing batch - moving to QC module");
                    smartBatch.Close(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException);
                }
                else
                {
                    Log.Debug("Closing batch - moving to next module");
                    smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext);
                }

                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                }
            }
        }

        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here happens the action
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }

        }

        #endregion login and processing methods

        #region GUIhelpers

        /// <summary>
        /// Logs a message to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            LogEvent?.Invoke(this, new TextEventArgs(message));
        }

        /// <summary>
        /// Logs the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentOpened(Document document)
        {
            DocumentOpened?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Logs the document closing to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentClosed(Document document)
        {
            DocumentClosed?.Invoke(this, new DocumentEventArgs(document));
        }

        #endregion
    }
}
