﻿using SmartCAP.SmartKCBatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SmartCAP.CM.SmartSort
{
    public static class KofaxTools
    {
        public static void CombineDocuments(Document targetDoc, Document sourceDoc, bool inheritIndexFields, bool overwriteIndexFields)
        { 
            if (inheritIndexFields)
            {
                // Only go for IndexFields that have a zone on the document (instead of all document´s indexfields)
                sourceDoc.IndexFields.Where(sdIF => sdIF.Parameters.First(para => para.Name == "ZoneWidth").Value != "0").ToList().ForEach(sdIF =>
                {
                    IndexField targetDocIF;
                    targetDocIF = targetDoc.IndexFields.FirstOrDefault(tdIF => tdIF.Name == sdIF.Name);
                    if (targetDocIF != null)
                    {
                        if (overwriteIndexFields || (targetDocIF.Value == "" && !overwriteIndexFields))
                        {
                            targetDocIF.Value = sdIF.Value;
                            targetDocIF.Parameters.First(para => para.Name == "RecognitionType").Value = sdIF.Parameters.First(para => para.Name == "RecognitionType").Value;
                            targetDocIF.Parameters.First(para => para.Name == "ZoneTop").Value = sdIF.Parameters.First(para => para.Name == "ZoneTop").Value;
                            targetDocIF.Parameters.First(para => para.Name == "ZoneLeft").Value = sdIF.Parameters.First(para => para.Name == "ZoneLeft").Value;
                            targetDocIF.Parameters.First(para => para.Name == "ZoneHeight").Value = sdIF.Parameters.First(para => para.Name == "ZoneHeight").Value;
                            targetDocIF.Parameters.First(para => para.Name == "ZoneWidth").Value = sdIF.Parameters.First(para => para.Name == "ZoneWidth").Value;
                            targetDocIF.Parameters.First(para => para.Name == "PageNumber").Value = (targetDoc.Pages.Count + int.Parse(sdIF.Parameters.First(para => para.Name == "PageNumber").Value)).ToString();
                        }
                    }
                });
            }

            sourceDoc.Pages.ForEach(p => p.MoveToDocument(targetDoc));
        }

    }
}
