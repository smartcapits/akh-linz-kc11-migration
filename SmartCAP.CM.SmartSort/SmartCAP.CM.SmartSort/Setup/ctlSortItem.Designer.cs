﻿namespace SmartCAP.CM.SmartSort
{
    partial class ctlSortItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkInheritIndexFields = new System.Windows.Forms.CheckBox();
            this.cboFormType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkPresenceMandatory = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSearchRealm = new System.Windows.Forms.ComboBox();
            this.chkOverwriteNonEmptyIndexFields = new System.Windows.Forms.CheckBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkInheritIndexFields
            // 
            this.chkInheritIndexFields.AutoSize = true;
            this.chkInheritIndexFields.Location = new System.Drawing.Point(211, 42);
            this.chkInheritIndexFields.Name = "chkInheritIndexFields";
            this.chkInheritIndexFields.Size = new System.Drawing.Size(140, 17);
            this.chkInheritIndexFields.TabIndex = 0;
            this.chkInheritIndexFields.Text = "Inherit IndexField values";
            this.chkInheritIndexFields.UseVisualStyleBackColor = true;
            // 
            // cboFormType
            // 
            this.cboFormType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormType.FormattingEnabled = true;
            this.cboFormType.Location = new System.Drawing.Point(74, 7);
            this.cboFormType.Margin = new System.Windows.Forms.Padding(2);
            this.cboFormType.Name = "cboFormType";
            this.cboFormType.Size = new System.Drawing.Size(186, 21);
            this.cboFormType.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "FormType:";
            // 
            // chkPresenceMandatory
            // 
            this.chkPresenceMandatory.AutoSize = true;
            this.chkPresenceMandatory.Location = new System.Drawing.Point(15, 42);
            this.chkPresenceMandatory.Name = "chkPresenceMandatory";
            this.chkPresenceMandatory.Size = new System.Drawing.Size(178, 17);
            this.chkPresenceMandatory.TabIndex = 4;
            this.chkPresenceMandatory.Text = "Must be present in Search-realm";
            this.chkPresenceMandatory.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Search-distance:";
            // 
            // cboSearchRealm
            // 
            this.cboSearchRealm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchRealm.FormattingEnabled = true;
            this.cboSearchRealm.Location = new System.Drawing.Point(373, 7);
            this.cboSearchRealm.Margin = new System.Windows.Forms.Padding(2);
            this.cboSearchRealm.Name = "cboSearchRealm";
            this.cboSearchRealm.Size = new System.Drawing.Size(206, 21);
            this.cboSearchRealm.TabIndex = 5;
            // 
            // chkOverwriteNonEmptyIndexFields
            // 
            this.chkOverwriteNonEmptyIndexFields.AutoSize = true;
            this.chkOverwriteNonEmptyIndexFields.Location = new System.Drawing.Point(373, 42);
            this.chkOverwriteNonEmptyIndexFields.Name = "chkOverwriteNonEmptyIndexFields";
            this.chkOverwriteNonEmptyIndexFields.Size = new System.Drawing.Size(179, 17);
            this.chkOverwriteNonEmptyIndexFields.TabIndex = 7;
            this.chkOverwriteNonEmptyIndexFields.Text = "Overwrite non-epmty IndexFields";
            this.chkOverwriteNonEmptyIndexFields.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(560, 36);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(19, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ctlSortItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.chkOverwriteNonEmptyIndexFields);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboSearchRealm);
            this.Controls.Add(this.chkPresenceMandatory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboFormType);
            this.Controls.Add(this.chkInheritIndexFields);
            this.Name = "ctlSortItem";
            this.Size = new System.Drawing.Size(588, 70);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkInheritIndexFields;
        private System.Windows.Forms.ComboBox cboFormType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkPresenceMandatory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboSearchRealm;
        private System.Windows.Forms.CheckBox chkOverwriteNonEmptyIndexFields;
        private System.Windows.Forms.Button btnDelete;
    }
}
