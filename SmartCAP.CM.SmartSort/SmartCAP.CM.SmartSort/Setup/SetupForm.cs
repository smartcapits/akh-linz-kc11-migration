﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace SmartCAP.CM.SmartSort
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IBatchClass BatchClass;

        public SetupForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(IBatchClass batchClass)
        {
            BatchClass = batchClass;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }
        #endregion

        public void InitializePanel()
        {
            // Load all available FormTypes into combo box
            foreach (DocumentClass dc in BatchClass.DocumentClasses) { foreach (FormType ft in dc.FormTypes) { cboMasterFormType.Items.Add(ft.Name); } }
        }

        #region Settings
        public void LoadSettings()
        {
            CustomModuleSettings settings = new CustomModuleSettings();

            try
            {
                string settingsXML = GetCustomStorageString(settings.SettingsCSSName);
                if (settingsXML != "")
                {
                    settings = (CustomModuleSettings)SerializationTool.DeSerialize(settingsXML, typeof(CustomModuleSettings));

                    cboMasterFormType.Text = settings.MasterFormType;
                    foreach (SortItemSetting siSetting in settings.SortItemSettings) { flpSortItems.Controls.Add(new ctlSortItem(BatchClass, siSetting)); }
                }
            }
            catch (Exception)
            { }  // Default-Settings = Empty ruleset
        }

        public bool VerifySettings()
        {
            string errorMsg = "";
            int ctrItemIndex = 1;

            try
            {
                // Perform checks here
                if (flpSortItems.Controls.Count == 0) { errorMsg += "Add at least 1 SortItem" + "\r\n"; }

                foreach (ctlSortItem condItem in flpSortItems.Controls)
                {
                    if (condItem.Controls.Find("cboFormType", true)[0].Text == "") { errorMsg += "SortItem #" + ctrItemIndex.ToString() + ": Please specify a FormType" + "\r\n"; }
                    ctrItemIndex++;
                }

                if (errorMsg != "") { throw new ConditionValidationException("Please correct the following errors:\r\n" + errorMsg); }
            }
            catch (ConditionValidationException cve)
            {
                MessageBox.Show(cve.Message, "Error saving settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (Exception)
            { throw; }

            return true;
        }

        public void SaveSettings()
        {
            // save all control settings to CSS as XML stream
            CustomModuleSettings settings = new CustomModuleSettings();

            settings.MasterFormType = cboMasterFormType.Text;
            GetAll(flpSortItems, typeof(ctlSortItem)).ToList().ForEach(ctlSI => settings.SortItemSettings.Add(new SortItemSetting((ctlSortItem)ctlSI)));

            SetCustomStorageString(settings.SettingsCSSName, SerializationTool.Serialize(settings));
        }
        #endregion


        #region UserInteraction
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            { BatchClass.set_CustomStorageString(name, value); }
            catch
            { }
        }

        private string GetCustomStorageString(string name, string defaultValue = "")
        {
            try
            { return BatchClass.get_CustomStorageString(name); }
            catch
            { return defaultValue; }
        }

        #endregion

        private void btnAddSortItem_Click(object sender, EventArgs e)
        {
            flpSortItems.Controls.Add(new ctlSortItem(BatchClass));
        }
    }
}
