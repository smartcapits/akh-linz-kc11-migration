﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kofax.Capture.AdminModule.InteropServices;

namespace SmartCAP.CM.SmartSort
{
    public partial class ctlSortItem : UserControl
    {
        private IBatchClass _batchClass;

        public ctlSortItem(IBatchClass batchClass)
        {
            _batchClass = batchClass;

            InitializeComponent();
            InitializePanel();
        }

        public ctlSortItem(IBatchClass batchClass, SortItemSetting siSettings)
        {
            _batchClass = batchClass;

            InitializeComponent();
            InitializePanel();
            SetPanel(siSettings);
        }


        private void InitializePanel()
        {
            foreach (DocumentClass dc in _batchClass.DocumentClasses) { foreach (FormType ft in dc.FormTypes) { cboFormType.Items.Add(ft.Name); } }

            cboSearchRealm.Items.Add("To next Master FormType");
            if (_batchClass.FolderClasses.Count > 0) { cboSearchRealm.Items.Add("Within Folder"); }
            cboSearchRealm.Items.Add("Within Batch");
            cboSearchRealm.Text = cboSearchRealm.Items[0].ToString();
        }

        private void SetPanel(SortItemSetting siSettings)
        {
            this.Controls.Find("cboFormType", true)[0].Text = siSettings.FormType;
            this.Controls.Find("cboSearchRealm", true)[0].Text = siSettings.SearchRealm;

            ((CheckBox)this.Controls.Find("chkPresenceMandatory", true)[0]).Checked = siSettings.PresenceMandatory;
            ((CheckBox)this.Controls.Find("chkInheritIndexFields", true)[0]).Checked = siSettings.InheritIndexValues;
            ((CheckBox)this.Controls.Find("chkOverwriteNonEmptyIndexFields", true)[0]).Checked = siSettings.OverwriteIndexFields;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
    }
}
