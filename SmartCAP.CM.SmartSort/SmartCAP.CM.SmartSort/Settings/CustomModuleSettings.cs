﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCAP.CM.SmartSort
{
    public class CustomModuleSettings
    {
        public string SettingsCSSName { get; set; } = "SmartCAP.CM.SmartSort.Settings";

        public string MasterFormType { get; set; } = "";
        public List<SortItemSetting> SortItemSettings { get; set; } = new List<SortItemSetting>();
    }

    public class SortItemSetting
    {
        public string FormType { get; set; } = "";
        public string SearchRealm { get; set; } = "";
        public bool PresenceMandatory { get; set; } = true;
        public bool InheritIndexValues { get; set; } = true;
        public bool OverwriteIndexFields { get; set; } = false;

        public SortItemSetting() { }
        public SortItemSetting(string formType, string searchRealm, bool presenceMandatory, bool inheritIndexValues, bool overwriteIndexFields)
        {
            this.FormType = formType;
            this.SearchRealm = searchRealm;
            this.PresenceMandatory = presenceMandatory;
            this.InheritIndexValues = inheritIndexValues;
            this.OverwriteIndexFields = overwriteIndexFields;
        }
        public SortItemSetting(ctlSortItem sortItemControl)
        {
            this.FormType = sortItemControl.Controls.Find("cboFormType", true)[0].Text;
            this.SearchRealm = sortItemControl.Controls.Find("cboSearchRealm", true)[0].Text;
            this.PresenceMandatory = ((CheckBox)sortItemControl.Controls.Find("chkPresenceMandatory", true)[0]).Checked;
            this.InheritIndexValues = ((CheckBox)sortItemControl.Controls.Find("chkInheritIndexFields", true)[0]).Checked;
            this.OverwriteIndexFields = ((CheckBox)sortItemControl.Controls.Find("chkOverwriteNonEmptyIndexFields", true)[0]).Checked;
        }
    }
}
