﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace SmartCAP.CM.SmartSort
{
    static class SerializationTool
    {
        // XML string to object
        public static object DeSerialize(string settingsXML, Type outputObjectType)
        {
            try
            {
                XmlSerializer xmlSer = new XmlSerializer(outputObjectType);
                using (TextReader textReader = new StringReader(settingsXML)) { return xmlSer.Deserialize(textReader); }
            }
            catch (Exception ex)
            { throw new Exception("Error in SeritalizationTool.DeSerialize! Message: " + ex.Message); }
        }


        // object to XML string
        public static string Serialize(object outputObject)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();

                using (TextWriter textWriter = new StringWriter(stringBuilder)) { new XmlSerializer(outputObject.GetType()).Serialize(textWriter, outputObject); }
                return stringBuilder.ToString();
            }

            catch (Exception ex)
            { throw new Exception("Error in SeritalizationTool.Serialize! Message: " + ex.Message); }
        }
    }
}
