﻿using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CM.SmartSort
{
    partial class CustomModuleService : ServiceBase
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        private CustomModule cm;
        private string user = "";
        private string password = "";
        public CustomModuleService(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeComponent();
            InitializeLogging();

            // try to get username and password from arguments
            user = args.FirstOrDefault(a => a.StartsWith("-u:", StringComparison.InvariantCultureIgnoreCase));
            user = user == null ? "" : user.Substring(3).Trim('"');
            password = args.FirstOrDefault(a => a.StartsWith("-p:", StringComparison.InvariantCultureIgnoreCase));
            password = password == null ? "" : password.Substring(3).Trim('"');
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.

            // login to kc
            cm = new CustomModule();
            cm.Login(user, password);

            if (CustomModule.BatchNotificationEnabled == true)
            {
                cm.ListenForNewBatches();
            }
            else
            {
                cm.PollForNewBatches();
            }

            Log.Info("SmartCAP.CM.SmartSort service started successfully");
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.

            // logout from kc
            cm.Logout();

            Log.Info("SmartCAP.CM.SmartSort service stopped successfully");
        }
    }
}
