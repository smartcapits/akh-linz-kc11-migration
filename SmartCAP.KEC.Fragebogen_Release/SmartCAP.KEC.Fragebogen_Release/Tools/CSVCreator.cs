﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Kofax.ReleaseLib;

namespace SmartCAP.KEC.Fragebogen_Release
{
    public static class CSVCreator
    {
        public static List<string> CreateCSVForDocument(Values indexValues, string csvSeparatorChar, string splitFieldExpression, string splitFieldSeparator, string textFieldExpression, bool valuesInHighCommas)
        {
            // Gibt 2 Zeilen zurück: 1. Zeile = Spaltennamen; 2. Zeile = Indexwerte
            // Splittet dabei OMR MultiChoice-Felder (die SplitFieldExpression matchen) in Einzelfelder auf 

            string columnNames = "";
            string values = "";
            List<KeyValuePair<string, string>> splitFieldValues;
            List<string> returnValue = new List<string>();

            try
            {
                // Durch alle Indexwerte gehen
                foreach (Value value in indexValues)
                {
                    // Schauen ob Indexwert auf SplitFieldExpression matcht -> Feld splitten
                    if (!splitFieldExpression.Equals("") && Regex.IsMatch(value.SourceName, splitFieldExpression))
                    {
                        // Feld aufsplitten
                        splitFieldValues = SplitMultiChoiceField(value.SourceName, value.Value, splitFieldSeparator);

                        //KVPairs in Result aufsplitten
                        foreach (KeyValuePair<string, string> kvPair in splitFieldValues)
                        {
                            if (valuesInHighCommas)
                            { columnNames = columnNames + "" + kvPair.Key + "" + csvSeparatorChar; values = values + "" + kvPair.Value + "" + csvSeparatorChar; }
                            else
                            { columnNames = columnNames + kvPair.Key + csvSeparatorChar; values = values + kvPair.Value + csvSeparatorChar; }
                        }
                    }

                    // Schauen ob Indexwert auf TextFieldExpression matcht -> CSVSeparatorChar aus Textinhalt filtern
                    else if (!textFieldExpression.Equals("") && Regex.IsMatch(value.SourceName, textFieldExpression))
                    {
                        // Gleiche Ausgabe wie Feld ohne Aufsplittung, nur Filterung um CSVSeparator-Char (damit CSV-Integrität gewährt bleibt)
                        if (valuesInHighCommas)
                        {
                            columnNames = columnNames + "" + value.SourceName + "" + csvSeparatorChar;
                            // Falls kein Text angegeben Leerstring statt "0" zurückgeben
                            if (value.Value.Equals("0"))
                            { values = values + "" + "" + csvSeparatorChar; }
                            else
                            { values = values + "" + KofaxReleaseTools.FilterOut(value.Value, csvSeparatorChar) + "" + csvSeparatorChar; }
                        }
                        else
                        {
                            columnNames = columnNames + value.SourceName + csvSeparatorChar;
                            // Falls kein Text angegeben Leerstring statt "0" zurückgeben
                            if (value.Value.Equals("0"))
                            { values = values + "'" + "'" + csvSeparatorChar; }
                            else
                            { values = values + "'" + KofaxReleaseTools.FilterOut(value.Value, csvSeparatorChar) + "'" + csvSeparatorChar; }
                        }
                    }

                    // Anderenfalls Wert einfach so hinzufügen
                    else
                    {
                        if (valuesInHighCommas)
                        { columnNames = columnNames + "" + value.SourceName + "" + csvSeparatorChar; values = values + "" + value.Value + "" + csvSeparatorChar; }
                        else
                        { columnNames = columnNames + value.SourceName + csvSeparatorChar; values = values + value.Value + csvSeparatorChar; }
                    }
                }

                // CSV-SeparatorChar als letztes Zeichen rausfiltern
                if (columnNames.Substring(columnNames.Length - 1, 1).Equals(csvSeparatorChar)) { columnNames = columnNames.Remove(columnNames.Length - 1, 1); }
                if (values.Substring(values.Length - 1, 1).Equals(csvSeparatorChar)) { values = values.Remove(values.Length - 1, 1); }

                // Values hinzufügen
                returnValue.Add(columnNames);
                returnValue.Add(values);
            }
            catch (Exception e)
            { throw new Exception("Error in CreateCSVForDocument! Msg: " + e.Message); }

            return returnValue;
        }


        private static List<KeyValuePair<string, string>> SplitMultiChoiceField(string fieldName, string fieldValue, string separatorChar)
        {
            // Splittet Multi-OMR-Felder in Einzelfelder auf. Zählt letzte Stellen (numerisch) hoch, sonst zählt bei 1 hoch

            List<KeyValuePair<string, string>> returnValue = new List<KeyValuePair<string, string>>();
            string fieldStartNumber = "";
            string fieldNumber = "1";
            string fieldNameWithoutStartNumber;

            try
            {
                // Feldnummer des ersten Feldes eruieren (Zahl am Ende des Feldnamens) - keine Feldnummer im Namen = Feldname + Laufnummer wird verwendet
                // Nummer als String da die Anzahl führender Nullen behalten werden muss
                fieldStartNumber = GetFieldStartNumber(fieldName);
                if (!fieldStartNumber.Equals(""))
                {
                    fieldNumber = fieldStartNumber;     // StartNummer aus IndexFeld-Namen setzen
                    fieldNameWithoutStartNumber = GetFieldNameWithoutStartNumber(fieldName, fieldNumber.ToString());        // Nummernteil aus Indexfeldnamen ausschneiden
                }
                else
                { fieldNameWithoutStartNumber = fieldName; }

                // Feldinhalt mit Hilfe des SeparatorChars splitten
                string[] fieldValues = fieldValue.Split(separatorChar.ToCharArray());

                // Für jeden Wert den passenden Feldnamen erzeugen
                foreach (string value in fieldValues)
                {
                    // Feldname und -Wert als KVPair in Liste speichern, dabei angekreuzte Werte als "1" repräsentieren, nicht angekreuzte als "0"
                    if (value.Equals("0") || value.Equals(""))
                    { returnValue.Add(new KeyValuePair<string, string>(fieldNameWithoutStartNumber + fieldNumber, "0")); }
                    else
                    { returnValue.Add(new KeyValuePair<string, string>(fieldNameWithoutStartNumber + fieldNumber, "1")); }

                    // FeldNummer erhöhen
                    fieldNumber = AccumulateStringNumber(fieldNumber);
                }
            }
            catch (Exception e)
            { throw new Exception("Error in SplitMultiChoiceField! Msg: " + e.Message); }

            return returnValue;
        }


        private static string GetFieldNameWithoutStartNumber(string fullFieldName, string startNumber)
        {
            // Entfernt die StartNumber aus dem String, so weit hinten wie möglich
            string returnValue = fullFieldName;

            try
            {
                // Schauen wie oft Startnumber im String vorkommt
                int lastOccurence = fullFieldName.LastIndexOf(startNumber);

                // Von hinten nach vorne schauen wo Zahl aufhört
                if (lastOccurence > -1)
                {
                    returnValue = fullFieldName.Substring(0, lastOccurence);
                }
            }
            catch (Exception e)
            { throw new Exception("Error in GetFieldNameWithoutStartNumber! Msg: " + e.Message); }

            return returnValue;
        }


        private static string GetFieldStartNumber(string fieldName)
        {
            string counterValue = "";

            try
            {
                // Von hinten nach vorne schauen wo Zahl aufhört
                for (int i = fieldName.Length - 1; i >= 0; i--)
                {
                    // Schauen ob Stelle eine Zahl ist - sonst Suche stoppen
                    if ("0123456789".Contains(fieldName.Substring(i, 1))) { counterValue = fieldName.Substring(i, 1) + counterValue; }
                    else { break; }
                }
            }
            catch (Exception e)
            { throw new Exception("Error in GetFieldStartNumber! Msg: " + e.Message); }

            return counterValue;
        }


        private static string AccumulateStringNumber(string inputNumber)
        {
            string returnValue = "";
            int intNumber;

            // Schauen ob wir auch wirklich eine Zahl haben
            if (inputNumber.Length > 0 && Int32.TryParse(inputNumber, out intNumber))
            {
                returnValue = (intNumber + 1).ToString().PadLeft(inputNumber.Length, "0".ToCharArray()[0]);
            }

            return returnValue;
        }

    }
}
