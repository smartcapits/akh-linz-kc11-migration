﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Kofax.ReleaseLib;

namespace SmartCAP.KEC.Fragebogen_Release
{
    public static class KofaxReleaseTools
    {
        public static string GetSetupCSSValue(ReleaseSetupData setupData, string cssName)
        {
            try
            {
                foreach (CustomProperty property in setupData.CustomProperties)
                {
                    if (property.Name.Equals(cssName)) { return property.Value; }
                }

                // If we could not find the property, raise an error
                throw new Exception("CSS " + cssName + " does not exist!");
            }
            catch (Exception e)
            {
                throw new Exception("Error listing CSS-Values! Msg: " + e.Message);
            }
        }


        public static List<string> CopyImageFiles(ReleaseData releaseData, string folder, string subFolder = "")
        {
            try
            {
                string targetFileName;
                List<string> releasedFiles = new List<string>();

                // See if Release-Folder exists. If not, create it
                if (!Directory.Exists(Path.Combine(folder, subFolder)))
                { Directory.CreateDirectory(Path.Combine(folder, subFolder)); }

                // Copy image files
                releaseData.ImageFiles.Copy(Path.Combine(folder, subFolder));

                // Rename released files
                string fileName = GetUniqueDocumentID(releaseData);
                string extension = ".tif";

                // Build Target FileName
                targetFileName = Path.Combine(Path.Combine(folder, subFolder), fileName) + extension;
                targetFileName = FilterOut(targetFileName, @"\/:*?\""<>|");

                // Rename released files (only supports multipage yet!)
                File.Move(Path.Combine(Path.Combine(folder, subFolder), GetUniqueDocumentID(releaseData)) + extension, targetFileName);

                // Add Target File to list
                releasedFiles.Add(targetFileName);

                return releasedFiles;
            }
            catch (Exception e)
            { throw new Exception("Error in CopyImageFiles! Msg: " + e.Message); }
        }


        public static List<string> CopyPDFFiles(ReleaseData releaseData, string folder, string subFolder = "")
        {
            try
            {
                string targetFileName;
                List<string> releasedFiles = new List<string>();

                // Check if SourceFileName is set - so PDF has been created
                string sourceFileName = releaseData.KofaxPDFFileName;

                if (!(sourceFileName == null))
                {
                    // See if Release-Folder exists. If not, create it
                    if (!Directory.Exists(Path.Combine(folder, subFolder)))
                    { Directory.CreateDirectory(Path.Combine(folder, subFolder)); }

                    // Copy PDF files
                    releaseData.CopyKofaxPDFFileToPath(Path.Combine(folder, subFolder));

                    // Rename released files
                    string fileName = GetUniqueDocumentID(releaseData); 
                    string extension = ".pdf";
 
                    // Build Target FileName
                    targetFileName = Path.Combine(Path.Combine(folder, subFolder), fileName) + extension;
                    targetFileName = FilterOut(targetFileName, @"\/:*?\""<>|");

                    // Rename released files (only supports multipage yet!)
                    File.Move(Path.Combine(Path.Combine(folder, subFolder), GetUniqueDocumentID(releaseData)) + extension, targetFileName);

                    // Add Target File to list
                    releasedFiles.Add(targetFileName);
                }

                return releasedFiles;
            }
            catch (Exception e)
            { throw new Exception("Error in CopyPDFFiles! Msg: " + e.Message); }
        }


        public static string GetUniqueDocumentID(ReleaseData relData)
        {
            return relData.UniqueDocumentID.ToString("X8");
        }


        public static string FilterOut(string input, string bannedChars)
        {
            string Abl = "";

            for (int i = 0; i < input.Length; i++)
            {
                if (!bannedChars.Contains(input.Substring(i, 1)))
                { Abl = Abl + input.Substring(i, 1); }
            }

            return Abl;
        }

        public static string FilterPositive(string input, string allowedChars)
        {
            string Abl = "";

            for (int i = 0; i < input.Length; i++)
            {
                if (allowedChars.Contains(input.Substring(i, 1)))
                { Abl = Abl + input.Substring(i, 1); }
            }

            return Abl;
        }


    }
}
