﻿namespace SmartCAP.KEC.Fragebogen_Release
{
    partial class AddIndexLink
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFixedValue = new System.Windows.Forms.TextBox();
            this.cboBatchField = new System.Windows.Forms.ComboBox();
            this.cboIndexField = new System.Windows.Forms.ComboBox();
            this.rbFixedValue = new System.Windows.Forms.RadioButton();
            this.rbBatchField = new System.Windows.Forms.RadioButton();
            this.rbIndexField = new System.Windows.Forms.RadioButton();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFixedValue);
            this.groupBox1.Controls.Add(this.cboBatchField);
            this.groupBox1.Controls.Add(this.cboIndexField);
            this.groupBox1.Controls.Add(this.rbFixedValue);
            this.groupBox1.Controls.Add(this.rbBatchField);
            this.groupBox1.Controls.Add(this.rbIndexField);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 135);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Verknüpfungstyp";
            // 
            // txtFixedValue
            // 
            this.txtFixedValue.Location = new System.Drawing.Point(118, 96);
            this.txtFixedValue.Name = "txtFixedValue";
            this.txtFixedValue.Size = new System.Drawing.Size(207, 20);
            this.txtFixedValue.TabIndex = 2;
            // 
            // cboBatchField
            // 
            this.cboBatchField.FormattingEnabled = true;
            this.cboBatchField.Location = new System.Drawing.Point(118, 62);
            this.cboBatchField.Name = "cboBatchField";
            this.cboBatchField.Size = new System.Drawing.Size(207, 21);
            this.cboBatchField.TabIndex = 1;
            // 
            // cboIndexField
            // 
            this.cboIndexField.FormattingEnabled = true;
            this.cboIndexField.Location = new System.Drawing.Point(118, 29);
            this.cboIndexField.Name = "cboIndexField";
            this.cboIndexField.Size = new System.Drawing.Size(207, 21);
            this.cboIndexField.TabIndex = 1;
            // 
            // rbFixedValue
            // 
            this.rbFixedValue.AutoSize = true;
            this.rbFixedValue.Location = new System.Drawing.Point(16, 97);
            this.rbFixedValue.Name = "rbFixedValue";
            this.rbFixedValue.Size = new System.Drawing.Size(73, 17);
            this.rbFixedValue.TabIndex = 0;
            this.rbFixedValue.TabStop = true;
            this.rbFixedValue.Text = "Fixer Wert";
            this.rbFixedValue.UseVisualStyleBackColor = true;
            // 
            // rbBatchField
            // 
            this.rbBatchField.AutoSize = true;
            this.rbBatchField.Location = new System.Drawing.Point(16, 63);
            this.rbBatchField.Name = "rbBatchField";
            this.rbBatchField.Size = new System.Drawing.Size(72, 17);
            this.rbBatchField.TabIndex = 0;
            this.rbBatchField.TabStop = true;
            this.rbBatchField.Text = "Stapelfeld";
            this.rbBatchField.UseVisualStyleBackColor = true;
            // 
            // rbIndexField
            // 
            this.rbIndexField.AutoSize = true;
            this.rbIndexField.Location = new System.Drawing.Point(16, 30);
            this.rbIndexField.Name = "rbIndexField";
            this.rbIndexField.Size = new System.Drawing.Size(68, 17);
            this.rbIndexField.TabIndex = 0;
            this.rbIndexField.TabStop = true;
            this.rbIndexField.Text = "Indexfeld";
            this.rbIndexField.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(91, 157);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(68, 32);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(206, 162);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AddIndexLink
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 197);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddIndexLink";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Indexverknüpfung hinzufügen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbIndexField;
        private System.Windows.Forms.RadioButton rbFixedValue;
        private System.Windows.Forms.RadioButton rbBatchField;
        private System.Windows.Forms.TextBox txtFixedValue;
        private System.Windows.Forms.ComboBox cboBatchField;
        private System.Windows.Forms.ComboBox cboIndexField;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}