﻿namespace SmartCAP.KEC.Fragebogen_Release
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabIndex = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtTextFieldExpression = new System.Windows.Forms.TextBox();
            this.txtSplitFieldSeparator = new System.Windows.Forms.TextBox();
            this.txtSplitFieldExpression = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkColumNamesInFirstRow = new System.Windows.Forms.CheckBox();
            this.chkSetValuesInHighCommas = new System.Windows.Forms.CheckBox();
            this.txtCSVSeparatorChar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgIndexLinks = new System.Windows.Forms.DataGridView();
            this.btnIndexRemove = new System.Windows.Forms.Button();
            this.btnIndexDown = new System.Windows.Forms.Button();
            this.btnIndexUp = new System.Windows.Forms.Button();
            this.btnIndexAdd = new System.Windows.Forms.Button();
            this.tabPaths = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rbDocIDSubFolder = new System.Windows.Forms.RadioButton();
            this.cboSubFolderIndexField = new System.Windows.Forms.ComboBox();
            this.rbUseSubFolderIndexField = new System.Windows.Forms.RadioButton();
            this.rbNoSubFolder = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cboPDFFNINdexField = new System.Windows.Forms.ComboBox();
            this.rbPDFFNIndexField = new System.Windows.Forms.RadioButton();
            this.rbPDFFNUniqueDocID = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cboCSVFNIndexField = new System.Windows.Forms.ComboBox();
            this.rbXMLFNIndexField = new System.Windows.Forms.RadioButton();
            this.rbXMLFNUniqueBatchID = new System.Windows.Forms.RadioButton();
            this.rbXMLFNUniqueDocID = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOpenTIFFIleDialog = new System.Windows.Forms.Button();
            this.btnOpenPDFFileDialog = new System.Windows.Forms.Button();
            this.btnOpenXMLFileDialog = new System.Windows.Forms.Button();
            this.txtPDFFilePath = new System.Windows.Forms.TextBox();
            this.txtTIFFilePath = new System.Windows.Forms.TextBox();
            this.txtCSVFilePath = new System.Windows.Forms.TextBox();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.btnSaveExit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabIndex.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgIndexLinks)).BeginInit();
            this.tabPaths.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl);
            this.groupBox1.Location = new System.Drawing.Point(18, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1128, 677);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fragebogen-Release - Einrichtung";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabIndex);
            this.tabControl.Controls.Add(this.tabPaths);
            this.tabControl.Location = new System.Drawing.Point(9, 29);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1110, 638);
            this.tabControl.TabIndex = 0;
            // 
            // tabIndex
            // 
            this.tabIndex.Controls.Add(this.groupBox4);
            this.tabIndex.Controls.Add(this.groupBox3);
            this.tabIndex.Controls.Add(this.groupBox2);
            this.tabIndex.Location = new System.Drawing.Point(4, 29);
            this.tabIndex.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabIndex.Name = "tabIndex";
            this.tabIndex.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabIndex.Size = new System.Drawing.Size(1102, 605);
            this.tabIndex.TabIndex = 0;
            this.tabIndex.Text = "Indexfelder";
            this.tabIndex.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTextFieldExpression);
            this.groupBox4.Controls.Add(this.txtSplitFieldSeparator);
            this.groupBox4.Controls.Add(this.txtSplitFieldExpression);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(552, 192);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(522, 188);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Indexfeldverarbeitung-Einstellungen";
            // 
            // txtTextFieldExpression
            // 
            this.txtTextFieldExpression.Location = new System.Drawing.Point(254, 137);
            this.txtTextFieldExpression.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTextFieldExpression.Name = "txtTextFieldExpression";
            this.txtTextFieldExpression.Size = new System.Drawing.Size(235, 26);
            this.txtTextFieldExpression.TabIndex = 8;
            this.txtTextFieldExpression.TextChanged += new System.EventHandler(this.txtTextFieldExpression_TextChanged);
            // 
            // txtSplitFieldSeparator
            // 
            this.txtSplitFieldSeparator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSplitFieldSeparator.Location = new System.Drawing.Point(254, 89);
            this.txtSplitFieldSeparator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSplitFieldSeparator.MaxLength = 1;
            this.txtSplitFieldSeparator.Name = "txtSplitFieldSeparator";
            this.txtSplitFieldSeparator.Size = new System.Drawing.Size(24, 26);
            this.txtSplitFieldSeparator.TabIndex = 9;
            // 
            // txtSplitFieldExpression
            // 
            this.txtSplitFieldExpression.BackColor = System.Drawing.Color.White;
            this.txtSplitFieldExpression.Location = new System.Drawing.Point(254, 42);
            this.txtSplitFieldExpression.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSplitFieldExpression.Name = "txtSplitFieldExpression";
            this.txtSplitFieldExpression.Size = new System.Drawing.Size(235, 26);
            this.txtSplitFieldExpression.TabIndex = 9;
            this.txtSplitFieldExpression.TextChanged += new System.EventHandler(this.txtSplitFieldExpression_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 142);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(188, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Expression für Textfelder:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Trennzeichen für Aufspaltfelder:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 46);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(217, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Expression für Aufspaltfelder:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkColumNamesInFirstRow);
            this.groupBox3.Controls.Add(this.chkSetValuesInHighCommas);
            this.groupBox3.Controls.Add(this.txtCSVSeparatorChar);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(552, 9);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(522, 174);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CSV-Einstellungen";
            // 
            // chkColumNamesInFirstRow
            // 
            this.chkColumNamesInFirstRow.AutoSize = true;
            this.chkColumNamesInFirstRow.Location = new System.Drawing.Point(14, 89);
            this.chkColumNamesInFirstRow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkColumNamesInFirstRow.Name = "chkColumNamesInFirstRow";
            this.chkColumNamesInFirstRow.Size = new System.Drawing.Size(210, 24);
            this.chkColumNamesInFirstRow.TabIndex = 10;
            this.chkColumNamesInFirstRow.Text = "Spaltennamen in 1. Zeile";
            this.chkColumNamesInFirstRow.UseVisualStyleBackColor = true;
            // 
            // chkSetValuesInHighCommas
            // 
            this.chkSetValuesInHighCommas.AutoSize = true;
            this.chkSetValuesInHighCommas.Location = new System.Drawing.Point(14, 129);
            this.chkSetValuesInHighCommas.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkSetValuesInHighCommas.Name = "chkSetValuesInHighCommas";
            this.chkSetValuesInHighCommas.Size = new System.Drawing.Size(354, 24);
            this.chkSetValuesInHighCommas.TabIndex = 10;
            this.chkSetValuesInHighCommas.Text = "Setze Feldwerte unter Hochkommas (\"Wert\")";
            this.chkSetValuesInHighCommas.UseVisualStyleBackColor = true;
            // 
            // txtCSVSeparatorChar
            // 
            this.txtCSVSeparatorChar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCSVSeparatorChar.Location = new System.Drawing.Point(166, 42);
            this.txtCSVSeparatorChar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCSVSeparatorChar.MaxLength = 1;
            this.txtCSVSeparatorChar.Name = "txtCSVSeparatorChar";
            this.txtCSVSeparatorChar.Size = new System.Drawing.Size(24, 26);
            this.txtCSVSeparatorChar.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "CSV-Trennzeichen:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgIndexLinks);
            this.groupBox2.Controls.Add(this.btnIndexRemove);
            this.groupBox2.Controls.Add(this.btnIndexDown);
            this.groupBox2.Controls.Add(this.btnIndexUp);
            this.groupBox2.Controls.Add(this.btnIndexAdd);
            this.groupBox2.Location = new System.Drawing.Point(9, 9);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(534, 549);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Indexzuweisung";
            // 
            // dgIndexLinks
            // 
            this.dgIndexLinks.AllowUserToAddRows = false;
            this.dgIndexLinks.AllowUserToDeleteRows = false;
            this.dgIndexLinks.AllowUserToResizeColumns = false;
            this.dgIndexLinks.AllowUserToResizeRows = false;
            this.dgIndexLinks.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgIndexLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgIndexLinks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgIndexLinks.Location = new System.Drawing.Point(9, 29);
            this.dgIndexLinks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgIndexLinks.MultiSelect = false;
            this.dgIndexLinks.Name = "dgIndexLinks";
            this.dgIndexLinks.ReadOnly = true;
            this.dgIndexLinks.RowHeadersWidth = 22;
            this.dgIndexLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgIndexLinks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgIndexLinks.ShowCellErrors = false;
            this.dgIndexLinks.ShowCellToolTips = false;
            this.dgIndexLinks.ShowRowErrors = false;
            this.dgIndexLinks.Size = new System.Drawing.Size(392, 511);
            this.dgIndexLinks.TabIndex = 2;
            // 
            // btnIndexRemove
            // 
            this.btnIndexRemove.Location = new System.Drawing.Point(410, 306);
            this.btnIndexRemove.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnIndexRemove.Name = "btnIndexRemove";
            this.btnIndexRemove.Size = new System.Drawing.Size(112, 35);
            this.btnIndexRemove.TabIndex = 1;
            this.btnIndexRemove.Text = "Entfernen";
            this.btnIndexRemove.UseVisualStyleBackColor = true;
            this.btnIndexRemove.Click += new System.EventHandler(this.btnIndexRemove_Click);
            // 
            // btnIndexDown
            // 
            this.btnIndexDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIndexDown.BackgroundImage")));
            this.btnIndexDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIndexDown.Location = new System.Drawing.Point(438, 245);
            this.btnIndexDown.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnIndexDown.Name = "btnIndexDown";
            this.btnIndexDown.Size = new System.Drawing.Size(54, 52);
            this.btnIndexDown.TabIndex = 1;
            this.btnIndexDown.UseVisualStyleBackColor = true;
            this.btnIndexDown.Click += new System.EventHandler(this.btnIndexDown_Click);
            // 
            // btnIndexUp
            // 
            this.btnIndexUp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIndexUp.BackgroundImage")));
            this.btnIndexUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIndexUp.Location = new System.Drawing.Point(438, 183);
            this.btnIndexUp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnIndexUp.Name = "btnIndexUp";
            this.btnIndexUp.Size = new System.Drawing.Size(54, 52);
            this.btnIndexUp.TabIndex = 1;
            this.btnIndexUp.UseVisualStyleBackColor = true;
            this.btnIndexUp.Click += new System.EventHandler(this.btnIndexUp_Click);
            // 
            // btnIndexAdd
            // 
            this.btnIndexAdd.Location = new System.Drawing.Point(410, 138);
            this.btnIndexAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnIndexAdd.Name = "btnIndexAdd";
            this.btnIndexAdd.Size = new System.Drawing.Size(112, 35);
            this.btnIndexAdd.TabIndex = 1;
            this.btnIndexAdd.Text = "Hinzufügen";
            this.btnIndexAdd.UseVisualStyleBackColor = true;
            this.btnIndexAdd.Click += new System.EventHandler(this.btnIndexAdd_Click);
            // 
            // tabPaths
            // 
            this.tabPaths.Controls.Add(this.groupBox9);
            this.tabPaths.Controls.Add(this.groupBox7);
            this.tabPaths.Controls.Add(this.groupBox6);
            this.tabPaths.Controls.Add(this.groupBox5);
            this.tabPaths.Location = new System.Drawing.Point(4, 29);
            this.tabPaths.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPaths.Name = "tabPaths";
            this.tabPaths.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPaths.Size = new System.Drawing.Size(1102, 605);
            this.tabPaths.TabIndex = 1;
            this.tabPaths.Text = "Pfade";
            this.tabPaths.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rbDocIDSubFolder);
            this.groupBox9.Controls.Add(this.cboSubFolderIndexField);
            this.groupBox9.Controls.Add(this.rbUseSubFolderIndexField);
            this.groupBox9.Controls.Add(this.rbNoSubFolder);
            this.groupBox9.Location = new System.Drawing.Point(9, 226);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Size = new System.Drawing.Size(1080, 83);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Unterverzeichnis (für alle Ausgabefade)";
            this.groupBox9.Visible = false;
            // 
            // rbDocIDSubFolder
            // 
            this.rbDocIDSubFolder.AutoSize = true;
            this.rbDocIDSubFolder.Location = new System.Drawing.Point(267, 29);
            this.rbDocIDSubFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbDocIDSubFolder.Name = "rbDocIDSubFolder";
            this.rbDocIDSubFolder.Size = new System.Drawing.Size(257, 24);
            this.rbDocIDSubFolder.TabIndex = 3;
            this.rbDocIDSubFolder.Text = "Verwende UniqueDocument-ID";
            this.rbDocIDSubFolder.UseVisualStyleBackColor = true;
            // 
            // cboSubFolderIndexField
            // 
            this.cboSubFolderIndexField.FormattingEnabled = true;
            this.cboSubFolderIndexField.Location = new System.Drawing.Point(748, 28);
            this.cboSubFolderIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboSubFolderIndexField.Name = "cboSubFolderIndexField";
            this.cboSubFolderIndexField.Size = new System.Drawing.Size(250, 28);
            this.cboSubFolderIndexField.TabIndex = 2;
            // 
            // rbUseSubFolderIndexField
            // 
            this.rbUseSubFolderIndexField.AutoSize = true;
            this.rbUseSubFolderIndexField.Location = new System.Drawing.Point(561, 29);
            this.rbUseSubFolderIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbUseSubFolderIndexField.Name = "rbUseSubFolderIndexField";
            this.rbUseSubFolderIndexField.Size = new System.Drawing.Size(175, 24);
            this.rbUseSubFolderIndexField.TabIndex = 0;
            this.rbUseSubFolderIndexField.Text = "Verwende Indexfeld";
            this.rbUseSubFolderIndexField.UseVisualStyleBackColor = true;
            // 
            // rbNoSubFolder
            // 
            this.rbNoSubFolder.AutoSize = true;
            this.rbNoSubFolder.Checked = true;
            this.rbNoSubFolder.Location = new System.Drawing.Point(9, 29);
            this.rbNoSubFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbNoSubFolder.Name = "rbNoSubFolder";
            this.rbNoSubFolder.Size = new System.Drawing.Size(229, 24);
            this.rbNoSubFolder.TabIndex = 0;
            this.rbNoSubFolder.TabStop = true;
            this.rbNoSubFolder.Text = "Kein Extra-Unterverzeichnis";
            this.rbNoSubFolder.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cboPDFFNINdexField);
            this.groupBox7.Controls.Add(this.rbPDFFNIndexField);
            this.groupBox7.Controls.Add(this.rbPDFFNUniqueDocID);
            this.groupBox7.Location = new System.Drawing.Point(561, 318);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox7.Size = new System.Drawing.Size(528, 154);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Dateinamen PDF- und TIF-Datei";
            this.groupBox7.Visible = false;
            // 
            // cboPDFFNINdexField
            // 
            this.cboPDFFNINdexField.FormattingEnabled = true;
            this.cboPDFFNINdexField.Location = new System.Drawing.Point(196, 98);
            this.cboPDFFNINdexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboPDFFNINdexField.Name = "cboPDFFNINdexField";
            this.cboPDFFNINdexField.Size = new System.Drawing.Size(250, 28);
            this.cboPDFFNINdexField.TabIndex = 1;
            // 
            // rbPDFFNIndexField
            // 
            this.rbPDFFNIndexField.AutoSize = true;
            this.rbPDFFNIndexField.Location = new System.Drawing.Point(9, 100);
            this.rbPDFFNIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbPDFFNIndexField.Name = "rbPDFFNIndexField";
            this.rbPDFFNIndexField.Size = new System.Drawing.Size(175, 24);
            this.rbPDFFNIndexField.TabIndex = 0;
            this.rbPDFFNIndexField.Text = "Verwende Indexfeld";
            this.rbPDFFNIndexField.UseVisualStyleBackColor = true;
            // 
            // rbPDFFNUniqueDocID
            // 
            this.rbPDFFNUniqueDocID.AutoSize = true;
            this.rbPDFFNUniqueDocID.Checked = true;
            this.rbPDFFNUniqueDocID.Location = new System.Drawing.Point(9, 29);
            this.rbPDFFNUniqueDocID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbPDFFNUniqueDocID.Name = "rbPDFFNUniqueDocID";
            this.rbPDFFNUniqueDocID.Size = new System.Drawing.Size(437, 24);
            this.rbPDFFNUniqueDocID.TabIndex = 0;
            this.rbPDFFNUniqueDocID.TabStop = true;
            this.rbPDFFNUniqueDocID.Text = "Verwende UniqueDocument-ID (eine Datei je Dokument)\r\n";
            this.rbPDFFNUniqueDocID.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cboCSVFNIndexField);
            this.groupBox6.Controls.Add(this.rbXMLFNIndexField);
            this.groupBox6.Controls.Add(this.rbXMLFNUniqueBatchID);
            this.groupBox6.Controls.Add(this.rbXMLFNUniqueDocID);
            this.groupBox6.Location = new System.Drawing.Point(9, 318);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Size = new System.Drawing.Size(543, 154);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Dateinamen CSV-Datei";
            this.groupBox6.Visible = false;
            // 
            // cboCSVFNIndexField
            // 
            this.cboCSVFNIndexField.FormattingEnabled = true;
            this.cboCSVFNIndexField.Location = new System.Drawing.Point(196, 98);
            this.cboCSVFNIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboCSVFNIndexField.Name = "cboCSVFNIndexField";
            this.cboCSVFNIndexField.Size = new System.Drawing.Size(250, 28);
            this.cboCSVFNIndexField.TabIndex = 1;
            // 
            // rbXMLFNIndexField
            // 
            this.rbXMLFNIndexField.AutoSize = true;
            this.rbXMLFNIndexField.Location = new System.Drawing.Point(9, 100);
            this.rbXMLFNIndexField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbXMLFNIndexField.Name = "rbXMLFNIndexField";
            this.rbXMLFNIndexField.Size = new System.Drawing.Size(175, 24);
            this.rbXMLFNIndexField.TabIndex = 0;
            this.rbXMLFNIndexField.Text = "Verwende Indexfeld";
            this.rbXMLFNIndexField.UseVisualStyleBackColor = true;
            // 
            // rbXMLFNUniqueBatchID
            // 
            this.rbXMLFNUniqueBatchID.AutoSize = true;
            this.rbXMLFNUniqueBatchID.Location = new System.Drawing.Point(9, 65);
            this.rbXMLFNUniqueBatchID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbXMLFNUniqueBatchID.Name = "rbXMLFNUniqueBatchID";
            this.rbXMLFNUniqueBatchID.Size = new System.Drawing.Size(196, 24);
            this.rbXMLFNUniqueBatchID.TabIndex = 0;
            this.rbXMLFNUniqueBatchID.Text = "Verwende Stapelname";
            this.rbXMLFNUniqueBatchID.UseVisualStyleBackColor = true;
            this.rbXMLFNUniqueBatchID.Visible = false;
            // 
            // rbXMLFNUniqueDocID
            // 
            this.rbXMLFNUniqueDocID.AutoSize = true;
            this.rbXMLFNUniqueDocID.Checked = true;
            this.rbXMLFNUniqueDocID.Location = new System.Drawing.Point(9, 29);
            this.rbXMLFNUniqueDocID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbXMLFNUniqueDocID.Name = "rbXMLFNUniqueDocID";
            this.rbXMLFNUniqueDocID.Size = new System.Drawing.Size(437, 24);
            this.rbXMLFNUniqueDocID.TabIndex = 0;
            this.rbXMLFNUniqueDocID.TabStop = true;
            this.rbXMLFNUniqueDocID.Text = "Verwende UniqueDocument-ID (eine Datei je Dokument)\r\n";
            this.rbXMLFNUniqueDocID.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.btnOpenTIFFIleDialog);
            this.groupBox5.Controls.Add(this.btnOpenPDFFileDialog);
            this.groupBox5.Controls.Add(this.btnOpenXMLFileDialog);
            this.groupBox5.Controls.Add(this.txtPDFFilePath);
            this.groupBox5.Controls.Add(this.txtTIFFilePath);
            this.groupBox5.Controls.Add(this.txtCSVFilePath);
            this.groupBox5.Location = new System.Drawing.Point(9, 9);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(1080, 208);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ausgabepfade";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 152);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "TIF-Datei:";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 103);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "PDF-Datei:";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "CSV-Datei:";
            // 
            // btnOpenTIFFIleDialog
            // 
            this.btnOpenTIFFIleDialog.Location = new System.Drawing.Point(1046, 145);
            this.btnOpenTIFFIleDialog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOpenTIFFIleDialog.Name = "btnOpenTIFFIleDialog";
            this.btnOpenTIFFIleDialog.Size = new System.Drawing.Size(26, 35);
            this.btnOpenTIFFIleDialog.TabIndex = 2;
            this.btnOpenTIFFIleDialog.Text = ">";
            this.btnOpenTIFFIleDialog.UseVisualStyleBackColor = true;
            this.btnOpenTIFFIleDialog.Visible = false;
            this.btnOpenTIFFIleDialog.Click += new System.EventHandler(this.btnOpenTIFFIleDialog_Click);
            // 
            // btnOpenPDFFileDialog
            // 
            this.btnOpenPDFFileDialog.Location = new System.Drawing.Point(1046, 95);
            this.btnOpenPDFFileDialog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOpenPDFFileDialog.Name = "btnOpenPDFFileDialog";
            this.btnOpenPDFFileDialog.Size = new System.Drawing.Size(26, 35);
            this.btnOpenPDFFileDialog.TabIndex = 2;
            this.btnOpenPDFFileDialog.Text = ">";
            this.btnOpenPDFFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenPDFFileDialog.Visible = false;
            this.btnOpenPDFFileDialog.Click += new System.EventHandler(this.btnOpenPDFFileDialog_Click);
            // 
            // btnOpenXMLFileDialog
            // 
            this.btnOpenXMLFileDialog.Location = new System.Drawing.Point(1046, 46);
            this.btnOpenXMLFileDialog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOpenXMLFileDialog.Name = "btnOpenXMLFileDialog";
            this.btnOpenXMLFileDialog.Size = new System.Drawing.Size(26, 35);
            this.btnOpenXMLFileDialog.TabIndex = 2;
            this.btnOpenXMLFileDialog.Text = ">";
            this.btnOpenXMLFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenXMLFileDialog.Click += new System.EventHandler(this.btnOpenXMLFileDialog_Click);
            // 
            // txtPDFFilePath
            // 
            this.txtPDFFilePath.Location = new System.Drawing.Point(108, 98);
            this.txtPDFFilePath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPDFFilePath.Name = "txtPDFFilePath";
            this.txtPDFFilePath.Size = new System.Drawing.Size(931, 26);
            this.txtPDFFilePath.TabIndex = 0;
            this.txtPDFFilePath.Visible = false;
            // 
            // txtTIFFilePath
            // 
            this.txtTIFFilePath.Location = new System.Drawing.Point(108, 148);
            this.txtTIFFilePath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTIFFilePath.Name = "txtTIFFilePath";
            this.txtTIFFilePath.Size = new System.Drawing.Size(931, 26);
            this.txtTIFFilePath.TabIndex = 0;
            this.txtTIFFilePath.Visible = false;
            // 
            // txtCSVFilePath
            // 
            this.txtCSVFilePath.Location = new System.Drawing.Point(108, 49);
            this.txtCSVFilePath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCSVFilePath.Name = "txtCSVFilePath";
            this.txtCSVFilePath.Size = new System.Drawing.Size(931, 26);
            this.txtCSVFilePath.TabIndex = 0;
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveExit.Location = new System.Drawing.Point(790, 705);
            this.btnSaveExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Size = new System.Drawing.Size(148, 68);
            this.btnSaveExit.TabIndex = 1;
            this.btnSaveExit.Text = "&Speichern && beenden";
            this.btnSaveExit.UseVisualStyleBackColor = true;
            this.btnSaveExit.Click += new System.EventHandler(this.btnSaveExit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(982, 705);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(148, 68);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "A&bbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 801);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSaveExit);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Release-Setup";
            this.groupBox1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabIndex.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgIndexLinks)).EndInit();
            this.tabPaths.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabIndex;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tabPaths;
        private System.Windows.Forms.Button btnIndexRemove;
        private System.Windows.Forms.Button btnIndexDown;
        private System.Windows.Forms.Button btnIndexUp;
        private System.Windows.Forms.Button btnIndexAdd;
        private System.Windows.Forms.DataGridView dgIndexLinks;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cboPDFFNINdexField;
        private System.Windows.Forms.RadioButton rbPDFFNIndexField;
        private System.Windows.Forms.RadioButton rbPDFFNUniqueDocID;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cboCSVFNIndexField;
        private System.Windows.Forms.RadioButton rbXMLFNIndexField;
        private System.Windows.Forms.RadioButton rbXMLFNUniqueBatchID;
        private System.Windows.Forms.RadioButton rbXMLFNUniqueDocID;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.Button btnSaveExit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOpenTIFFIleDialog;
        private System.Windows.Forms.Button btnOpenPDFFileDialog;
        private System.Windows.Forms.Button btnOpenXMLFileDialog;
        private System.Windows.Forms.TextBox txtPDFFilePath;
        private System.Windows.Forms.TextBox txtTIFFilePath;
        private System.Windows.Forms.TextBox txtCSVFilePath;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox cboSubFolderIndexField;
        private System.Windows.Forms.RadioButton rbUseSubFolderIndexField;
        private System.Windows.Forms.RadioButton rbNoSubFolder;
        private System.Windows.Forms.RadioButton rbDocIDSubFolder;
        private System.Windows.Forms.TextBox txtCSVSeparatorChar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtTextFieldExpression;
        private System.Windows.Forms.TextBox txtSplitFieldSeparator;
        private System.Windows.Forms.TextBox txtSplitFieldExpression;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkColumNamesInFirstRow;
        private System.Windows.Forms.CheckBox chkSetValuesInHighCommas;
    }
}