﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Kofax.ReleaseLib;


namespace SmartCAP.KEC.Fragebogen_Release
{
    public partial class AddIndexLink : Form
    {
        ReleaseSetupData _setupData;
        private KeyValuePair<string, KfxLinkSourceType> indexLink;


        public KeyValuePair<string, KfxLinkSourceType> OpenLinkForm(ReleaseSetupData setupData)
        {
            _setupData = setupData;

            // Initialize
            InitializeComponent();
            FillAllIndexFields();
            FillAllBatchFields();

            rbIndexField.Checked = true;
                        
            // Display Window
            this.ShowDialog();

            // Return Index link
            return indexLink;
        }


        private void FillAllIndexFields()
        {
            IndexFields indexFields = _setupData.IndexFields;

            // Add Index Fields 
            foreach (IndexField field in indexFields)
            { cboIndexField.Items.Add(field.Name); }

            if (cboIndexField.Items.Count > 0) { cboIndexField.Text = cboIndexField.Items[0].ToString(); }
        }


        private void FillAllBatchFields()
        {
            BatchFields batchFields = _setupData.BatchFields;
            
            foreach (BatchField field in batchFields)
            { cboBatchField.Items.Add(field.Name);  }

            if (cboBatchField.Items.Count > 0) { cboBatchField.Text = cboBatchField.Items[0].ToString(); }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            // Clear Index Links
            //indexLink.Clear();

            // Save index link
            if (rbIndexField.Checked)
            {  //indexLink.Add(cboIndexField.Text, KfxLinkSourceType.KFX_REL_INDEXFIELD); }
                indexLink = new KeyValuePair<string, KfxLinkSourceType>(cboIndexField.Text, KfxLinkSourceType.KFX_REL_INDEXFIELD);
            }

            else if (rbBatchField.Checked)
            { //indexLink.Add(cboBatchField.Text, KfxLinkSourceType.KFX_REL_BATCHFIELD); 
                indexLink = new KeyValuePair<string, KfxLinkSourceType>(cboBatchField.Text, KfxLinkSourceType.KFX_REL_BATCHFIELD);
            }

            else if (rbFixedValue.Checked)
            { //indexLink.Add(txtFixedValue.Text, KfxLinkSourceType.KFX_REL_TEXTCONSTANT); 
                indexLink = new KeyValuePair<string, KfxLinkSourceType>(txtFixedValue.Text, KfxLinkSourceType.KFX_REL_TEXTCONSTANT);
            }

            // Close dialog window
            this.Close();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Create empty KVPair
            indexLink = new KeyValuePair<string, KfxLinkSourceType>("", KfxLinkSourceType.KFX_REL_UNDEFINED_LINK);

            // Close dialog window
            this.Close();
        }

    }
}
