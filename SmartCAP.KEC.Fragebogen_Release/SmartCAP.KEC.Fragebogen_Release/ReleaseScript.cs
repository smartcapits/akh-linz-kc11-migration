﻿using Kofax.ReleaseLib;
using System;
using System.Runtime.InteropServices;
using log4net;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using SmartCAP.Logging;
using SmartCAP.KEC.Model;
using SmartCAP.KEC.Helpers;
using log4net.Repository.Hierarchy;

namespace SmartCAP.KEC.Fragebogen_Release
{
    /// <summary>
    /// Implements the KfxReleaseScript COM interface
    /// </summary>
    [ProgId("SmartCAP.KEC.Fragebogen_Release")]
    public class ReleaseScript
    {
        // Standard constructor. We use it to resolve KC's assemblies from the calling application (i.e. Admin.exe or Release.exe)
        // This prevents clutter in the KEC's folder (no redundant Capture-libraries that already existin in ServLib\Bin)
        public ReleaseScript()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }


        // Init Log4Net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
                //Schreib ins Log immer den aktuell Verarbeitetenden Stapel
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }


        /// <summary>
        /// Holds data relevant to release in general. You should always favour Document (.NET) objects over ReleaseData (COM)
        /// Use it if there's no other way.
        /// </summary>
        public ReleaseData DocumentData;
        private string _csvFileName = "";


        // Add code here for any initialization before the export happens
        public KfxReturnValue OpenScript()
        {
            InitializeLogging();

            //CSV-FileName für Stapel setzen (TimeStamp)
            _csvFileName = DateTime.Now.ToString("yyyyMMddHHmmsss");

            return KfxReturnValue.KFX_REL_SUCCESS;
        }


        // This method is called for each document. Most of your code will likely go here.
        public KfxReturnValue ReleaseDoc()
        {
            List<string> releasedTIFFiles = new List<string>();
            List<string> releasedPDFFiles = new List<string>();
            string subFolder;

            List<string> csvOutputResult = new List<string>();
            string csvSeparatorCharacter;
            bool columnNamesInFirstRow = false;
            bool csvValuesInHighCommas = false;
            string splitFieldExpression;
            string splitFieldSeparator;
            string textFieldexpression;

            try
            {
                // always prefer this wrapper class over the ReleaseData object, if possible!
                Document document = new Document(DocumentData);

                // Get Settings
                Log.Debug("Getting Fragebogen_Release Settings from CSS");
                subFolder = document.CustomProperties["FragebogenReleasse.SubFolderName"];
                if (subFolder.Equals("UniqueDocumentID")) { subFolder = document.UniqueIdHex; }
                else if (!subFolder.Equals("")) { subFolder = DocumentData.Values[subFolder].Value; }
                // Get CSV- and IndexFile-settings
                splitFieldExpression = document.CustomProperties["FragebogenReleasse.SplitFieldExpression"];
                splitFieldSeparator = document.CustomProperties["FragebogenReleasse.SplitFieldSeparator"];
                textFieldexpression = document.CustomProperties["FragebogenReleasse.TextFieldExpression"];
                csvSeparatorCharacter = document.CustomProperties["FragebogenReleasse.CSVSeparatorChar"];
                if (document.CustomProperties["FragebogenReleasse.CSVColumnNamesInFirstRow"] == true.ToString()) { columnNamesInFirstRow = true; }
                if (document.CustomProperties["FragebogenReleasse.CSVValuesInHighCommas"] == true.ToString()) { csvValuesInHighCommas = true; }

                // Copy image files (TIF)
                if (!DocumentData.ImageFilePath.Equals(""))
                { Log.Debug("Copying TIF files"); releasedTIFFiles = KofaxReleaseTools.CopyImageFiles(DocumentData, DocumentData.ImageFilePath, subFolder); }

                // Copy PDF files
                if (!DocumentData.KofaxPDFPath.Equals(""))
                { Log.Debug("Copying PDF files"); releasedPDFFiles = KofaxReleaseTools.CopyPDFFiles(DocumentData, DocumentData.KofaxPDFPath, subFolder); }

                // Create output CSV & save it
                if (!DocumentData.CustomProperties["FragebogenReleasse.CSVFilePath"].Equals(""))
                {

                    Log.Debug("Creating CSV output file content");
                    // CSV-Datei zusammenbauen mit CSVCreator
                    csvOutputResult = CSVCreator.CreateCSVForDocument(DocumentData.Values, csvSeparatorCharacter, splitFieldExpression, splitFieldSeparator, textFieldexpression, csvValuesInHighCommas);

                    // Pfad und Dateinamen der CSV-Datei zusammenbauen (FileName kommt aus OpenScript-Methode)
                    string releasePath = Path.Combine(document.CustomProperties["FragebogenReleasse.CSVFilePath"], subFolder);

                    // Wenn Output-File noch nicht existert -> Schauen ob Columnnames in 1. Zeile gehören, ansonsten nur neues File erzeugen mit Values
                    if (File.Exists(Path.Combine(releasePath, _csvFileName) + ".csv"))
                    {
                        Log.Debug("Appending to CSV output file " + _csvFileName);
                        // File existiert -> nur Values ausgeben
                        File.AppendAllText(Path.Combine(releasePath, _csvFileName) + ".csv", csvOutputResult[1] + "\r\n");
                    }
                    else
                    {
                        Log.Debug("Creating CSV output file " + _csvFileName);
                        // Wenn Option gewählt Spaltennamen ausgeben
                        if (columnNamesInFirstRow) { File.AppendAllText(Path.Combine(releasePath, _csvFileName) + ".csv", csvOutputResult[0] + "\r\n"); }

                        // Werte ausgeben
                        File.AppendAllText(Path.Combine(releasePath, _csvFileName) + ".csv", csvOutputResult[1] + "\r\n");
                        }
                    }
                
                return KfxReturnValue.KFX_REL_SUCCESS;
            }
            catch (Exception ex)
            {
                Log.Error("Error in ReleaseDoc: " + ex.ToString());
                DocumentData.SendMessage(ex.Message, 0, KfxInfoReturnValue.KFX_REL_DOC_ERROR);
                return KfxReturnValue.KFX_REL_ERROR;
            }
        }


        // Kofax calls this method after all documents were released. Use it to clean up if you did anything in OpenScript.
        public KfxReturnValue CloseScript()
        {
            return KfxReturnValue.KFX_REL_SUCCESS;
        }
    }
}
