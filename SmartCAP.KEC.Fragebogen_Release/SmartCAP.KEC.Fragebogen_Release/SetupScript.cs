﻿using Kofax.ReleaseLib;
using log4net;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using SmartCAP.KEC.Helpers;
using SmartCAP.Logging;

namespace SmartCAP.KEC.Fragebogen_Release
{
    /// <summary>
    /// Implements the KfxReleaseSetupScript COM interface
    /// </summary>
    [ProgId("SmartCAP.KEC.Fragebogen_Release.Setup")]
    public class SetupScript
    {
        public ReleaseSetupData SetupData;


        // Standard constructor. We use it to resolve KC's assemblies from the calling application (i.e. Admin.exe or Release.exe)
        // This prevents clutter in the KEC's folder (no redundant Capture-libraries that already existin in ServLib\Bin)
        public SetupScript()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
        }


        // init Log4Net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }


        // Add code here for any initialization before the GUI loads
        public KfxReturnValue OpenScript()
        {
            InitializeLogging();
            return KfxReturnValue.KFX_REL_SUCCESS;
        }


        // Kofax calls this method right after OpenScript. Usually you shouldn' have to add anything here.
        public KfxReturnValue RunUI()
        {
            try
            {
                SetupForm form = new SetupForm();
                form.RunSetup(SetupData);
                return KfxReturnValue.KFX_REL_SUCCESS;
            }
            catch (Exception ex)
            {
                Log.Error("Error in RunUI: " + ex.ToString());
                return KfxReturnValue.KFX_REL_ERROR;
            }
        }


        // Kofax calls this method is after any change to either the batch or document class (e.g. adding a new index field)
        // You may want to call RunUI to interact with the changes, but don't flood the user (DON'T pop up the GUI for every new field!)
        public KfxReturnValue ActionEvent(KfxActionValue actionID, string data1, string data2)
        {
            try
            {
                if (new KfxActionValue[] {
                KfxActionValue.KFX_REL_BATCHFIELD_INSERT,
                KfxActionValue.KFX_REL_BATCHFIELD_RENAME,
                KfxActionValue.KFX_REL_BATCHFIELD_DELETE,
                KfxActionValue.KFX_REL_INDEXFIELD_INSERT,
                KfxActionValue.KFX_REL_INDEXFIELD_RENAME,
                KfxActionValue.KFX_REL_INDEXFIELD_DELETE }.Contains(actionID))
                {
                    SetupForm_template.AddAllFields(SetupData);
                    SetupData.Apply();
                }
                return KfxReturnValue.KFX_REL_SUCCESS;
            }
            catch (Exception ex)
            {
                Log.Error("Error in ActionEvent: " + ex.ToString());
                return KfxReturnValue.KFX_REL_ERROR;
            }
        }


        // Kofax calls this method after the RunUI. Use it to clean up if you did anything in OpenScript.
        public KfxReturnValue CloseScript()
        {
            return KfxReturnValue.KFX_REL_SUCCESS;
        }
    }
}
