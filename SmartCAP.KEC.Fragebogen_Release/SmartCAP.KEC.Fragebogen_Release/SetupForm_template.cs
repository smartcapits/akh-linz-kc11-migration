﻿using Kofax.ReleaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using SmartCAP.KEC.Exceptions;
using SmartCAP.KEC.Helpers;
using SmartCAP.KEC.Model;

namespace SmartCAP.KEC.Fragebogen_Release
{
    public partial class SetupForm_template : Form
    {
        #region Initialization

        public ReleaseSetupData SetupData;
        public SetupForm_template()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(ReleaseSetupData setupData)
        {
            SetupData = setupData;

            if (setupData.New == 0)
            {
                // existing instance. load settings
                LoadSettings();
            }
            else
            {
                // new instance. initialize form
                InitializeSettings();
            }

            return this.ShowDialog();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                AddAllFields(SetupData);
                SetupData.Apply();

                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Customization

        private void InitializeSettings()
        {
            txtSampleField.Text = "initial value";
        }

        private void LoadSettings()
        {
            // load checkbox-, combobox-, radiobutton- and textbox-CustomProperties into their form controls
            GetAll(this, typeof(CheckBox)).ToList().ForEach(x => ((CheckBox)x).Checked = GetCustomProperty(x.Name) == "True");
            GetAll(this, typeof(ComboBox)).ToList().ForEach(x => x.Text = GetCustomProperty(x.Name));
            GetAll(this, typeof(RadioButton)).ToList().ForEach(x => ((RadioButton)x).Checked = GetCustomProperty(x.Name) == "True");
            GetAll(this, typeof(TextBox)).ToList().ForEach(x => x.Text = GetCustomProperty(x.Name));
        }

        private bool VerifySettings()
        {
            if (txtSampleField.Text == "")
            {
                MessageBox.Show("Bitte Feld ausfüllen!");
                return false;
            }

            return true;
        }

        private void SaveSettings()
        {
            // save the values of checkboxes, comboboxes, radiobuttons and textboxes to CustomProperties named after the controls name
            GetAll(this, typeof(CheckBox)).ToList().ForEach(x => SetCustomProperty(x.Name, ((CheckBox)x).Checked.ToString()));
            GetAll(this, typeof(ComboBox)).ToList().ForEach(x => SetCustomProperty(x.Name, x.Text));
            GetAll(this, typeof(RadioButton)).ToList().ForEach(x => SetCustomProperty(x.Name, ((RadioButton)x).Checked.ToString()));
            GetAll(this, typeof(TextBox)).ToList().ForEach(x => SetCustomProperty(x.Name, x.Text));
        }

        #endregion

        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomProperty(string name, string value)
        {
            try
            {
                SetupData.CustomProperties[name].Value = value;
            }
            catch
            {
                SetupData.CustomProperties.Add(name, value);
            }
        }

        private string GetCustomProperty(string name)
        {
            try
            {
                return SetupData.CustomProperties[name].Value;
            }
            catch
            {
                return "";
            }
        }

        public static void AddAllFields(ReleaseSetupData setupData)
        {
            setupData.Links.RemoveAll();
            for (int i = 1; i <= setupData.BatchFields.Count; i++)
            {
                setupData.Links.Add(setupData.BatchFields[i].Name, KfxLinkSourceType.KFX_REL_BATCHFIELD, "{$" + setupData.BatchFields[i].Name + "}");
            }
            for (int i = 1; i <= setupData.IndexFields.Count; i++)
            {
                setupData.Links.Add(setupData.IndexFields[i].Name, KfxLinkSourceType.KFX_REL_INDEXFIELD, setupData.IndexFields[i].Name);
            }
            foreach (string item in setupData.BatchVariableNames)
            {
                setupData.Links.Add(item, KfxLinkSourceType.KFX_REL_VARIABLE, item);
            }
        }

        #endregion
    }
}
