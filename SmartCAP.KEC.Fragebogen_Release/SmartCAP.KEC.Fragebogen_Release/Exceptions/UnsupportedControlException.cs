﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.KEC.Exceptions
{
    public class UnsupportedControlException : Exception
    {
        public UnsupportedControlException()
        {
        }

        public UnsupportedControlException(string message) : base(message)
        {
        }

        public UnsupportedControlException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
