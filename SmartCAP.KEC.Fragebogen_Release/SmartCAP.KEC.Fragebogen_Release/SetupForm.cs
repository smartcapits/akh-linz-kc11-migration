﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Kofax.ReleaseLib;


namespace SmartCAP.KEC.Fragebogen_Release
{
    public partial class SetupForm : Form
    {
        public ReleaseSetupData SetupData;
        
        // Bindings for DataGridView
        private DataTable indexLinksTable = new DataTable();
        private BindingSource bindingSource = new BindingSource();

        //Dictionary<string, KfxLinkSourceType> indexLinks = new Dictionary<string, KfxLinkSourceType>();
        //Dictionary<string, KfxLinkSourceType> variables = new Dictionary<string, KfxLinkSourceType>();


        #region Initializers
        // Initialize Form (will be fired when form is created as object)
        public SetupForm()
        {
            try
            {
                InitializeComponent();

                // Initialize DataGridView
                InitializeIndexLinks();
            }
            catch (Exception e)
            { MessageBox.Show("Fehler beim Initialisieren! Msg: " + e.Message); }
        }

        // Pass Setup-Object to form, show form
        public void RunSetup(ReleaseSetupData setupData)
        {
            SetupData = setupData;

            // Default Background-Color für RegEx-Felder
            txtSplitFieldExpression.BackColor = Color.Salmon;
            txtTextFieldExpression.BackColor = Color.Salmon;

            // Initialize FileName DropDowns
            FillComboWithFields(cboCSVFNIndexField, SetupData);
            FillComboWithFields(cboPDFFNINdexField, SetupData);
            FillComboWithFields(cboSubFolderIndexField, SetupData);

            // Initialize or load settings
            if (SetupData.New.Equals(0))
            { LoadReleaseSettings(); }
            else
            { LoadReleaseDefaults(); }


            // Show this form
            this.ShowDialog();
        }

        public void InitializeIndexLinks()
        {
            // Bind DataGridView to DataTable, Create Columns
            bindingSource.DataSource = indexLinksTable;
            dgIndexLinks.DataSource = bindingSource;
            indexLinksTable.Columns.Add(CreateDataColumn("System.String", "Index-Name", "Index-Name"));
            indexLinksTable.Columns.Add(CreateDataColumn("System.String", "Typ", "Typ"));
            //dgIndexLinks.ShowEditingIcon = false;
            dgIndexLinks.Columns[0].Width = 175;
            dgIndexLinks.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgIndexLinks.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgIndexLinks.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        #endregion


        #region Release-Settings functions (Initialize, Load, Save, Check)
        // Enter release mask the first time - load defaults
        private void LoadReleaseDefaults()
        {
            try
            {
                // Place all BatchFields and IndexFields in the DataGrid
                foreach (BatchField batchField in SetupData.BatchFields)
                { AddRowToDataGrid(batchField.Name, "BatchField"); }

                foreach (IndexField indexField in SetupData.IndexFields)
                { AddRowToDataGrid(indexField.Name, "IndexField"); }

                // Set CSV-Settings
                txtCSVSeparatorChar.Text = ";";
                chkColumNamesInFirstRow.Checked = true;
                chkSetValuesInHighCommas.Checked = false;

                // Set IndexField-Settings
                txtSplitFieldSeparator.Text = "-";
            }
            catch (Exception e)
            {
                MessageBox.Show("Fehler beim Laden der Release-Defaults! Msg: " + e.Message);
                this.Close();
            }   
        }


        // Load settings when release script has already been set up
        private void LoadReleaseSettings()
        {
            try
            {
                // Load all links to DataGrid
                foreach (Link link in SetupData.Links)
                {
                    switch (link.SourceType)
                    {
                        case KfxLinkSourceType.KFX_REL_INDEXFIELD: AddRowToDataGrid(link.Destination, "IndexField"); break;
                        case KfxLinkSourceType.KFX_REL_BATCHFIELD: AddRowToDataGrid(link.Destination, "BatchField"); break;
                        case KfxLinkSourceType.KFX_REL_TEXTCONSTANT: AddRowToDataGrid(link.Destination, "FixedValue"); break;
                        default: break;
                    }
                }

                // Load CSV specific settings
                txtCSVSeparatorChar.Text = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.CSVSeparatorChar");
                if (KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.CSVColumnNamesInFirstRow").Equals(true.ToString())) { chkColumNamesInFirstRow.Checked = true; }
                if (KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.CSVValuesInHighCommas").Equals(true.ToString())) { chkSetValuesInHighCommas.Checked = true; }

                // Load IndexField options
                txtSplitFieldExpression.Text = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.SplitFieldExpression");
                txtSplitFieldSeparator.Text = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.SplitFieldSeparator");
                txtTextFieldExpression.Text = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.TextFieldExpression");

                // Load folders and paths
                txtCSVFilePath.Text = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.CSVFilePath");
                txtTIFFilePath.Text = SetupData.ImageFilePath;
                txtPDFFilePath.Text = SetupData.KofaxPDFPath;
                
                // Load CSV-filename options
                string csvFileName = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.CSVFileName");
                switch (csvFileName)
                {
                    case "UniqueDocumentID":
                        rbXMLFNUniqueDocID.Checked = true;
                        break;
                    case "UniqueBatchID":
                        rbXMLFNUniqueBatchID.Checked = true;
                        break;
                    default:    
                        rbXMLFNIndexField.Checked = true;
                        if (cboCSVFNIndexField.Items.Contains(csvFileName))
                        { cboCSVFNIndexField.Text = csvFileName; }
                        break;
                }

                // Load ImageFilename-options
                string pdfFileName = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.ImageFileName");
                switch (pdfFileName)
                {
                    case "UniqueDocumentID":
                        rbPDFFNUniqueDocID.Checked = true;
                        break;
                    default: 
                        rbPDFFNIndexField.Checked = true;
                        if (cboPDFFNINdexField.Items.Contains(pdfFileName))
                        { cboPDFFNINdexField.Text = pdfFileName; }
                        break;
                }


                // Load SubFolder-options
                string subFolderName = KofaxReleaseTools.GetSetupCSSValue(SetupData, "FragebogenReleasse.SubFolderName");
                switch (subFolderName)
                {
                    case "":
                        rbNoSubFolder.Checked = true;
                        break;
                    case "UniqueDocumentID":
                        rbDocIDSubFolder.Checked = true;
                        break;
                    default:
                        rbUseSubFolderIndexField.Checked = true;
                        if (cboSubFolderIndexField.Items.Contains(subFolderName))
                        { cboSubFolderIndexField.Text = subFolderName; }
                        break;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fehler beim Laden der Release-Einstellungen! Msg: " + e.Message);
            }
        }

        private bool CheckReleaseSettings()
        {
            try
            {
                // Check if CSV-Settings are correct
                if (txtCSVSeparatorChar.Text.Equals("")) { MessageBox.Show("Bitte geben Sie ein CSV-Trennzeichen an"); return false; }
                
                // Check Indexfieldprocessing-Settings
                if (txtSplitFieldExpression.Text.Equals("")) { MessageBox.Show("Bitte geben Sie eine Expression für Aufspaltfelder an"); return false; }
                else {  }
                if (txtSplitFieldSeparator.Text.Equals("")) { MessageBox.Show("Bitte geben Sie ein Trennzeichen für Aufspaltfelder an"); return false; }
                //if (txtTextFieldExpression.Text.Equals("")) { MessageBox.Show("Bitte geben Sie eine Expression für Textfelder an"); return false; }
                //else { }

                // Check if we have at least 1 output-folder set. 
                if (txtCSVFilePath.Text.Equals("") && txtTIFFilePath.Text.Equals("") && txtPDFFilePath.Text.Equals(""))
                { MessageBox.Show("Bitte geben Sie zumindest einen Speicherpfad für CSV-, TIF- oder PDF-Files an"); tabControl.SelectTab(1); return false; }
                else
                {
                    // Check if paths are existing
                    if (!txtCSVFilePath.Text.Equals("")) { if (!Directory.Exists(txtCSVFilePath.Text)) { MessageBox.Show("Der Angegebene Pfad für CSV-Dateien existiert nicht!"); tabControl.SelectTab(1); txtCSVFilePath.Focus(); txtCSVFilePath.SelectAll(); return false; } }
                    if (!txtPDFFilePath.Text.Equals("")) { if (!Directory.Exists(txtPDFFilePath.Text)) { MessageBox.Show("Der Angegebene Pfad für PDF-Dateien existiert nicht!"); tabControl.SelectTab(1); txtPDFFilePath.Focus(); txtPDFFilePath.SelectAll(); return false; } }
                    if (!txtTIFFilePath.Text.Equals("")) { if (!Directory.Exists(txtTIFFilePath.Text)) { MessageBox.Show("Der Angegebene Pfad für TIF-Dateien existiert nicht!"); tabControl.SelectTab(1); txtTIFFilePath.Focus(); txtTIFFilePath.SelectAll(); return false; } }
                }
            }
            catch (Exception)
            { return false; }

            return true;
        }

        // Save release settings
        private bool SaveReleaseSettings()
        {
            try
            {
                // Clear Release-Settings
                SetupData.Links.RemoveAll();
                SetupData.CustomProperties.RemoveAll();

                // Save Index links
                foreach (DataRow row in indexLinksTable.Rows)
                {
                    switch (row.ItemArray[1].ToString())
                    {
                        case "IndexField": SetupData.Links.Add(row.ItemArray[0].ToString(), KfxLinkSourceType.KFX_REL_INDEXFIELD, row.ItemArray[0].ToString()); break;
                        case "BatchField": SetupData.Links.Add(row.ItemArray[0].ToString(), KfxLinkSourceType.KFX_REL_BATCHFIELD, "{$" + row.ItemArray[0].ToString() + "}"); break;
                        case "FixedValue": SetupData.Links.Add(row.ItemArray[0].ToString(), KfxLinkSourceType.KFX_REL_TEXTCONSTANT, row.ItemArray[0].ToString()); break;
                        default: break;
                    }
                }

                // Save CSV-settings
                SetupData.CustomProperties.Add("FragebogenReleasse.CSVSeparatorChar", txtCSVSeparatorChar.Text);
                SetupData.CustomProperties.Add("FragebogenReleasse.CSVColumnNamesInFirstRow", chkColumNamesInFirstRow.Checked.ToString());
                SetupData.CustomProperties.Add("FragebogenReleasse.CSVValuesInHighCommas", chkSetValuesInHighCommas.Checked.ToString());

                // Save IndexField-settings
                SetupData.CustomProperties.Add("FragebogenReleasse.SplitFieldExpression", txtSplitFieldExpression.Text);
                SetupData.CustomProperties.Add("FragebogenReleasse.SplitFieldSeparator", txtSplitFieldSeparator.Text);
                SetupData.CustomProperties.Add("FragebogenReleasse.TextFieldExpression", txtTextFieldExpression.Text);

                // Save Folders
                if (!txtCSVFilePath.Text.Equals("")) { SetupData.CustomProperties.Add("FragebogenReleasse.CSVFilePath", txtCSVFilePath.Text); }
                else { SetupData.CustomProperties.Add("FragebogenReleasse.CSVFilePath", ""); }
                if (!txtPDFFilePath.Text.Equals("")) { SetupData.KofaxPDFPath = txtPDFFilePath.Text; SetupData.KofaxPDFReleaseScriptEnabled = true; }
                    else { SetupData.KofaxPDFPath = "";  SetupData.KofaxPDFReleaseScriptEnabled = false; }
                if (!txtTIFFilePath.Text.Equals("")) { SetupData.ImageFilePath = txtTIFFilePath.Text; }
                    else { SetupData.ImageFilePath = ""; }

                // Save SubFolder- settings - 
                if (rbNoSubFolder.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.SubFolderName", ""); }
                else if (rbDocIDSubFolder.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.SubFolderName", "UniqueDocumentID"); }
                else if (rbUseSubFolderIndexField.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.SubFolderName", cboSubFolderIndexField.Text); }

                // Save Filename settings - CSV-File
                if (rbXMLFNUniqueDocID.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.CSVFileName", "UniqueDocumentID"); }
                else if (rbXMLFNUniqueBatchID.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.CSVFileName", "UniqueBatchID"); }
                else if (rbXMLFNIndexField.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.CSVFileName", cboCSVFNIndexField.Text); }

                // Save Filename settings - Image-File
                if (rbPDFFNUniqueDocID.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.ImageFileName", "UniqueDocumentID"); }
                else if (rbPDFFNIndexField.Checked) { SetupData.CustomProperties.Add("FragebogenReleasse.ImageFileName", cboPDFFNINdexField.Text); }

            }
            catch (Exception e)
            {
                MessageBox.Show("Fehler beim Speichern der Release-Einstellungen! Msg: " + e.Message);
                return false; 
            }

            return true;
        }
        #endregion


        #region Folder-Selection buttons
        private void btnOpenXMLFileDialog_Click(object sender, EventArgs e)
        {
            folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
            if (folderBrowser.ShowDialog().Equals(DialogResult.OK)) { txtCSVFilePath.Text = folderBrowser.SelectedPath; }
        }

        private void btnOpenPDFFileDialog_Click(object sender, EventArgs e)
        {
            folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
            if (folderBrowser.ShowDialog().Equals(DialogResult.OK)) { txtPDFFilePath.Text = folderBrowser.SelectedPath; }
        }

        private void btnOpenTIFFIleDialog_Click(object sender, EventArgs e)
        {
            folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
            if (folderBrowser.ShowDialog().Equals(DialogResult.OK)) { txtTIFFilePath.Text = folderBrowser.SelectedPath; }
        }
        #endregion


        #region Index-Links buttons and methods
        private void btnIndexAdd_Click(object sender, EventArgs e)
        {
            AddIndexLink addLink = new AddIndexLink();

            KeyValuePair<string, KfxLinkSourceType> newField = addLink.OpenLinkForm(SetupData);
            if (!newField.Key.Equals(""))
            {
                // Check if field already exists
                if (DataTableItemExists(indexLinksTable, newField.Key))
                { MessageBox.Show("Eine Indexzuweisung mit dem Namen '" + newField.Key + "' existiert bereits!"); }
                else
                {
                    switch (newField.Value)
                    {
                        case KfxLinkSourceType.KFX_REL_INDEXFIELD:
                            AddRowToDataGrid(newField.Key, "IndexField");
                            break;
                        case KfxLinkSourceType.KFX_REL_BATCHFIELD:
                            AddRowToDataGrid(newField.Key, "BatchField");
                            break;
                        case KfxLinkSourceType.KFX_REL_TEXTCONSTANT:
                            AddRowToDataGrid(newField.Key, "FixedValue");
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void btnIndexUp_Click(object sender, EventArgs e)
        {
            MoveUpIndexLink();
        }

        private void btnIndexDown_Click(object sender, EventArgs e)
        {
            MoveDownIndexLink();
        }

        // Move up DataTable entry (will move up selected line in DGV)
        private void MoveUpIndexLink()
        {
            try
            {
                if (dgIndexLinks.SelectedRows.Count.Equals(1) && !dgIndexLinks.SelectedRows[0].Index.Equals(0))
                {
                    int selectedIndex = dgIndexLinks.SelectedRows[0].Index;
                    DataRow selectedRow = indexLinksTable.Rows[selectedIndex];
                    DataRow newRow = indexLinksTable.NewRow();
                    newRow.ItemArray = selectedRow.ItemArray; // copy data
                    indexLinksTable.Rows.Remove(selectedRow);
                    indexLinksTable.Rows.InsertAt(newRow, selectedIndex - 1);
                    dgIndexLinks.Rows[selectedIndex - 1].Selected = true;
                }
            }
            catch (Exception)
            { }
        }

        // Move down DataTable entry (will move down selected line in DGV)
        private void MoveDownIndexLink()
        {
            try
            {
                if (dgIndexLinks.SelectedRows.Count.Equals(1) && !dgIndexLinks.SelectedRows[0].Index.Equals(dgIndexLinks.Rows.Count - 1))
                {
                    int selectedIndex = dgIndexLinks.SelectedRows[0].Index;
                    DataRow selectedRow = indexLinksTable.Rows[selectedIndex];
                    DataRow newRow = indexLinksTable.NewRow();
                    newRow.ItemArray = selectedRow.ItemArray; // copy data
                    indexLinksTable.Rows.Remove(selectedRow);
                    indexLinksTable.Rows.InsertAt(newRow, selectedIndex + 1);
                    dgIndexLinks.Rows[selectedIndex + 1].Selected = true;
                }
            }
            catch (Exception)
            { }
        }

        // Remove selected Index link
        private void btnIndexRemove_Click(object sender, EventArgs e)
        {
            if (dgIndexLinks.SelectedRows.Count.Equals(1))
            {
                int selectedIndex = dgIndexLinks.SelectedRows[0].Index;
                DataRow selectedRow = indexLinksTable.Rows[selectedIndex];
                indexLinksTable.Rows.Remove(selectedRow);
            }
        }

        private void AddRowToDataGrid(string indexName, string typ)
        {
            DataRow newRow = indexLinksTable.NewRow();
            newRow[0] = indexName;
            newRow[1] = typ;
            indexLinksTable.Rows.Add(newRow);
        }

        private static DataColumn CreateDataColumn(string typ, string name, string caption)
        {
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType(typ);
            column.ColumnName = name;
            column.Caption = caption;
            return column;
        }
        #endregion


        #region OK - Cancel - buttons
        private void btnSaveExit_Click(object sender, EventArgs e)
        {
            if (CheckReleaseSettings())
            {
                SaveReleaseSettings();
                SetupData.Apply();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Möchten Sie die Änderungen verwerfen und die Einrichtungsmaske verlassen?", "Einrichtungsmaske verlassen", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
            { this.Close(); }
        }
        #endregion


        #region TextFieldFunctions
        private void txtSplitFieldExpression_TextChanged(object sender, EventArgs e)
        {
            // Check ob Expression gültig ist
            if (!txtSplitFieldExpression.Text.Equals("") && CheckRegExValidity(txtSplitFieldExpression.Text)) { txtSplitFieldExpression.BackColor = Color.LightGreen; }
            else { txtSplitFieldExpression.BackColor = Color.Salmon; }
        }

        private void txtTextFieldExpression_TextChanged(object sender, EventArgs e)
        {
            // Check ob Expression gültig ist
            if (!txtTextFieldExpression.Text.Equals("") && CheckRegExValidity(txtTextFieldExpression.Text)) { txtTextFieldExpression.BackColor = Color.LightGreen; }
            else { txtTextFieldExpression.BackColor = Color.Salmon; }
        }

        #endregion


        #region Local Functions
        private void FillComboWithFields(ComboBox comBox, ReleaseSetupData setupData)
        {
            foreach (BatchField batchField in SetupData.BatchFields)
            { comBox.Items.Add(batchField.Name); }

            foreach (IndexField indexField in SetupData.IndexFields)
            { comBox.Items.Add(indexField.Name); }

            // Select first entry if available
            if (comBox.Items.Count > 0) { comBox.Text = comBox.Items[0].ToString(); }
        }


        private bool DataTableItemExists(DataTable dataTable, string itemName)
        {
            foreach (DataRow row in dataTable.Rows)
            {
                if (row.ItemArray[0].ToString().Equals(itemName))
                { return true; }
            }

            return false;
        }


        private bool CheckRegExValidity(string expression)
        {
            try
            {
                Regex regex = new Regex(expression);
                return true;
            }
            catch
            { return false; }
        }

        #endregion

        

        

 
    }
}
