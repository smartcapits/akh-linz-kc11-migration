﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartCAP.KEC.Helpers
{
    public static class Serialization
    {
        public static TData DeserializeFromString<TData>(string settings)
        {
            byte[] b = Convert.FromBase64String(settings);
            using (var stream = new MemoryStream(b))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TData));
                stream.Seek(0, SeekOrigin.Begin);
                return (TData)serializer.Deserialize(stream);
            }
        }

        public static string SerializeToString<TData>(TData settings)
        {
            using (var stream = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TData));
                serializer.Serialize(stream, settings);
                stream.Flush();
                stream.Position = 0;
                return Convert.ToBase64String(stream.ToArray());
            }
        }

        /// <summary>
        /// Serializes any object to JSON, using DataContractJsonSerializer (standard .NET, no third party package)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeToJson<T>(T item)
        {
            string json = "";
            using (var ms = new MemoryStream())
            {
                // Serializer the User object to the stream.  
                var serializer = new DataContractJsonSerializer(typeof(T));
                serializer.WriteObject(ms, item);
                byte[] jsonArr = ms.ToArray();
                json = Encoding.UTF8.GetString(jsonArr, 0, jsonArr.Length);
            }
            return json;
        }

        /// <summary>
        /// Deserializes any object from JSON, using DataContractJsonSerializer (standard .NET, no third party package) 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T DeserializeJson<T>(string json)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            T item;
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                item = (T)serializer.ReadObject(ms);
            }
            return item;
        }
    }
}
