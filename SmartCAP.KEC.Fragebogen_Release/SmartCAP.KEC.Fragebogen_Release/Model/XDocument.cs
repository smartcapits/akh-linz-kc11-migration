﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SmartCAP.KEC.Model
{
    public enum BenchmarkType { TruePositive, FalseNegative, TrueNegative, FalsePositive }

    public class XDocument
    {
        public string FilePath { get; set; }
        public string ExtractionClass { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<XDocumentField> Fields
        {
            get; //{ return _fields; }
            set;
        }
        //private List<XdcField> _fields;


        XmlDocument xdoc;
        string xdcSourcePath;


        public XDocument()
        {
            //xdoc = new XmlDocument();
        }


        public XDocument(string xdcSourcePath, bool prefetchAllFields = false)
        {
            Load(xdcSourcePath, prefetchAllFields);
        }


        public void Load(string xdcSourcePath, bool prefetchAllFields = false)
        {
            xdoc = new XmlDocument();
            this.xdcSourcePath = xdcSourcePath;
            using (Stream gz = new GZipStream(File.OpenRead(xdcSourcePath), CompressionMode.Decompress))
            {
                xdoc.Load(gz);
            }
            LastModifiedDate = File.GetLastWriteTime(xdcSourcePath);
            FilePath = xdcSourcePath;
            ExtractionClass = xdoc.SelectSingleNode("/xdoc").Attributes["extclass"].Value;
            if (prefetchAllFields)
            {
                Fields = GetFields();
                Unload();
            }
        }


        public void Unload()
        {
            xdoc = null;
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
        }


        public void Save()
        {
            Save(xdcSourcePath, xdoc);
        }


        public void Save(string xdcTargetPath)
        {
            Save(xdcTargetPath, xdoc);
        }


        public void Save(string xdcTargetPath, XmlDocument xdoc)
        {
            using (FileStream fs = new FileStream(xdcTargetPath, FileMode.Create))
            using (GZipStream gz = new GZipStream(fs, CompressionMode.Compress))
            {
                xdoc.Save(gz);
            }
        }


        public XmlDocument GetXmlDocument()
        {
            return xdoc;
        }


        public List<string> GetSourceFiles()
        {
            string xdcDirectory = Path.GetDirectoryName(xdcSourcePath);
            List<string> sourceFiles = new List<string>();
            foreach (XmlNode file in xdoc.SelectNodes("/xdoc/cdoc/sourcefiles/sourcefile"))
            {
                sourceFiles.Add(Path.Combine(xdcDirectory, file.Attributes["relpath"].Value, file.Attributes["fname"].Value));
            }
            return sourceFiles;
        }


        public void CopyXDocWithSourceFilesToPath(string targetPath, bool uniqueSubDirectory = false)
        {
            if (uniqueSubDirectory)
            {
                targetPath = Path.Combine(targetPath, Guid.NewGuid().ToString());
            }
            Directory.CreateDirectory(targetPath);
            List<string> files = GetSourceFiles();
            files.Add(xdcSourcePath);
            foreach (string file in files)
            {
                File.Copy(file, Path.Combine(targetPath, Path.GetFileName(file)), true);
            }
        }


        public string GetFieldValue(string fieldName, string valueIfNotFound = "")
        {
            XmlNode field = xdoc.SelectSingleNode(String.Format("/xdoc/fields/field[@name='{0}']/text", fieldName));
            if (field == null)
                return valueIfNotFound;
            else
                return field.InnerText;
        }


        public XDocumentField GetField(string fieldName)
        {
            XDocumentField f = new XDocumentField();
            try
            {
                if (xdoc == null) // if xdoc was unloaded, try to get field from Fields list
                    return Fields.Find(x => x.Name == fieldName);
                XmlNode field = xdoc.SelectSingleNode(String.Format("/xdoc/fields/field[@name='{0}']", fieldName));
                f.Name = fieldName;
                //f.OriginalText = XmlNodeGetSubNodeValue(field, "orgtext");
                f.Text = XmlNodeGetSubNodeValue(field, "text");
                //f.ValidatedText = XmlNodeGetSubNodeValue(field, "valtext");
                //f.OriginalValid = Boolean.Parse(XmlNodeGetAttribute(field, "orgvald", "0"));
                //f.OriginalValid = XmlNodeGetAttribute(field, "orgvald", "0") == "1";
                f.PageIndex = Convert.ToInt32(XmlNodeGetAttribute(field, "pagenr", "-1"));
                //f.BenchmarkResult = f.GetBenchmarkType();

                f.ErrorDescription = XmlNodeGetAttribute(field, "edesc");
                f.Valid = XmlNodeGetAttribute(field, "valid") == "0" ? false : true;
                f.Confidence = double.Parse(XmlNodeGetAttribute(field, "conf"));
                f.Top = Convert.ToInt32(XmlNodeGetAttribute(field, "top", "0"));
                f.Left = Convert.ToInt32(XmlNodeGetAttribute(field, "left", "0"));
                f.Width = Convert.ToInt32(XmlNodeGetAttribute(field, "width", "0"));
                f.Height = Convert.ToInt32(XmlNodeGetAttribute(field, "height", "0"));
                return f;
            }
            catch
            {
                return null;
            }
        }


        public List<XDocumentField> GetFields()
        {
            List<XDocumentField> fields = new List<XDocumentField>();

            XmlNodeList nodes = xdoc.SelectNodes("/xdoc/fields/field");
            foreach (XmlNode node in nodes)
            {
                XDocumentField field = GetField(node.Attributes["name"].Value);
                if (field != null)
                    fields.Add(field);

            }
            return fields;
        }


        public List<Dictionary<string, string>> GetTableRows(string tableName)
        {
            XmlNode table = xdoc.SelectSingleNode(String.Format("/xdoc/fields/field[@name='{0}']/table", tableName));
            List<Dictionary<string, string>> rows = new List<Dictionary<string, string>>();
            Dictionary<string, string> columns = new Dictionary<string, string>();
            Dictionary<string, string> cells;
            foreach (XmlNode column in table.SelectNodes("cols/col"))
            {
                columns.Add(column.Attributes["gcid"].Value, column.Attributes["name"].Value);
            }

            foreach (XmlNode row in table.SelectNodes("rows/row"))
            {
                cells = new Dictionary<string, string>();
                foreach (XmlNode cell in row.SelectNodes("cells/cell"))
                {
                    if (cell.Attributes["text"] == null)
                        cells.Add(columns[cell.Attributes["gcid"].Value], "");
                    else
                        cells.Add(columns[cell.Attributes["gcid"].Value], cell.Attributes["text"].Value);
                }
                rows.Add(cells);
            }

            return rows;
        }


        public List<string> GetTableColumnValues(string tableName, string columnName)
        {
            List<string> values = new List<string>();
            var rows = GetTableRows(tableName);
            foreach (var row in rows)
            {
                values.Add(row[columnName]);
            }
            return values;
        }

        #region helpers
        private string XmlNodeGetSubNodeValue(XmlNode node, string subNodeName, string valueIfNotFound = "")
        {
            XmlNode sub = node.SelectSingleNode(subNodeName);
            if (sub == null)
                return valueIfNotFound;
            else
                return sub.InnerText;
        }

        private string XmlNodeGetAttribute(XmlNode node, string attributeName, string valueIfNotFound = "")
        {
            XmlAttribute attr = node.Attributes[attributeName];
            if (attr == null)
                return valueIfNotFound;
            else
                return attr.Value;
        }
        #endregion helpers
    }
}
