﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.KEC.Model
{
    public class XDocumentField
    {
        public string Name { get; set; }
        public string Text { get; set; }
        //public string ValidatedText { get; set; }
        //public string OriginalText { get; set; }
        //public bool OriginalValid { get; set; }        
        //public BenchmarkType BenchmarkResult { get; set; }
        public string ErrorDescription { get; set; }
        public double Confidence { get; set; }
        public bool Valid { get; set; }
        public int PageIndex { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }


        //public BenchmarkType GetBenchmarkType()
        //{
        //    if (OriginalText == ValidatedText && OriginalValid == true)
        //        return BenchmarkType.TruePositive;
        //    else if (OriginalText == ValidatedText && OriginalValid == false)
        //        return BenchmarkType.FalseNegative;
        //    else if (OriginalText != ValidatedText && OriginalValid == false)
        //        return BenchmarkType.TrueNegative;
        //    else //if (OriginalText != ValidatedText && OriginalValid == true)
        //        return BenchmarkType.FalsePositive;
        //}
    }
}
