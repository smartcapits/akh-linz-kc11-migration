﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartCAP.KEC.Model
{
    public class Row
    {
        public List<Cell> Cells { get; set; }


        public Row()
        {
            Cells = new List<Cell>();
        }


        public Cell GetCell(string Name)
        {
            if (Cells.Exists(x => x.Name == Name))
            {
                return Cells.First(x => x.Name == Name);
            }
            else
            {
                throw new Exception(string.Format("a cell with the name {0} was not found in the collection", Name));
            }
        }
    }
}
