﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SmartCAP.KEC.Model
{
    public class Table
    {
        public List<Row> Rows { get; set; }
        public string Name { get; set; }


        private static readonly ILog Log =
          LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.BaseType.Name);


        public Table()
        {
            Rows = new List<Row>();
        }


        public Table(string Name, List<Column> Columns)
        {
            this.Name = Name;
            Rows = new List<Row>();
            // = number of rows; all columns MUST have the same amount of cells (= rows)
            // first, we create the rows
            for (int i = 0; i < Columns.First().Cells.Count; i++)
            {
                Rows.Add(new Row());
            }

            Log.Debug("Added " + Rows.Count + "  Rows");

            // then, we populate the individual cells
            foreach (var column in Columns)
            {
                for (int i = 0; i < column.Cells.Count; i++)
                {
                    Log.Debug("Adding cell with name " + column.Cells[i].Name + " and value " + column.Cells[i].Value + " to row number " + i);
                    Rows[i].Cells.Add(new Cell(column.Cells[i].Name, column.Cells[i].Value));
                }
            }
        }


        public string GetTableAsCsv(char separator)
        {
            string csv = string.Empty;

            foreach (Row row in this.Rows)
            {
                foreach (Cell cell in row.Cells)
                {
                    csv += cell.Value + separator;
                }
                csv += Environment.NewLine;
            }

            return csv;
        }
    }
}
