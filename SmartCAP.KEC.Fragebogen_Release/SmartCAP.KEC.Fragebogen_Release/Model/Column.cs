﻿using System.Collections.Generic;

namespace SmartCAP.KEC.Model
{
    public class Column
    {
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public List<Cell> Cells { get; set; }


        public Column(string TableName, string ColumnName, string[] Cells)
        {
            this.TableName = TableName;
            this.ColumnName = ColumnName;
            this.Cells = new List<Cell>();
            foreach (string c in Cells)
            {
                this.Cells.Add(new Cell(ColumnName, c));
            }
        }
    }
}
