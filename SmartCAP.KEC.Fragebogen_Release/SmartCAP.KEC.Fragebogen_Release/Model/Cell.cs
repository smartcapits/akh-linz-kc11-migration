﻿namespace SmartCAP.KEC.Model
{
    public class Cell
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Cell(string Name, string Value)
        {
            this.Name = Name;
            this.Value = Value;
        }

    }
}
