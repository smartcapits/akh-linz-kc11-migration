﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmartCAP.KEC.Model
{
    [Serializable]
    public class ExtensionField
    {
        public enum FieldType { BatchField, IndexField }

        [Browsable(false)]
        public string Name { get; set; }
        [Browsable(false)]
        public FieldType Type { get; set; }
        [Category("Test")]
        [Description("Use this provide a value for testing your regular expression.")]
        public string Value { get; set; } = "";
        [Category("Field Validation")]
        [Description("When the regular expression pattern does not match, processing will fail with this error. You can provide {0} as a placeholder for the field's value.")]
        public string ErrorText { get; set; } = "";
        [Category("Field Validation")]
        [Description("The regular expression pattern to check against.")]
        public string RegExPattern { get; set; } = "";
        // in case you need more extension properties, here's how you can define them
        [Category("Configuration")]
        [Description("An alternative name for the field if required in export.")]
        public string AlternativeName { get; set; } = "";
        [Category("Configuration")]
        [Description("An alternative name for the field if required in export.")]
        public bool UseInExport { get; set; } = true;


        public ExtensionField()
        {
            this.Name = "";
        }


        public ExtensionField(string Name, FieldType FieldType)
        {
            this.Name = Name;
            this.Type = FieldType;
        }


        public bool IsValid()
        {
            Regex r = new Regex(RegExPattern);
            return r.Match(Value).Success;
        }


        public string GetInterpolatedErrorText()
        {
            return string.Format(ErrorText, Value);
        }


        public override string ToString()
        {
            if (Type == FieldType.BatchField) return "$" + Name;
            return Name;
        }


    }
}
