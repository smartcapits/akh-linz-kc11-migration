﻿using System;
using System.Collections.Generic;
using Kofax.ReleaseLib;
using System.IO;
using System.Linq;
using log4net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using SmartCAP.KEC.Helpers;

namespace SmartCAP.KEC.Model
{
    public class Document
    {
        /// <summary>
        /// A list of all BatchFields
        /// </summary>
        public Dictionary<string, string> BatchFields { get; private set; }
        /// <summary>
        /// A list of all IndexFields and FolderFields. FolderFields can be accessed in the form: "[FolderName].FieldName"
        /// </summary>
        public Dictionary<string, string> IndexFields { get; private set; }
        /// <summary>
        /// A list of all KofaxValues (BatchVariables)
        /// </summary>
        public Dictionary<string, string> KofaxValues { get; set; }
        /// <summary>
        /// A list of all TextConstants
        /// </summary>
        public Dictionary<string, string> TextConstants { get; set; }
        /// <summary>
        /// A list of all CustomProperties
        /// </summary>        
        public Dictionary<string, string> CustomProperties { get; private set; }
        /// <summary>
        /// A list of all Tables
        /// </summary>
        public List<Table> Tables { get; private set; }
        private List<Column> Columns;

        /// <summary>
        /// Contains a list of the document source files (e.g. single page tifs or pdf, if e-documents were imported)
        /// </summary>
        public List<string> ImageFileNames { get; private set; }
        /// <summary>
        /// Contains the path to the converted PDF file, if PDF Generator was executed
        /// </summary>
        public string KofaxPDFFileName { get; private set; }
        /// <summary>
        /// Contains the path to the XDocument, if KTM Server was executed
        /// </summary>
        public string XdcFilePath { get; private set; }

        /// <summary>
        /// XDocument wrapper class, if KTM Server was executed
        /// </summary>
        public XDocument XDocument { get; private set; }

        /// <summary>
        /// Contains the DocumentData.ImageFilePath, if used in setup
        /// </summary>
        public string ImageFilePath { get; private set; }

        /// <summary>
        /// Contains the DocumentData.KofaxPDFPath, if used in setup
        /// </summary>
        public string KofaxPDFPath { get; private set; }

        /// <summary>
        /// Contains the DocumentData.TextFilePath, if used in setup
        /// </summary>
        public string TextFilePath { get; private set; }

        /// <summary>
        /// Contains the document as multipage-tif, if the source files were tif-files (will be null in case of source pdf / e-documents)
        /// </summary>
        public byte[] BinaryImage { get; private set; }

        public string CellSeparator { get; set; }

        public int UniqueId { get; private set; }
        public string UniqueIdHex { get; private set; }

        private static readonly ILog Log =
           LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.BaseType.Name);

        /// <summary>
        /// initializes the document from a releasedata object, filling index and batch fields, as well as text constants and more stuff.
        /// </summary>
        /// <param name="DocumentData"></param>
        public Document(ReleaseData DocumentData, string cellSeparator = ";")
        {
            BatchFields = new Dictionary<string, string>();
            IndexFields = new Dictionary<string, string>();
            KofaxValues = new Dictionary<string, string>();
            TextConstants = new Dictionary<string, string>();
            CustomProperties = new Dictionary<string, string>();
            Tables = new List<Table>();
            Columns = new List<Column>();
            ImageFileNames = new List<string>();
            CellSeparator = cellSeparator;

            // add index and batch fields
            foreach (Value v in DocumentData.Values)
            {
                switch (v.SourceType)
                {
                    case KfxLinkSourceType.KFX_REL_BATCHFIELD:
                        BatchFields.Add(v.SourceName, v.Value);
                        break;
                    case KfxLinkSourceType.KFX_REL_DOCUMENTID:
                        break;
                    case KfxLinkSourceType.KFX_REL_INDEXFIELD:
                        // index fields may also contain table fields (TableName <> "") and folder fields (SourceName = "[FolderName].IndexFieldName")
                        if (v.TableName == "")
                        {
                            IndexFields.Add(v.SourceName, v.Value); // this is a regular index field
                        }
                        else
                        {
                            // this is a table field! 
                            Columns.Add(new Column(v.TableName, v.SourceName, String.IsNullOrEmpty(v.Value) ? new string[] { "" } : v.Value.Split(new string[] { cellSeparator }, StringSplitOptions.None)));
                        }
                        break;
                    case KfxLinkSourceType.KFX_REL_TEXTCONSTANT:
                        TextConstants.Add(v.SourceName, v.Value);
                        break;
                    case KfxLinkSourceType.KFX_REL_UNDEFINED_LINK:
                        break;
                    case KfxLinkSourceType.KFX_REL_VARIABLE:
                        KofaxValues.Add(v.SourceName, v.Value);
                        break;
                }
            }

            // add the custom properties (but don't expose passwords)
            foreach (CustomProperty p in DocumentData.CustomProperties)
            {
                CustomProperties.Add(p.Name, p.Value);
            }

            ImageFilePath = DocumentData.ImageFilePath;
            KofaxPDFPath = DocumentData.KofaxPDFPath;
            KofaxPDFFileName = DocumentData.KofaxPDFFileName;
            TextFilePath = DocumentData.TextFilePath;
            UniqueId = DocumentData.UniqueDocumentID;
            UniqueIdHex = DocumentData.UniqueDocumentID.ToString("X8");

            // load the image file names into a collection
            foreach (ImageFile img in DocumentData.ImageFiles)
            {
                ImageFileNames.Add(img.FileName);
            }

            // set the binary image
            DocumentData.ImageFiles.Copy(Path.GetTempPath(), -1);
            string tmpFile = Path.Combine(Path.GetTempPath(), DocumentData.UniqueDocumentID.ToString("X8")) + Path.GetExtension(ImageFileNames[0]);
            if (File.Exists(tmpFile))
            {
                BinaryImage = File.ReadAllBytes(tmpFile);
                File.Delete(tmpFile);
            }

            // set xdoc path, if exists
            if (ImageFileNames.Count > 0)
            {
                string tmpFileName = Path.Combine(Path.GetDirectoryName(ImageFileNames[0]), Path.GetFileNameWithoutExtension(ImageFileNames[0]) + ".xdc");
                if (File.Exists(tmpFileName))
                {
                    XdcFilePath = tmpFileName;
                    XDocument = new XDocument(XdcFilePath, true);
                }
            }

            // add table rows
            foreach (var n in Columns.GroupBy(x => x.TableName).Select(n => new { n.Key, Columns = n.ToList() }))
            {
                Tables.Add(new Table(n.Key, n.Columns));
            }
        }


        public string GetIndexField(string key, string valueIfNotFound)
        {
            string k = key.Length > 32 ? key.Substring(0, 32) : key;
            if (IndexFields.ContainsKey(k)) return IndexFields[k];
            else return valueIfNotFound;
        }

        public string GetBatchField(string key, string valueIfNotFound)
        {
            string k = key.Length > 32 ? key.Substring(0, 32) : key;
            if (BatchFields.ContainsKey(k)) return BatchFields[k];
            else return valueIfNotFound;
        }

        public string GetTextConstant(string key, string valueIfNotFound)
        {
            string k = key.Length > 32 ? key.Substring(0, 32) : key;
            if (TextConstants.ContainsKey(k)) return TextConstants[k];
            return valueIfNotFound;
        }

        public string GetCustomProperty(string key, string valueIfNotFound)
        {
            if (CustomProperties.ContainsKey(key)) return CustomProperties[key];
            else return valueIfNotFound;
        }

        public Table GetTable(string Name)
        {
            if (Tables.Exists(x => x.Name == Name))
            {
                return Tables.First(x => x.Name == Name);
            }
            else
            {
                throw new Exception(string.Format("a table with the name {0} was not found in the collection", Name));
            }
        }

        private static List<List<T>> Transpose<T>(List<List<T>> lists)
        {
            var longest = lists.Any() ? lists.Max(l => l.Count) : 0;
            List<List<T>> outer = new List<List<T>>(longest);
            for (int i = 0; i < longest; i++)
                outer.Add(new List<T>(lists.Count));
            for (int j = 0; j < lists.Count; j++)
                for (int i = 0; i < longest; i++)
                    outer[i].Add(lists[j].Count > i ? lists[j][i] : default(T));
            return outer;
        }

        /// <summary>
        /// Replaces placeholders with values from Batch Fields, Index Fields, Folder Fields or Kofax Values.
        /// Placeholders must be defined in the form {Kofax Value}, {$Batch Field Name}, {@Index Field Name} or {@[Folder Name].Folder Field Name}
        /// </summary>
        /// <param name="text">The text to search for replace placeholders</param>
        /// <param name="escapeInvalidChars">If set to true, illegal path or filename characters will be replaced with the replaceChar</param>
        /// <param name="replaceChar">Character to replace the illegal path or filename characters with</param>
        /// <returns></returns>
        public string InterpolateStringWithFieldValues(string text, bool escapeInvalidChars = false, string replaceChar = "")
        {
            // replace batch fields: {$Field Name}
            Regex rex = new Regex(@"\{\$([\w\s]+)\}");
            foreach (Match match in rex.Matches(text))
            {
                text = replaceValue(text, match, BatchFields, escapeInvalidChars, replaceChar);
            }

            // replace index fields: {@Field Name}
            rex = new Regex(@"\{@([\w\s]+)\}");
            foreach (Match match in rex.Matches(text))
            {
                text = replaceValue(text, match, IndexFields, escapeInvalidChars, replaceChar);
            }

            // replace folder fields: {@[Folder Name].Field Name}
            rex = new Regex(@"\{@(\[[\w\s]+\]\.[\w\s]+)\}");
            foreach (Match match in rex.Matches(text))
            {
                text = replaceValue(text, match, IndexFields, escapeInvalidChars, replaceChar);
            }

            // replace kofax values: {Kofax Value}
            rex = new Regex(@"\{([\w\s]+)\}");
            foreach (Match match in rex.Matches(text))
            {
                text = replaceValue(text, match, KofaxValues, escapeInvalidChars, replaceChar);
            }
            return text;
        }

        private string replaceValue(string text, Match match, Dictionary<string, string> values, bool escapeInvalidChars, string replaceChar)
        {
            if (values.ContainsKey(match.Groups[1].Value))
            {
                string replaceValue = values[match.Groups[1].Value];
                if (escapeInvalidChars)
                {
                    Path.GetInvalidFileNameChars().ToList().ForEach(c => replaceValue = replaceValue.Replace(c.ToString(), replaceChar));
                    Path.GetInvalidPathChars().ToList().ForEach(c => replaceValue = replaceValue.Replace(c.ToString(), replaceChar));
                }
                text = text.Replace(match.Value, replaceValue);
            }
            return text;
        }

        /// <summary>
        /// Saves the document as PDF file to the target directory (requires a source pdf or the PDF Generator queue) 
        /// If the source file is PDF, this file will be copied, otherwise the file generated by PDF Generator will be copied
        /// </summary>
        /// <param name="targetFileName">The full path of the target PDF file</param>
        /// <param name="overwrite">Set to true, if the target file should be overwritten, if it exists</param>
        public void SaveToPDF(string targetFileName, bool overwrite = false)
        {
            if (ImageFileNames.Count > 0 && Path.GetExtension(ImageFileNames[0]).ToLower() == ".pdf")
                File.Copy(ImageFileNames[0], targetFileName, overwrite);
            else if (!String.IsNullOrEmpty(KofaxPDFFileName))
                File.Copy(KofaxPDFFileName, targetFileName, overwrite);
            else
                throw new Exception("Neither the source file is PDF, nor PDF Generator was executed for this document");
        }

        /// <summary>
        /// Saves the document as multipage tif to the target directory (requires single page tifs as source files)
        /// </summary>
        /// <param name="targetFileName">The full path of the target multipage tif</param>
        public void SaveToMultipageTIF(string targetFileName)
        {
            if (BinaryImage != null)
            {
                File.WriteAllBytes(targetFileName, BinaryImage);
            }
            else
            {
                throw new Exception("The source files were not in TIF format");
            }
        }


        /// <summary>
        /// If an Xdoc exists, returns a collection of fields plus the classification result as string
        /// </summary>
        /// <returns></returns>
        public string XdcToJson()
        {
            string json = "";
            if (XDocument != null)
                json = Serialization.SerializeToJson(XDocument);

            return json;
        }

        /// <summary>
        /// Saves all meta data specified to an XML document
        /// </summary>
        /// <param name="targetFileName">Full path for the XML file, including the file name</param>
        /// <param name="overwrite">Set to true, if the target file should be overwritten, if it exists</param>
        /// <param name="includeBatchFields">Set to true, to include batch fields and values</param>
        /// <param name="includeKofaxValues">Set to true, to include kofax internal values (e.g. "Batch Class", "Form Type", "Scan Operator", ...)</param>
        /// <param name="includeFilePath">Set to true, to include a reference to the exported document (TIF/PDF)</param>
        /// <param name="filePath">Full path to the exported document that should be referenced in the XML. Only used with the "includeFilePath" paramater</param>
        public void SaveFieldsToXml(string targetFileName, bool overwrite = false, bool includeBatchFields = false, bool includeKofaxValues = false, bool includeFilePath = false, string filePath = "")
        {
            if (File.Exists(targetFileName) && overwrite == false)
                throw new IOException(String.Format("The file '{0}' already exists", targetFileName));

            // create xml document with xml declaration
            XmlDocument xmlDoc = getFieldsAsXml(includeBatchFields, includeKofaxValues, includeFilePath, filePath);

            // save file
            xmlDoc.Save(targetFileName);
        }

        /// <summary>
        /// Returns all meta data specified as XML string
        /// </summary>
        /// <param name="includeBatchFields">Include Key/Values of the KC Batch Fields</param>
        /// <param name="includeKofaxValues">Include Key/Values of the KC internal values (for example "Batch Class", "Form Type", "Scan Operator", ...)</param>
        /// <param name="includeFilePath">Include an XML Node for the Image File Path (to be set via filePath Parameter)</param>
        /// <param name="filePath">FilePath of the Imagefile which should be referenced in the xml. Only works with "includeFilePath" Paramater set to true</param>
        /// <returns></returns>
        public string GetFieldsAsXml(bool includeBatchFields = false, bool includeKofaxValues = false, bool includeFilePath = false, string filePath = "")
        {
            XmlDocument xmlDoc = getFieldsAsXml(includeBatchFields, includeKofaxValues, includeFilePath, filePath);

            using (var sw = new StringWriter())
            using (var xml = XmlWriter.Create(sw))
            {
                xmlDoc.WriteTo(xml);
                xml.Flush();
                return sw.GetStringBuilder().ToString();
            }
        }

        private XmlDocument getFieldsAsXml(bool includeBatchFields = false, bool includeKofaxValues = false, bool includeFilePath = false, string filePath = "")
        {
            // create xml document with xml declaration
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, root);

            // add document element
            XmlElement doc = xmlDoc.CreateElement("document");
            xmlDoc.AppendChild(doc);

            // add filepath
            if (includeFilePath)
            {
                AddXmlElement(xmlDoc, doc, "filePath", null, filePath);
            }

            XmlElement fields;

            // add batchfields
            if (includeBatchFields)
            {
                fields = AddXmlElement(xmlDoc, doc, "batchFields");
                foreach (var field in BatchFields)
                {
                    AddXmlElement(xmlDoc, fields, "batchField", field.Key, field.Value);
                }
            }

            // add indexfields
            fields = AddXmlElement(xmlDoc, doc, "indexFields");
            foreach (var field in IndexFields)
            {
                AddXmlElement(xmlDoc, fields, "indexField", field.Key, field.Value);
            }

            // add tables
            if (Tables.Count > 0)
            {
                XmlElement xmlTables = AddXmlElement(xmlDoc, doc, "tables");
                foreach (Table table in Tables)
                {
                    XmlElement xmlTable = AddXmlElement(xmlDoc, xmlTables, "table", table.Name);
                    foreach (Row row in table.Rows)
                    {
                        XmlElement xmlRow = AddXmlElement(xmlDoc, xmlTable, "row");
                        foreach (Cell cell in row.Cells)
                        {
                            AddXmlElement(xmlDoc, xmlRow, "cell", cell.Name, cell.Value);
                        }
                    }
                }
            }

            // add kofax values
            if (includeKofaxValues)
            {
                fields = AddXmlElement(xmlDoc, doc, "kofaxValues");
                foreach (var field in KofaxValues)
                {
                    AddXmlElement(xmlDoc, fields, "kofaxValue", field.Key, field.Value);
                }
            }

            return xmlDoc;
        }

        private XmlElement AddXmlElement(XmlDocument doc, XmlElement rootElement, string subElementName, string fieldName = null, string fieldValue = null)
        {
            XmlElement field = doc.CreateElement(subElementName);
            if (fieldName != null)
            {
                XmlAttribute name = doc.CreateAttribute("name");
                name.Value = fieldName;
                field.Attributes.Append(name);
            }
            if (fieldValue != null)
            {
                XmlText value = doc.CreateTextNode(fieldValue);
                field.AppendChild(value);
            }
            rootElement.AppendChild(field);
            return field;
        }
    }
}
